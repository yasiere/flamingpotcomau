<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Page extends Model
{
	use Sluggable;
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey  = 'pageId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'page_title',
		'page_content',
		'page_img',
		'page_slug'
	];
	
	/**
	* Column Data Castings.
	*
	* @var array
	*/
	public function sluggable()
	{
		return [
			'page_slug' => [
				'source' => 'page_title'
			]
		];
	}
}