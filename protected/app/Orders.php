<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'orderId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'invoice_code',
		'payment',
		'order_meta',
		'order_status',
	];
	
	protected $casts = [
		'payment' => 'array',
		'order_meta' => 'array',
	];
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function details()
	{
		return $this->hasMany('App\OrderDetails', 'order_ID');
	}
}
