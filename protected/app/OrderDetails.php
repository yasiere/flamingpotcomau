<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	//protected $table  = 'orderDetails';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'orderDetailId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'order_ID',
		'product_ID',
		'detail_meta',
	];
	
	protected $casts = [
		'detail_meta' => 'array',
	];
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function products()
	{
		return $this->hasMany('App\Product', 'product_ID');
	}
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function order()
	{
		return $this->belongsTo('App\Orders', 'order_ID');
	}
}
