<?php 
namespace App\Developer;

use Menu;


/***D
 * This is the base class for all menus.
 * 
 *  Variable defined to use this class:
 *    - $menu_name			(required) a reference laravy menu name
 *    - $use_menu_data		(optional) if set true, menu data will add
 *    - $modul_prefix		(optional) a reference prefix modul route
 *    - $url_prefix			(optional) a reference prefix url menu
 *    - //$user_roles			(optional) a reference role of user
 *
 *  Function to use this class:
 *		- validateMenuPermission (optional) function to check menu will add or not.
 *			- @param (array) a reference menu item permission 
 *			- @return (boolean)
 *
 *
 *  @since:	1.0.1
 *  @created: 02/10/16
 */
class MenuBase
{
   /***D
    *  Reference laravy menu name. (required)
    *
    *  @date	03/10/16
    *  @since 1.0.1
	 *
    *  @var Class
	 */
	public $menu_name;
	
   /***D
    *  Reference prefix each menu item url. (optional)
    *
    *  @date	03/10/16
    *  @since 1.0.1
	 *
    *  @var Class
	 */
	public $url_prefix;
	
   /***D
    *  Reference role of user. (optional)
    *
    *  @date:	03/10/16
    *  @since: 1.0.1
	 *
    *  @var Class
	 
	public $user_roles;*/
	
   /***D
    *  Reference prefix modul route. (optional)
    *
    *  @date:	03/10/16
    *  @since: 1.0.1
	 *
    *  @var Class
	 */
	public $modul_prefix;
	
   /***D
    *  Reference if set true, menu data will add. (optional)
    *
    *  @date:	03/10/16
    *  @since: 1.0.1
	 *
    *  @var Class
	 */
	public $use_menu_data = false;
	
   /***D
    *  
    *
    *  @date:	03/10/16
    *  @since: 1.0.1
	 *
    *  @var Class
	 */
	protected $lavaryMenuCollection;
	
	
	/***D
	 * Class constructor
	 *
	 */
	public function __construct()
	{
		$this->lavaryMenuCollection = Menu::make($this->menu_name, function(){});
		$this->menuRegister();
	}
	
	
	/***D
	 *  This function will create menu item nickname if $use_menu_data set to true.
	 *
	 *  @description: Nickname use for get the developer modul name specially for admin page,
	 *						next step for front page. like maybe if use member modul ect.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $array (array) a reference menu item
	 *  @return: (array) a reference menu item with nickname
	 */
	public function createMenuItemNickname( $item )
	{
		// remove modul prefix
		$name = str_replace($this->modul_prefix, '', $item['url']);
		$name = explode('/', $name);
		$name = head($name);
		$item['nickname'] = camel_case($name);
		
		return $item;
	}
	
	
	/***D
	 *  This function will create menu item href.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $array (array) a reference menu item
	 *  @return: (array) a reference menu item with href
	 */
	public function createMenuItemHref( $item )
	{
		if( preg_match("/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", $item['url']) )
		{
			// instead href don't modified
			return $item;
		}

		$item = $this->createMenuItemNickname($item);
		
		if( $this->url_prefix )
		{
			$item['url'] = $this->url_prefix .'/'. $item['url'];
		} 
		
		$item['href'] = url($item['url']);
		
		return $item;
	}
	
	
	/***D
	 *  This function will merger the developer menu with defaults,
	 *  and add url with prefix if needed.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $array (array) a reference developer menu item
	 *  @return: (array)
	 */
	public function menuItemDefaults( $array )
	{
		$array = array_merge([
			'url' => '',
			'icon' => '',
			'title' => '',
			'modul' => '',
			'sub_menu' => '',
			'permission' => '*',
		], $array);
		
		$array = $this->createMenuItemHref($array);
		
		$array['modul'] = studly_case($array['modul']);

		return $array;
	}
	
	
	/***D
	 *  This function will check user menu permission.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $item (array) a reference menu item
	 *  @return: (boolean) a reference menu will register ?
	 */
	public function checkMenuPermission( $item )
	{
		$valid = true;
		if( $item['permission'] != '*' )
		{
			// check permission in developer function
			if( method_exists($this, 'validateMenuPermission') )
			{
				$valid = $this->validateMenuPermission($item['permission']);
			}
		}
		
		return $valid;
	}
	
	
	/***D
	 *  This function will add menu item to lavary menu.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $item (array) a reference menu item
	 *  @param: $parent_nickname (string) a reference parent menu nickname
	 */
	public function addToLavaryMenu( $item, $parent_nickname )
	{
		$lavary = $this->lavaryMenuCollection;
		if( $parent_nickname )
		{
			$lavary = $lavary->get($parent_nickname);
		}

		$lavary = $lavary->add($item['title'], array_only($item, ['url', 'nickname']));
		
		if( $this->use_menu_data )
		{
			$lavary->data(array_except($item, ['sub_menu']));
		}
	}
	
	
	/***D
	 *  This function will render developer menu.
	 *
	 *  @since:	1.0.1
	 *  @created: 02/10/16
	 *
	 *  @param: $menu (array) a reference developer menu
	 *  @param: $parent_nickname (string) a reference parent menu nickname
	 */
	public function addMenu( $menu, $parent_nickname = '' )
	{
		foreach( $menu as $item )
		{
			$item = $this->menuItemDefaults($item);
			if( $this->checkMenuPermission($item) )
			{
				$this->addToLavaryMenu($item, $parent_nickname);				
				if( $item['sub_menu'] )
				{
					$this->addMenu($item['sub_menu'], $item['nickname']);
				}
			}
		}
	}
}