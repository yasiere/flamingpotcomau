<?php 



function product_img_src( $img_id, $size = 'thumbnail' )
{
	$url = get_media_image_src($img_id, $size);
	return $url[0];
}


function hitung_harga_emas($product, $harga = false)
{
	if( ! $harga )
	{
		$harga = \App\Price::orderBy('priceId', 'DESC')->first();
		$harga = $harga->price_nominal;
	}

	$ongkos = $product->product_cost;
	if($ongkos == 0)
	{
		$ongkos = (float)$product->product_weight * 5000;
		if($ongkos <= 25000)
		{
			$ongkos = 25000;
		}
	}
	
	$kadar = $product->product_purity_exchange / 100;
	$price = ($harga * $kadar * $product->product_weight) + $ongkos;
	
	return ceil($price);
}