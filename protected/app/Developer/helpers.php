<?php
/***
 *  This helper function will convert number to currency.
 *
 *	@description: $decimals, $decimalPoint, $thousandSeperator
 *					can be set individually, or set by default on config (config.cms.helper.currency)
 *
 *  @date: 19/10/16
 *  @since: 1.0.3
 *  @author:
 *		- Wimbo denmazwimbo@yahoo.com
 *		- ...
 *
 *  @param	$value (number) a reference number to formatted
 *  @param	$decimals (integer) a reference currency formatted decimal
 *  @param	$decimalPoint (string) a reference currency formatted decimal point
 *  @param	$thousandSeperator (string) a reference currency formatted thousand seperator
 *
 *  @return: (string) formatted currency
 */
function currency($value, $numDecimals = null, $decimalSep = null, $thousandSep = null)
{
	$decimalSep = is_null($decimalSep)? config('cms.currency.decimal_sep'): $decimalSep;
	$thousandSep = is_null($thousandSep)? config('cms.currency.thousand_sep'): $thousandSep;
	$numDecimals = is_null($numDecimals)? config('cms.currency.num_decimals'): $numDecimals;
	
	return number_format($value, $numDecimals, $decimalSep, $thousandSep);
}




// get media from database
function get_media_image( $media_id, $render_as = 'collection' )
{
	$image = App\Media::find($media_id);
	if( $image && 'collection' != $render_as )
	{
	}
	
	return $image;
}

// render media wat needed
function get_media_image_src( $media_id, $size = 'thumbnail', $meta = false )
{
    $image = get_media_image($media_id);
    if ( $image )
	 {
		$image = $image->toArray();
		$param = 'media_meta.size.' . $size;
		if( array_has($image, $param) )
		{
			$image = array_get($image, $param);
		}
		else
		{
			$src = '/' . array_get($image, 'media_name');
			$image = [ $src, '100%', '100%' ];
		}
		
		$image[0] = asset('/upload') . $image[0];
		if( $meta )
		{
			$image['meta'] = array_forget($image, 'size');
		}
    }
	 
    return $image;
}

// get get_upload_url
function upload_url( $file )
{
	return asset('upload/' . $file);
} 