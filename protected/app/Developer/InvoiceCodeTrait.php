<?php 
namespace App\Developer;

/***D
 * This trait will generate auto invoice number
 *
 *  @since:	1.0.3
 *  @created: 19/10/16 
 */
trait InvoiceCodeTrait
{
	public function invoiceCode( $eloquent, $column )
	{
		$prefixCode = config('cms.invoice_code') . date('ymd');
		$invoiceCode = $eloquent::where($column,'like', "%{$prefixCode}%")->max($column);
		if($invoiceCode)
		{
			$invoiceCode = substr($invoiceCode, strlen($prefixCode));
		}

		$invoiceCode += 1;

		return $prefixCode . $invoiceCode;
	}
}