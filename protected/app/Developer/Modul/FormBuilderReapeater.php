<?php 
namespace App\Developer\Modul;

use Kris\LaravelFormBuilder\Form;


/********************************************************
 *  FLCMS Bundled
 *
 *	@autor: 
 *	- DenmazWimbo denmaz_wim2b@yahom.com
 */
class FormBuilderReapeater extends Form
{
	public function buildForm()
	{ 
		$fields = $this->getData('sub_fields');
		foreach( $fields as $field )
		{
			$this->add($field['name'], $field['type'], $field['options']);
		}
	}
}