<?php 
namespace App\Developer\Modul;


/***D
 *  This trait for show record on table
 *
 *	 Why me use trait, for some case developer need 
 *  to intercep/replace metode/function for special case condition.
 *
 *  @date: 04/10/16
 *  @since: 1.0.2
 *  @author: Wimbo denmazwimbo@yahoo.com
 *
 *  variable:
 *    - $_orderMethod	(optional) a reference shorting method, default 'DESC'
 *    - $_orderColum		(optional) a reference name table column to order,
 *											using on show table record.
 *											Default table primary key.
 *    - $_tableView		(optional) a reference table view blade, 'admin.layout.table'
 *
 ***/
trait TableTrait
{
	/***
	 *  This function will store what developers want to display.
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @param	$array (array) what developers want to display
	 */
	public function addColum( $array )
	{
		$array = array_merge([
			'title' => null,
			'column' => false,
			'img_column' => false,
		], $array);

		if( $array['img_column'] )
		{
			// if img_column set, it's mean developer whant to display image
			$column = $array['img_column'];
			$array['column'] = function( $record ) use ($column) {
				$img_id = is_array($record->$column) ? head($record->$column) : $record->$column;
				$src = get_media_image_src($img_id);
				
				return '<img src="'. $src[0] .'">';
			};
		}

		$this->_table[] = $array;
	}
	
	
	/***
	 *  This function will get DB table shorting method, default DESC. 
	 *
	 *	@defined $sorting	(string) a reference to sorting method, default DESC
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @return	(string) 
	 */
	public function getOrderMethod()
	{
		return property_exists($this, '_orderMethod') ? $this->_orderMethod : 'DESC';
	}
	
	
	/***D
	 *  This function will get name of DB table column to order, primary key name. 
	 *
	 *	@defined $_orderColum (string) a reference to name of Db table column
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 * 
	 *  @return	(string) 
	 */
	public function getOrderColumn()
	{
		return property_exists($this, '_orderColum')
					? $this->_orderColum : $this->getTableKeyName();
	}
	
	
	/***
	 *  This function will load records from database.
	 *  in certain cases the developers will determine how he retrieves records from a database with create this own function.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @param	$modul (string) a reference modul name
	 *  @return	(eloquent collect)
	 */
	public function getDataRecords()
	{
		$Eloquent = $this->getEloquent();
		
		return $Eloquent::orderBy($this->getOrderColumn(), $this->getOrderMethod())->get();
	}
	
	
	/***
	 *  This function just a dummy if developer not create registerTable function.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 */
	public function registerTable()
	{
		dd('Developer Make Mistake on table registration');
	}
	
	
	/***
	 *  This function render the table for view.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 */
	public function getRenderedTable()
	{
		$this->registerTable();

		return $table_data = [
			'column' => $this->_table,
			'records' => $this->getDataRecords(),
			'post_id' => $this->getTableKeyName(),
		];
	}
	
	
	/***
	 *  This function will get table view, default (admin.layout.table). 
	 *
	 *  @defined $_tableView	(string) a reference blade to show tableRecords
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @return (string) 
	 */
	public function getTableView()
	{
		return property_exists($this, '_tableView') ? $this->_tableView : 'admin.layout.table';
	}
}