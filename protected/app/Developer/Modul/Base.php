<?php
namespace App\Developer\Modul;

use Auth;

/***D
 *  This base class for all Modul
 *
 *  @date: 04/10/16
 *  @since: 1.0.2
 *  @author: Wimbo denmazwimbo@yahoo.com
 *
 *  variable:
 *    - $_table			(required and don't redefined) a reference table colum will use in show table record view
 *    - $_formGroups		(required and don't redefined) a reference table form will use in show table record view
 *    - $_modulName		(optional) see in class below
 *    - $_eloquent		(optional) see in class below
 *    - $_permission		(optional) a reference user permission to use this 
 *    - $_action			(optional) a reference action modul ['add', 'edit', 'delete']
 *
 ***/ 
class Base
{
	/***
	 *  Holds developer table records blueprit.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 *
	 *  @var: Class
	 */ 
	protected $_table = [];

	/***
	 *  Holds developer form groups records blueprit.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 *
	 *  @var: Class
	 */ 
	protected $_formGroups = [
		'side' => [],
		'default' => [],
		'primary' => [],
		'form_builder' => [],
	];

	/***
	 *  Holds developer modul class name.
	 *
	 *	@info: This variable is important.
	 *		can be set via child class contructor,
	 *		child class variable (but carefully on duplicated) or contoller class. 
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 *
	 *  @var: Class
	 */ 
	protected $_modulName;


	/***
	 *  This function will get eloquent model class location, defaults App\{ModulName}.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 *
	 *  @return: (string) a reference to eloquent class location
	 */
	public function getEloquent()
	{
		return property_exists($this, '_eloquent')
					? $this->_eloquent : 'App\\'. $this->_modulName;
	}


	/***
	 *  This function will will set the modul name for this class.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 *
	 *  @return: (string) a reference to eloquent class location
	 */
	public function setModulName( $name )
	{
		$this->_modulName = $name;
	}


	/***
	 *  This function will get DB table primary key name.
	 *
	 *  @date: 01/10/16
	 *  @since: 1.0.1
	 *
	 *  @return: (string)
	 */
	public function getTableKeyName()
	{
		$model = $this->getEloquent();
		
		return (new $model)->getKeyName();
	}


	/***
	 *  This function will get modul permission spesific role, or return false not defined.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 * 
	 *  @return	(array)
	 */
	public function getPermission()
	{ 
		$permission = property_exists($this, 'permission') ? $this->permission : false;

		if( $permission )
		{
			$role = Auth::user()->user_role;
			if( isset($permission[$role]) )
			{
				$permission = $permission[$role];
			}
			else
			{
				$permission = null;
			}
		}

		return $permission;
	}


	/***
	 *  This function will check modul permission.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *	Rule format:
	 *	- role name => [
	 *		action => [add, edit, show, delete] or * (grant all permission),
	 *		rule => '==' or '!='
	 *	
	 *  @param	$method (string) a reference method name will check {add, delete, edit, show}
	 *  @return	(boolean)
	 */
	public function hasPermission( $method )
	{
		$permission = $this->getPermission();
		if( $permission )
		{ 
			if( '!=' == $permission['rule'] )
			{
				return ( ! in_array($method, (array) $permission['action']) );
			}
			
			if( '==' == $permission['rule'] )
			{
				return ( in_array($method, (array) $permission['action']) );
			}
		}

		return true;
	}


	/***
	 *  This function will get modul what can do with this modul.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 * 
	 *  @return	(array)
	 */
	public function getAction()
	{
		$action = property_exists($this, '_action')? $this->_action : ['add', 'edit', 'delete'];
		$permission = $this->getPermission();
		if( $permission )
		{
			if( '!=' == $permission['rule'] )
			{
				$action = array_except($action, $permission['action']);
			}

			if( '==' == $permission['rule'] )
			{
				$action = $permission['action'];//array_only($permission['action'], $action);
			}
		}
		
		return $action;
	}
}