<?php 
namespace App\Developer\Modul;


/***D
 *  This trait for hadle form
 *
 *  Why me use trait, for some case developer need 
 *  to intercep/replace metode/function for special case condition.
 *
 *  @date: 04/10/16
 *  @since: 1.0.2
 *  @author: Wimbo denmazwimbo@yahoo.com
 *
 *  variable:
 *    - $_formView (optional) a reference form view blade, 'admin.layout.form'
 *
 ***/
trait FormTrait
{
	/***D
	 *  This function will create field repeater.
	 *
	 *  @since:	1.0.1
	 *  @created: 03/10/16
	 *
	 *  @param: $array (array) a reference form field
	 *  @return: (array)
	 */
	public function createRepeaterForm( $field )
	{
		// defaults 
		$field['options'] = $this->fieldDefaults($field['options']);/*  $field['options'] array_merge([
			'sub_fields' => [],
		], $field['options']); */
		
		//'display' => 'inline', or block
		
		
		if( 'form' == $field['options']['type'] )
		{
			$sub_fields = [];
			foreach( $field['options']['sub_fields'] as $items )
			{
				$sub_fields[] = $this->fieldDefaults($items);
			}
			
			if( !isset($field['options']['options']) )
			{
				$field['options']['options'] = [];
			}
			
			if( !isset($field['options']['options']['data']) )
			{
				$field['options']['options']['data'] = [];
			}
			
			$field['options']['options']['class'] = 'App\Developer\Modul\FormBuilderReapeater';
			$field['options']['options']['data']['sub_fields'] = $sub_fields;
		}
		
		return $field;
	}
	
	
	/***D
	 *  This function will merger the developer field with defaults.
	 *
	 *  @since:	1.0.1
	 *  @created: 03/10/16
	 *
	 *  @param: $array (array) a reference form field
	 *  @return: (array)
	 */
	public function fieldDefaults( $field )
	{
		// defaults 
		$field = array_merge([
			'name' => '',
			'type' => 'text',
			'label' => false,
			'options' => [],
			'instructions' => '',
		], $field); 
		
		$field['options']['label'] = $field['label'];
		
		if( $field['instructions'] )
		{
			$field['options']['help_block'] = ['text' => $field['instructions'] ];
		}
		
		if( 'repeater' == $field['type'] )
		{
			$field = $this->createRepeaterForm($field );
		}
		
		return $field;
	}


	/***D
	 *  This function will merger the developer form group with defaults.
	 *
	 *  @since:	1.0.1
	 *  @created: 03/10/16
	 *
	 *  @param: $array (array) a reference form group
	 *  @return: (array)
	 */
	public function formDefaults( $group )
	{
		$group = array_merge([
			'id' => '',
			'title' => '',
			'fields' => [],
			'options' => [
				'position' => 'default', //or primary or side
			],
		], $group);
		
		// add id
		if( ! $group['id'] )
		{
			$group['id'] = uniqid();
		}

		foreach( $group['fields'] as $key => $field )
		{
			$group['fields'][$key] = $this->fieldDefaults($field);
		}
		
		return $group;
	}


	/***
	 *  This function will store what developers form.
	 *
	 *  @date:	03/10/16
	 *  @since:	1.0.1
	 *
	 *  @param	$array (array) form goups
	 *  @storeTo $form_fields (array) 
	 */
	public function addFormGroup( $array )
	{
		$group = $this->formDefaults($array);
		
		$position = $group['options']['position'];
		
		$this->_formGroups[$position][] = $group;
	}


	/***
	 *  This function will create form, base on kris form builder.
	 *
	 *  @date:	03/10/16
	 *  @since:	1.0.1
	 *
	 *  @param
	 *  @return
	 */
	public function createForm( $Form_Builder, $model = false )
	{
		$this->registerForm();
		$form_options = ['method' => 'POST'];
		if( $model )
		{
			$form_options['model'] = $model;
		}

		$form = $this->_formGroups;		
		$fields = [];

		foreach( $form as $position => $groups )
		{
			foreach( $groups as $key => $group )
			{
				$fields = array_merge($fields, $group['fields']);
			}
		}
		
		$form['form_builder'] = $Form_Builder->create("App\Developer\Modul\FormBuilder",
												$form_options, [ 'fields' => $fields ]);
		//$form['post_id'] = $post_id;
		
		return $form;
	}


	/***
	 *  This function will get filter post value repeater. 
	 *
	 *	@defined $_tableView	(string) a reference blade to show form
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @return (string) 
	 */
	public function filterPostRepeater( $post )
	{
		foreach($post as $key => $val)
		{
			if( is_array($val) )
			{
				$val = $this->filterPostRepeater($val);
				$post[$key] = array_filter($val);
			}
		}
		
		return $post;
	}


	/***
	 *  This function will get form view. 
	 *
	 *	@defined $_tableView	(string) a reference blade to show form
	 *
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 *  @return (string) 
	 */
	public function getFormView()
	{
		return property_exists($this, '_formView') ? $this->_formView : 'admin.layout.form';
	}


	/***
	 *  This function just a dummy if developer not create registerForm function.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 */
	public function registerForm()
	{
		dd('Developer Make Mistake on form registration');
	}
} 