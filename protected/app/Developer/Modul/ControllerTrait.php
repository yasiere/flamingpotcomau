<?php 
namespace App\Developer\Modul;

use View;
use Auth;
use Menu;
use Route;
use Breadcrumbs;


/***D
 *  This trait for hadle App\Http\Controllers\Admin\BaseController
 *
 *
 *  @date: 04/10/16
 *  @since: 1.0.2
 *  @author: Wimbo denmazwimbo@yahoo.com
 *
 ***/
trait ControllerTrait
{
   /***
    *  Holds dashboard variable.
    *
    *  @date 01/10/16
    *  @since 1.0.1
	*
    *  @var Class
	*//* 
	protected $_dashboard; */
	
   /***
    *  Holds permission name.
    *
    *  @date: 01/10/16
    *  @since: 1.0.1
	*
    *  @var Class
	*/
	protected $_permission;
	
   /***
    *  Holds post id.
    *
    *  @date: 01/10/16
    *  @since: 1.0.1
	*
    *  @var Class
	*/
	protected $_postId;
	
   /***
    *  Holds laravel Request.
    *
    *  @date: 01/10/16
    *  @since: 1.0.1
	*
    *  @var Class
	*/
	protected $_request;
   
   /***D
    *  Holds contructed modul class.
    *
    *  @date: 01/10/16
    *  @since: 1.0.1
	*
    *  @var Class
	*/
	protected $_moduClass;
	
   /***
    *  Holds vendor kris laravel form builder.
    *
    *
    *  @date: 03/10/16
    *  @since: 1.0.1
	*
    *  @var Class
	*/
	protected $_formBuilder;
	
	/***
	 *  This function call on consruct.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 */
	public function init( $route_modul = false )
	{
		if( ! $route_modul )
		{
			$route_modul = $this->routeParam('modul');	
		}

		$menu = Menu::get('adminMenu');
		$menu_current = $menu->item(camel_case($route_modul));
		if( empty($menu_current) )
		{
			return redirect()->route('admin::page404')->send();
		}

		$name_modul = $menu_current->data['modul'];
		$this->constructModul($name_modul);

		// share variable for dashboard
		if( ! str_contains(Route::currentRouteName(), ['store', 'delete']) )
		{
			$menu_current->active();
			if( $menu->find($menu_current->parent) )
			{ 
				$menu->find($menu_current->parent)->active();
			}
			
			$dashboard = [
				'icon' => $menu_current->data['icon'],
				'title' => $menu_current->data['title'],
				'post_id' => $this->_postId,
				'modul_url' => $route_modul,
				'modul_name' => $name_modul,
			];
			
			$actions = $this->_moduClass->getAction();
			foreach(['add', 'edit', 'delete'] as $act )
			{
				$dashboard['button.' . $act] = in_array($act, $actions);
			}
			
			$sub_title = '';
			if( $this->_postId )
			{ 
				$Eloquent = $this->_moduClass->getEloquent();
				$Eloquent = $Eloquent::findOrFail($this->_postId);
				$sub_title = strtolower($name_modul). '_title';
				$sub_title = $Eloquent->$sub_title;
			} 
			
			$this->addBreadcrumbs($dashboard, $sub_title);

			View::share('dashboard', $dashboard);
		}
	}
	
	
	/***D
	 *  This function will convert route name to permission
	 *
	 *  @date: 19/10/16
	 *  @since: 1.0.3
	 *
	 *  @param $name (string) a reference to parameter name to get
	 *  @return (string) a reference to parameter name value
	 */
	public function setPermission()
	{
		$name = Route::currentRouteName();
		$this->_postId = $this->routeParam('post_id');
		if( str_contains($name, ['form', 'store']) )
		{
			$this->_permission = $this->_postId ? 'edit': 'add';
			return;
		}

		foreach(['delete', 'show'] as $param )
		{
			if( str_contains($name, $param) )
			{
				$this->_permission = $param;
				break;
			}
		}
	}
	
	
	/***D
	 *  This function will get current route parameter
	 *
	 *  @date: 19/10/16
	 *  @since: 1.0.3
	 *
	 *  @param $name (string) a reference to parameter name to get
	 *  @return (string) a reference to parameter name value
	 */
	public function routeParam( $name )
	{
		return Route::current()->getParameter($name);
	}
	
	public function constructModul( $modul_name )
	{
		$this->setPermission();
		$modul = '\App\Admin\Moduls\\'. $modul_name;
		$modul = new $modul();
		
		if( ! $modul->hasPermission($this->_permission) )
		{
			return redirect()->route('admin::page404')->send();
		}

		$modul->setModulName($modul_name);
		$this->_moduClass = $modul;
	}


	/***
	 *  This function share dashboard base variable to view.
	 *  like action (add, edit, delete), breadcrumbs etc.
	 * 
	 *  @date	01/10/16
	 *  @since 1.0.1
	 *
	 * @param $prefix (string) reference a title prefix for form action. default null
	 */
	public function addBreadcrumbs( $data, $sub_title = '' )
	{
		Breadcrumbs::register('admin::show', function($crumbs) use($data){
			$crumbs->push($data['title'], route('admin::show', $data['modul_url']));
		});
		
		if(  'show' != $this->_permission )
		{ 
			$name = Route::currentRouteName();
			Breadcrumbs::register($name, function($crumbs) use($data, $sub_title){
				if( $data['button.add'] )
				{
					$crumbs->parent('admin::show');
				}
				
				$title = $sub_title? str_singular($sub_title): 'Add '. str_singular($data['title']);
				
				$crumbs->push($title);
			}); 
		}
	}
}