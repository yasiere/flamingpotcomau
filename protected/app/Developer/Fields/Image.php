<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Image extends FormField {

    protected function getTemplate()
    {
        return 'developer.fields.image';
    }
}
