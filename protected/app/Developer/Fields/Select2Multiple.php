<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\ChoiceType;

class Select2Multiple extends ChoiceType {

    /**
     * @inheritdoc
     */
    protected function getDefaults()
    {
        return [
            'choices' => [],
            'selected' => null,
            'expanded' => false,
            'multiple' => true,
            'choice_options' => [
                'wrapper' => false,
                'is_child' => true
            ],
			'attr' => [
				'style' => 'width: 100%;',
				'class' => 'select2-control',
				'data-tags'=> 'true'
			],
        ];
    }
}
