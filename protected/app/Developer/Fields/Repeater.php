<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\CollectionType;

class Repeater extends CollectionType
{
    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'developer.fields.repeater';
    } 

	
    /**
     * @inheritdoc
     */
    protected function getDefaults()
    {
        return [
            'type' => null,
            'options' => ['is_child' => true],
            'prototype' => true,
            'data' => null,
            'property' => 'id',
            'prototype_name' => '__NAME__',
            'empty_row' => false,
				'display' => 'inline',
				'button_label' => 'Item',
        ];
    }


    public function render(array $options = [], $showLabel = true, $showField = true, $showError = true)
    {
        $options['repeater-clone'] = $this->prototype();
		
        return parent::render($options, $showLabel, $showField, $showError);
    }
}
