<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\TextareaType;

class Wysiwyg extends TextareaType {
    /**
     * @inheritdoc
     */
    protected function getDefaults()
    {
        return [
            'attr' => ['class' =>'wysiwyg-control']
        ];
    }
}
