<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\SelectType;

class Select2 extends SelectType {
    /**
     * @inheritdoc
     */
    public function getDefaults()
    {
        return [
            'choices' => [],
            'empty_value' => null,
            'selected' => null,
			'empty_value' => '=== Select ===',
			'attr' => [
				'style' => 'width: 100%;',
				'class' => 'select2-control'
			]
        ];
    }
}