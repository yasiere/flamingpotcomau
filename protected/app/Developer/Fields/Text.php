<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\FormField;

class Text extends FormField
{
    /**
     * @inheritdoc
     */
    protected function getTemplate()
    {
        return 'developer.fields.text';
    }
	
	
    /**
     * @inheritdoc
     */
    public function getDefaults()
    {
        return [
            'label_before' => '',
            'label_after' => '', 
        ];
    }
}
