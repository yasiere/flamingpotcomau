<?php
namespace App\Developer\Fields;

use Kris\LaravelFormBuilder\Fields\TextareaType;

class CKEditor extends TextareaType {
    /**
     * @inheritdoc
     */
    protected function getDefaults()
    {
        return [
            'attr' => ['class' =>'ckeditor-control']
        ];
    }
}
