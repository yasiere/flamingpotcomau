<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Media extends Model
{
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'mediaId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'media_meta',
		'media_type',
		'media_name'
	];
	
	/**
	* Column Data Castings.
	*
	* @var array
	*/
	protected $casts = [
		'media_meta' => 'array'
	];
}