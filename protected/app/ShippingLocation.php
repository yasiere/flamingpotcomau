<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingLocation extends Model
{
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	//protected $table  = 'shipping_locations';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'locationId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'shipping_ID',
		'location_code',
		'location_type',
	];
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function shipping()
	{
		return $this->belongsTo('App\Shipping', 'shipping_ID');
	}
}