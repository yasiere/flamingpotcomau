<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Category extends Model
{
	use Sluggable;
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'categoryId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'category_slug',
		'category_title',
	];
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function products()
	{
		return $this->hasMany('App\Product', 'category_ID');
	}
	
	/**
	* Column Data Castings.
	*
	* @var array
	*/
	public function sluggable()
	{
		return [
			'category_slug' => [
				'source' => 'category_title'
			]
		];
	}
}

