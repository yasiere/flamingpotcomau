<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Home extends Model
{
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	//protected $table  = 'home';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'homeId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'banner_title',
		'banner_img',
		'banner_link',
		'banner_category',
		'banner_text',
	];
	
	
	public function category()
	{
		return $this->belongsTo('App\Category', 'banner_category');
	}
}