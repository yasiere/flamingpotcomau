<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Subscribe extends Model
{
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	protected $table  = 'subscribe';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'subscribeId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'subscribe_name',
		'subscribe_email',
	];
}