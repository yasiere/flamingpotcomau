<?php
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey  = 'ID';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'user_login',
		'password',
		'user_nicename',
		'user_email',
		'user_role',
		'display_name',
		'user_avatar',
		'last_login',
	];
}