<?php
namespace App\Site;
 
use Kris\LaravelFormBuilder\Form;
use Carbon\Carbon;

class CheckoutCardForm extends Form
{
	protected $clientValidationEnabled = false;
	
	public function buildForm()
	{
		$date = Carbon::now();
		$scope = Carbon::now()->addYear(11);
		$years = [];
		for( $y = $date->year; $y < $scope->year; $y++ )
		{ 
			$years[$y] = $y;
		}
		
		/*$this ->add('cardNumber', 'text', [
			'rules' => 'required',
			'label' => 'Card Number',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Card Number'
			],
			'attr'  =>[
				'size' => 20,
				'data-stripe' => 'number',
				'placeholder' => 'Card Number',
			],
		]) */
		$this->add('cardExpiryMonth', 'select', [
			'choices' => ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
			'selected' => $date->month,
			'label' => 'Exp. Mo.',
			'wrapper' => [
				'class' => 'field required',
				'data-label' => 'Exp. Mo.',
			],
			'attr'  =>[ 
				'data-stripe' => 'exp_month', 
			],
		])
		->add('cardExpiryYear', 'select', [
			'choices' => $years,
			'selected' => $date->year,
			'label' => 'Exp. Year',
			'wrapper' => [
				'class' => 'field required',
				'data-label' => 'Exp. Year',
			],
			'attr'  =>[ 
				'data-stripe' => 'exp_year',
			],
		]); /* 
		->add('cardCVC', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'CVC',
			],
			'attr'  =>[
				'size' => 4,
				'placeholder' => 'CVC',
				'data-stripe' => 'cvc',
			],
		]); */
	}
}