<?php
namespace App\Site;
 
use Kris\LaravelFormBuilder\Form;

class ContactForm extends Form
{
	public function buildForm()
	{
		$this->add('fname', 'text',[
			'rules' => 'required|min:3',
			'attr'  =>[
				'class' => 'field-control',
				'maxlength' => 30
			]
		])
		->add('lname', 'text',[
			'rules' => 'required|min:3',
			'attr'  =>[
				'class' => 'field-control',
				'maxlength' => 30
			]
		])
		->add('cemail', 'email',[
			'rules' => 'required',
			'label' => 'Email Address',
			'attr' => [
				'class' => 'field-control',
			]
		])
		->add('csubject', 'text',[
			'rules' => 'required',
			'label' => 'Subject',
			'attr' => [
				'class' => 'field-control',
			]
		])
		->add('cmessage', 'textarea',[
			'rules' => 'required',
			'label' => 'Message',
			'attr' => [
				'class' => 'field-control',
			]
		]);
	}
}