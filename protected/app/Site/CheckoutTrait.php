<?php
namespace App\Site;

use View;
use Mail;
use Session;
use App\OrderDetails;

/***D
 * This trait will ...
 *
 *  @since:	1.0.3
 *  @created: 19/10/16 
 */
trait CheckoutTrait
{
	/***
	 *  This function will store order detail.
	 *
	 *
	 *  @date: 19/10/16
	 *  @since: 1.0.2
	 *
	 *  @param: $cart (closure) a reference to instace cart
	 *  @param: $orderId (string) a reference primary key order table
	 */
	public function storeDetailOrder( $cart, $orderId, $order )
	{
		$items = '';
		$order = array_dot($order);
		$carts = $cart->content();
		foreach( $carts as $item )
		{
			$detail = [
				'order_ID' => $orderId,
				'product_ID' => $item->id,
				'detail_meta' => [
					'qty' => $item->qty,
					'price' => $item->price,
					'variant' => $item->options->variant,
				]
			];
			
			OrderDetails::create($detail);
			$items = '<tr class="item"><td>'. $item->name;
			foreach($item->options->variant as $variant )
			{
				$items .= '<small style="display: block">'. $variant .'</small>';
			}
			
			$items .= '</td><td>'. $item->qty .'</td>';
			$items .= '<td>$'. currency($item->price) .'&nbsp;'. $order['order_meta.cost.currency'] .'</td><tr>';
		}
/* 
		Mail::send('emails.invoice', ['order'=> $order, 'items' => $items], function($message) use($order) {
			$shop_mail = config('cms.site.shop_mail');
			$shop_title = config('cms.site.shop_title');
			$message->to($order['order_meta.shipping.email'], $order['order_meta.shipping.name'])
				->bcc($shop_mail)
				->from($shop_mail, $shop_title)
				->subject("{$shop_title} Invoice");
		}); */
		
		
		// use this old, because laravel driver mail not stabel
		/* $mail  = "<h2>From: {$request->fname} {$request->lname}</h2>";
		$mail .= "<h2>Email Address: {$request->cemail}</h2>";
		$mail .= "<h2>Subject: {$request->csubject}</h2>";
		$mail .= "<h2>Message:</h2><div>{$request->cmessage}</div>"; */
		
		$view = View::make('emails.invoice', ['order'=> $order, 'items' => $items]);
		//$contents = (string) $view;
		// or
		$mail = $view->render();
		
		$headers  = 'From: info@vdunia.com' . "\r\n";
		$headers .= 'Reply-To: info@vdunia.com' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		$headers .= "MIME-Version: 1.0". PHP_EOL;
		$headers .= "Content-type: text/html; charset=ISO-8859-1" . PHP_EOL;
		
		mail('namaexample@gmail.com', "Flamingpot Invoice", $mail, $headers);

		// destroy cart session
		$cart->destroy();
		Session::forget(['meta_shipping', 'meta_cost']);

		// go to payment success with message
		return redirect()
			->route('checkout.success')
			->with('alert', ['class'=>'success', 'message' => 'payment success'])
			->send();
	}
}