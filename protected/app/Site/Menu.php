<?php 
namespace App\Site;

use App\Developer\MenuBase;


class Menu extends MenuBase
{
	public function __construct()
	{
		$this->menu_name = 'siteMenu';

		// do not delete!
    	parent::__construct();
	}
	
	
	public function menuRegister()
	{
		$this->addMenu([
			[
				'url' => '',
				'title' => 'Home',
			],
			[
				'url' => 'shop',
				'title' => 'Shop',
			],
			[
				'url' => 'about',
				'title' => 'About',
			],
			[
				'url' => 'news',
				'title' => 'News',
			],
			[
				'url' => 'news',
				'title' => 'Press',
			],
			[
				'url' => 'contact',
				'title' => 'Contact',
			],
		]); 
	}
}