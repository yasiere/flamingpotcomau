<?php
namespace App\Site;
 
use Kris\LaravelFormBuilder\Form;


class CheckoutBillingForm extends Form
{
	protected $clientValidationEnabled = false;
	
	public function buildForm()
	{
		$this->add('billingName', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Name',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Your Name',
			],
		])/* 
		->add('billingLastName', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Last Name',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Last Name',
			],
		]) */
		->add('billingAddress', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Street Address',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Street Address',
			],
		])/* 
		->add('billingAddress2', 'text', [
			'wrapper' =>[
				'class' => 'field',
				'data-label' => 'Street Address 2',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Street Address 2',
			],
		]) */
		->add('billingCountry', 'select', [
			'choices' => config('countries'),
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Country',
			]
		])
		->add('billingCity', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'City',
			],
			'attr' =>[
				'maxlength' => 30,
				'placeholder' => 'City',
			],
		])
		->add('billingState', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'State',
			],
			'attr' =>[
				'placeholder' => 'State',
			],
		])
		->add('billingZip', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Zip Code',
			],
			'attr' =>[
				'data-stripe' => 'address_zip',
				'placeholder' => 'Zip / Postal',
			],
		]);
	}
}