<?php
namespace App\Site;
 
use Kris\LaravelFormBuilder\Form;

class CheckoutShippingForm extends Form
{
	protected $clientValidationEnabled = false;
	
	public function buildForm()
	{
		$this->add('shippingEmail', 'email', [
			'rules' => 'required|email',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Email Address'
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Email',
			],
			'help_block' => [
				'text' => 'Receipts and notifications will be sent to this email address.',
			], 
		])
		->add('shippingName', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Name',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Your Name',
			],
		])/* 
		->add('shippingLastName', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Last Name',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Last Name',
			],
		]) */
		->add('shippingAddress', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Street Address',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Street Address',
			],
		])/* 
		->add('shippingAddress2', 'text', [
			'wrapper' =>[
				'class' => 'field',
				'data-label' => 'Street Address 2',
			],
			'attr'  =>[
				'maxlength' => 50,
				'placeholder' => 'Street Address 2',
			],
		]) */
		->add('shippingCountry', 'select', [
			'choices' => config('countries'),
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Country',
			]
		])
		->add('shippingCity', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'City',
			],
			'attr' =>[
				'maxlength' => 30,
				'placeholder' => 'City',
			],
		])
		->add('shippingState', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'State',
			],
			'attr' =>[
				'placeholder' => 'State',
			],
		])
		->add('shippingZip', 'text', [
			'rules' => 'required',
			'wrapper' =>[
				'class' => 'field required',
				'data-label' => 'Zip Code',
			],
			'attr' =>[
				'placeholder' => 'Zip / Postal',
			],
		])
		->add('shippingPhone', 'text', [
			'wrapper' =>[
				'class' => 'field',
				'data-label' => 'Phone Number',
			],
			'attr' =>[
				'placeholder' => 'Phone Number',
			],
		]);
	}
}