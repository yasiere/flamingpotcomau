<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class News extends Model
{
	use Sluggable;
	
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	protected $table  = 'news';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'newsId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [ 
		'news_title',
		'news_content',
		'news_img',
		'news_slug',
		'news_excerpt',
	];

	/**
	* Column add slug url Castings.
	*
	* @var array
	*/
	public function sluggable()
	{
		return [
			'news_slug' => [
				'source' => 'news_title'
			]
		];
	}
}