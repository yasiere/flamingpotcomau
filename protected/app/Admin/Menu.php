<?php 
namespace App\Admin;

use Auth;
use App\Developer\MenuBase;


class Menu extends MenuBase
{
	public function __construct()
	{
		$this->menu_name = 'adminMenu';
		$this->use_menu_data = true;
		$this->url_prefix = config('cms.cms_uri');
		$this->modul_prefix = ['add/', 'delete/'];

		// do not delete!
    	parent::__construct();
	}
	
	
	/***
	 * see trat default and register admin
	 * user_level dapat berupa
	 * * boolean true/false
	 * * number 1/2/3 etc
	 * * array [ 1, 2, 3 ]
	 *
	 */
	public function menuRegister()
	{
		$this->addMenu([
			[
				'url' => '',
				'icon' => 'th',
				'title' => 'Dashboard',
			],
			[	'url' => 'all-pages',
				'icon' => 'file',
				'title' => 'Pages',
				'sub_menu'=> [
					[
						'url' => 'home',
						'modul' => 'home',
						'title' => 'Home',
						'icon' => 'diamond',
					],
					[
						'url' => 'add/about/1',
						'modul' => 'about',
						'title' => 'About',
						'icon' => 'diamond',
					],
					[
						'url' => 'add/contact/2',
						'modul' => 'page',
						'title' => 'Contact',
						'icon' => 'diamond',
					],
					[
						'url' => 'add/faq/3',
						'modul' => 'page',
						'title' => 'Faq',
						'icon' => 'diamond',
					],
				]
			],
			/* [	'url' => 'orders',
				'icon' => 'truck',
				'modul' => 'orders',
				'title' => 'Orders',
			], */
			[	'url' => 'products',
				'icon' => 'archive',
				'modul' => 'product',
				'title' => 'Products',
			],
			[	'url' => 'category',
				'icon' => 'archive',
				'modul' => 'category',
				'title' => 'Category',
			],
			[	'url' => 'shipping',
				'icon' => 'truck',
				'modul' => 'shipping',
				'title' => 'Shipping',
			],
			[	'url' => 'subscribe',
				'icon' => 'truck',
				'modul' => 'subscribe',
				'title' => 'Subscribe',
			],
			[	'url' => 'news',
				'modul' => 'news',
				'title' => 'News',
				'icon' => 'newspaper-o',
			]
		]);
	}
	
	
	public function validateMenuPermission( $permission )
	{
		$user = Auth::user(); 
		
		return in_array($user->user_role, (array) $permission);
	}
}