<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;


class Home extends Base
{
	use FormTrait, TableTrait;
	
	/***
	 *  your modul can hadle
	 * 
	 *  @var
	 */
	protected $_action = ['edit'];

	/***
	 *  your form layout.
	 * 
	 *  @var
	 */ 
	//protected $_formView = 'admin.layout.form-full';
	
	/***
	 *
	 * 
	 *  @var
	 */
	protected $permission = [
		'admin' =>[
			'action'=>['show', 'edit'],
			'rule' => '=='
		]
	];
	
	protected $_orderMethod = 'ASC';
	
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'Row Numb',
			'column' => function( $record ){
				return 'Banner Row '. $record->homeId;
			},
		]);
		$this->addColum([
			'title' => 'Title',
			'column' => 'banner_title',
		]);
		$this->addColum([
			'title' => 'Cover',
			'column' => function( $record ){
				if($record->banner_img)
				{
					$src = get_media_image_src($record->banner_img);
					return '<img src="'. $src[0] .'">';
				}
			}
		]);
	}
	
	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$category = \App\Category::orderBy('category_title')->lists('category_title', 'categoryId')->toArray();
		$category = array_prepend($category, 'All'); 
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Banner Title',
					'name' => 'banner_title',
				],
				[
					'type' => 'text',
					'label' => 'Category Title',
					'name' => 'banner_link',
				],
				[
					'type' => 'select2',
					'label' => 'Category Link',
					'name'  => 'banner_category',
					'options' => [
						'choices' => $category,
						'empty_value' => false,
					]
				],
				[
					'type' => 'wysiwyg',
					'label' => 'Banner Text',
					'name' => 'banner_text',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
		$this->addFormGroup([
			'title' => 'Background Image',
			'fields' => [
				[
					'type' => 'image',
					'name' => 'banner_img',
				],
			],
			'options' => [
				'position' => 'side' // or side or default
			],
		]);
	}
}