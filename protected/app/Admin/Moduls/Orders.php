<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;


class Orders extends Base
{
	use FormTrait, TableTrait;
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'Product',
			'column' => function($record){
				$src = get_media_image_src(head($record->product_img));
				
				return '<span class="aspect-ratio aspect-ratio--square--50"><img class="aspect-ratio__content" src="'. $src[0] .'"></span>'. $record->product_title;
			},
		]);
		$this->addColum([
			'title' => 'Category',
			'column' => function($record){
				return $record->category->category_title;
			},
		]);
		$this->addColum([
			'title' => 'SKU',
			'column' => 'product_sku',
		]);
		$this->addColum([
			'title' => 'Price',
			'column' => 'product_price',
		]);/* 
		$this->addColum([
			'title' => 'Cover',
			'img_column' => 'product_img',
		]); */
	}
	
	
	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$Variant = $this->getEloquent();
		$variant = $Variant::distinct()->select('product_variant')->get()->toArray();
		$variant_option = [];
		foreach( $variant as $options )
		{
			if( is_array($options['product_variant']) )
			{
				foreach( $options['product_variant'] as $option )
				{
					if( is_array($option['variant_option']) )
					{
						foreach( $option['variant_option'] as $val )
						{
							$variant_option[$val] = $val;
						}
					}
				}
			}
		}
		
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Title',
					'name' => 'product_title',
				],
				[
					'type' => 'wysiwyg',
					'label' => 'Description',
					'name' => 'product_desc',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
		$this->addFormGroup([
			'title' => 'Images',
			'fields' => [
				[
					'type' => 'repeater',
					'name' => 'product_img',
					'instructions' => '* Minimum image resolution: 800 x 800 (px)',
					'options' => [
						'type' => 'image',
					]
				],
			],
		]);
		$this->addFormGroup([
			'title' => 'Detail',
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Price',
					'name' => 'product_price',
					'options' =>[
						'label_after' => 'AUD'
					]
				],
				[
					'type' => 'text',
					'label' => 'SKU (Stock Keeping Unit)',
					'name' => 'product_sku',
				],
				[
					'type' => 'text',
					'label' => 'Weight',
					'name' => 'product_weight',
					'options' => [
						'rules'=> 'required',
						'label_after' => 'oz'
					]
				],
			],
			'options' => [
				'position' => 'side' // or side or default
			],
		]);
		// repeater
		$this->addFormGroup([
			'title' => 'Variant',
			'fields' => [
				[
					'type' => 'repeater',
					'name' => 'product_variant',
					'options' => [
						'type' => 'form',
						'sub_fields' => [
							[
								'type' => 'text',
								'name' => 'variant_name',
								'label' => 'Variant Name',
							],
							[
								'type' => 'select2_multiple',
								'name' => 'variant_option',
								'label' => 'Variant Option',
								'options' => [
									'choices' => $variant_option,
								] 
							]
						]
					]
				],
			],
		]);
		$this->addFormGroup([
			'title' => 'Organization',
			'fields' => [
				[
					'type' => 'select2',
					'label' => 'Category',
					'name' => 'category_ID',
					'options' => [
						'choices' => \App\Category::orderBy('category_title')->lists('category_title', 'categoryId')->toArray(),
						'empty_value' => '=== Select ==='
					]
				],
			],
			'options' => [
				'position' => 'side' // or side or default
			],
		]);
	}
}