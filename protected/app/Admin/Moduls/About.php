<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;


class About extends Base
{ 
	use FormTrait;
	
	/***
	 *  your modul can hadle
	 * 
	 *  @var
	 */
	protected $_action = ['edit'];
		
	/***
	 *  your eloquent.
	 * 
	 *  @var
	 */
	 protected $_eloquent = 'App\Page';

	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Title',
					'name' => 'page_title',
				],
				[
					'type' => 'wysiwyg',
					'label' => 'Content',
					'name' => 'page_content',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
		$this->addFormGroup([
			'title' => 'Featured Image',
			'fields' => [
				[
					'type' => 'image',
					'name' => 'page_img',
					'instructions' => '* Minim image resolution: 800 x 800 (px)',
				],
			],
			'options' => [
				'position' => 'side' // or side or default
			],
		]);
	}
}