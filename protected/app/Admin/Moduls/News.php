<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;


class News extends Base
{
	use FormTrait, TableTrait;
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'News',
			'column' => function($record){
				$src = get_media_image_src($record->news_img);
				
				return '<span class="aspect-ratio aspect-ratio--square--50"><img class="aspect-ratio__content" src="'. $src[0] .'"></span>'. $record->news_title;
			},
		]);
		$this->addColum([
			'title' => 'Create At',
			'column' => 'created_at',
		]);
	}


	/***
	 *  This function will registe all field we needs.
	 *
	 */
	public function registerForm()
	{
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Title',
					'name' => 'news_title',
				],
				[
					'type' => 'wysiwyg',
					'label' => 'Content',
					'name' => 'news_content',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'textarea',
					'label' => 'Excerpt',
					'name' => 'news_excerpt',
				],
			],
			'options' => [
				'position' => 'default' // or side or default
			],
		]);
		$this->addFormGroup([
			'title' => 'Image',
			'fields' => [
				[
					'type' => 'image',
					'name' => 'news_img',
				],
			],
			'options' => [
				'position' => 'side' // or side or default
			],
		]);
	}
}