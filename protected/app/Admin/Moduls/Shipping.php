<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;


class Shipping extends Base
{
	use FormTrait, TableTrait;
	
	/***
	 *  your form layout.
	 * 
	 *  @var
	 */
	protected $_formView = 'admin.layout.form-full';
	
	protected $_tableView = 'admin.page.shipping-table';
	
		
	public function __construct()
	{
		$this->_states = config('states');
		$this->_countries = config('countries');
	}
	
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'Zone Name',
			'column' => 'shipping_title',
		]);
		$this->addColum([
			'title' => 'Regions',
			'column' => function($record){
				if( 1 == $record->shippingId )
				{
					return "This zone is used for shipping addresses that aren‘t included in any other shipping zone. <span class='text-danger'>Please don't delete.</span>";
				}

				$locations = '';
				foreach( $record->locations as $location )
				{
					$config = str_plural($location->location_type) . '.';
					$config .= str_replace(':', '.', $location->location_code);
					
					$locations .= '<span  class="label label-primary">'. config($config) .'</span>';
				}

				return '<span class="label-inline">'. $locations .'</span>';
			},
		]);
		$this->addColum([
			'title' => 'Cost',
			'column' => function($record){
				return currency($record->shipping_cost) .' AUD';
			},
		]);
	}

	
	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$regions = [];
		foreach($this->_countries as $code => $country)
		{
			$regions[ 'country:'. $code ] = $country;
			if( isset($this->_states[$code]) )
			{
				foreach($this->_states[$code] as $code2 => $state)
				{
					$regions[ 'state:'. $code .':'.$code2 ] = '&nbsp;&nbsp;&nbsp;&nbsp;'. $state;
				}
			}
		}
		
		
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Zone Name',
					'name' => 'shipping_title',
					'options' => [
						'rules' => 'required|min:3'
					]
				],
				[
					'type' => 'select2_multiple',
					'label' => 'Regions',
					'name' => 'shipping_regions',
					'options' => [
						'choices' => $regions,
					]
				],
				[
					'type' => 'text',
					'label' => 'Cost',
					'name' => 'shipping_cost',
					'instructions' => 'Per ('. config('cms.weight_unit') .')',
					'options' => [
						'label_after'=> 'AUD',
						'value' => function( $data ){
							if($data)
							{
								return number_format($data, 2, '.', '');								
							}
						}
					]
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
	}
}