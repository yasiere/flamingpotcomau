<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;


class User extends Base
{
	use FormTrait, TableTrait;
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'Product',
			'column' => function($record){
				$src = get_media_image_src(head($record->product_img));
				
				return '<span class="aspect-ratio aspect-ratio--square--50"><img class="aspect-ratio__content" src="'. $src[0] .'"></span>'. $record->product_title;
			},
		]);
		$this->addColum([
			'title' => 'Category',
			'column' => function($record){
				return $record->category->category_title;
			},
		]);
		$this->addColum([
			'title' => 'SKU',
			'column' => 'product_sku',
		]);
		$this->addColum([
			'title' => 'Price',
			'column' => 'product_price',
		]);/* 
		$this->addColum([
			'title' => 'Cover',
			'img_column' => 'product_img',
		]); */
	}
	
	
	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Username',
					'name' => 'user_login',
					'options' => [
						'rules' => 'required|min:5',
					]
				],
				[
					'type' => 'text',
					'label' => 'Name',
					'name' => 'display_name',
					'options' => [
						'rules' => 'required',
					]
				],
				[
					'type' => 'text',
					'label' => 'Nickname',
					'name' => 'user_nicename',
					'options' => [
						'rules' => 'required',
					]
				],
				[
					'type' => 'email',
					'label' => 'Email',
					'name' => 'user_email',
					'options' => [
						'rules' => 'required|email',
					]
				],
				[
					'type' => 'password',
					'label' => 'Password',
					'name' => 'password',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
	}
}