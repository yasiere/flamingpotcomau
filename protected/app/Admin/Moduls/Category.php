<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;
use App\Developer\Modul\TableTrait;

class Category extends Base
{
	use FormTrait, TableTrait;

	/***
	 *  your form layout.
	 * 
	 *  @var
	 */ 
	protected $_formView = 'admin.layout.form-full';
	

	public function registerTable()
	{
		$this->addColum([
			'title' => 'Title',
			'column' => 'category_title',
		]);
	}


	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Title',
					'name' => 'category_title',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
	}
}
