<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base; 
use App\Developer\Modul\TableTrait;


class Subscribe extends Base
{
	use TableTrait;
	
	protected $_action = ['show'];
	
	public function registerTable()
	{
		$this->addColum([
			'title' => 'Name',
			'column' => 'subscribe_name',
		]);
		$this->addColum([
			'title' => 'Email',
			'column' => 'subscribe_email',
		]); 
	}
}