<?php
namespace App\Admin\Moduls;

use App\Developer\Modul\Base;
use App\Developer\Modul\FormTrait;


class Page extends Base
{
	use FormTrait;
	
	/***
	 *  your modul can hadle
	 * 
	 *  @var
	 */
	protected $_action = ['edit'];
	
	/***
	 *  your form layout.
	 * 
	 *  @var
	 */ 
	protected $_formView = 'admin.layout.form-full';
	

	/***
	 *  This function will registe all field we needs.
	 *
	 *  @date: 04/10/16
	 *  @since: 1.0.2
	 */
	public function registerForm()
	{
		$this->addFormGroup([
			'fields' => [
				[
					'type' => 'text',
					'label' => 'Title',
					'name' => 'page_title',
				],
				[
					'type' => 'wysiwyg',
					'label' => 'Content',
					'name' => 'page_content',
				],
			],
			'options' => [
				'position' => 'primary' // or side or default
			],
		]);
	}
}