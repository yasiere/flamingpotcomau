<?php
namespace App\Http\Middleware\Site;

use Closure;

class SiteMiddlewareBefore
{
	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	public function handle($request, Closure $next)
	{
		new \App\Site\Menu();
		
		// register breadcrumbs for site/front end
		$breadcrumbs = new \App\Site\SiteBreadcrumbs;
		$breadcrumbs->register();
		
		return $next($request);
	}
}
