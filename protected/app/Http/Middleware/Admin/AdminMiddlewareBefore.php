<?php
namespace App\Http\Middleware\Admin;

use Closure;

class AdminMiddlewareBefore
{
	/**
	* Handle an incoming request.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \Closure  $next
	* @return mixed
	*/
	public function handle($request, Closure $next)
	{
		new \App\Admin\Menu();
		
		return $next($request);
	}
}
