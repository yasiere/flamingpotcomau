<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;

use App\ShippingLocation;
use App\Http\Controllers\Admin\BaseController;

class ShippingController extends BaseController
{
	/***
	 * Class Contructor
	 */
	public function __construct( FormBuilder $formBuilder, Request $request )
	{
		$this->init('shipping');
		$this->_request = $request;
		$this->_formBuilder = $formBuilder;
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function deleteShippingData($post_id)
	{ 
		$Eloquent = $this->_moduClass->getEloquent();
		$Eloquent::findOrFail($post_id)->delete();
		ShippingLocation::where('shipping_ID', $post_id)->delete();
		
		return redirect()->route('admin::show', 'shipping')
			->with('alert', ['class'=>'success', 'localization' => 'post.success.delete']);
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function deleteShippingRecords()
	{
		$post = $this->_request->input('checkbox');
		$key_name = $this->_moduClass->getTableKeyName(); 
		$Eloquent = $this->_moduClass->getEloquent(); 
		$Eloquent::whereIn($key_name, $post)->delete();
		ShippingLocation::whereIn('shipping_ID', $post)->delete();
		return redirect()->route('admin::show', 'shipping')
			->with('alert', ['class'=>'success', 'localization'=> 'post.success.delete']);
	} 
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function storeShipping( $post_id = null )
	{
		$form = $this->_moduClass->createForm($this->_formBuilder, false);
		$form = $form['form_builder'];
		if( ! $form->isValid() )
		{
			return redirect()
				->back()
				->withInput()
				->withErrors($form->getErrors())
				->with('alert', ['class'=>'danger', 'localization'=> 'post.errors.field']);
		} 
		
		$post = $this->_moduClass->filterPostRepeater($this->_request->except(['_token', 'files']));
		$Eloquent = $this->_moduClass->getEloquent();
		if( $post_id )
		{
			$Eloquent::findOrFail($post_id)->update($post);
			$localization = 'post.success.edit';
			ShippingLocation::where('shipping_ID', $post_id)->delete();
		}
		else
		{
			$post_id = $Eloquent::create($post)->getKey();
			$localization = 'post.success.add';
		}
		
		// add to location
		if( isset($post['shipping_regions']) )
		{
			$locations = $post['shipping_regions']; 
			foreach($locations as $location)
			{
				$location_parts = explode( ':', $location );
				$location_parts2 = isset($location_parts[2])? (':'. $location_parts[2]): ''; 
				$rows = [
					'shipping_ID' => $post_id,
					'location_type' => $location_parts[0],
					'location_code' => $location_parts[1] . $location_parts2,
				];
				
				ShippingLocation::create($rows);
			}
		}
		
		return redirect()->route('admin::form', ['modul' => 'shipping', 'post_id' => $post_id])
			->with('alert', ['class'=>'success', 'localization'=> $localization]);
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function shippingFormEdit( $post_id )
	{
		$modul = $this->_moduClass; 
		$Eloquent = $modul->getEloquent();
		$Eloquent = $Eloquent::findOrFail($post_id);
		$forms = $modul->createForm($this->_formBuilder, $Eloquent);
		
		$selected = [];
		$shippingId = $forms['form_builder']->getModel()->shippingId;
		$locations = ShippingLocation::where('shipping_ID', $shippingId)->get();
		foreach($locations as $location)
		{
			$selected[] = $location->location_type .':'. $location->location_code;
		}
		
		$forms['form_builder']->modify('shipping_regions', 'select2_multiple',[
			'selected' => $selected
		]);
		
		return view($modul->getFormView(), ['forms' => $forms]);
	}
}