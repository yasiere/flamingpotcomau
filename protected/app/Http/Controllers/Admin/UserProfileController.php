<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use Kris\LaravelFormBuilder\FormBuilder;

use Auth;
use App\User;
use App\Admin\Moduls\User as UserModul;

class UserProfileController extends Controller
{
	public function profile(FormBuilder $formBuilder)
	{
		$Auth = Auth::user();
		$Modul = new UserModul;
		$Eloquent = User::findOrFail($Auth->ID);
		$forms = $Modul->createForm($formBuilder, $Eloquent);
		
		$forms['form_builder']
			->modify('user_login', 'text', [
				'rules' => false,
				'attr' => ['disabled' => true],
			])
			->modify('password', 'password', [
				'value' => '',
				'help_block' => [
					'text' => '*Leave blank not  want to change.',
				]
			]);
			
		$dashboard = [
			'icon' => 'user',
			'title' => 'Profile',
			'post_id' => false,
			'modul_url' => 'user/profile',
			'modul_name' => 'userProfile',
			'button.add'=> false,
			'button.edit'=> true,
			'button.delete'=> false, 
		];
		
		return view('admin.page.user-profile', ['forms' => $forms, 'dashboard'=> $dashboard]);
	}
	
	
	public function updateProfile( FormBuilder $formBuilder, Request $request )
	{
		$Modul = new UserModul;
		$form = $Modul->createForm($formBuilder, false);
		$form = $form['form_builder'];
		$form->validate(['user_login' => '']);

		$new_pass = $request->input('password');
		if($new_pass)
		{
			$form->validate(['password' => 'required|min:5', 'user_login' => '']);
		}

		if( ! $form->isValid() )
		{
			return redirect()
				->back()
				->withInput($request->except('password'))
				->withErrors($form->getErrors())
				->with('alert', ['class'=>'danger', 'localization'=> 'post.errors.field']);
		}

		$post = $request->except('_token', 'user_login');
		if($new_pass)
		{
			$post['password'] = bcrypt($post['password']);
		}
		
		$Auth = Auth::user();
		User::findOrFail($Auth->ID)->update($post);
		
		return redirect()
			->route('admin::userProfile')
			->with('alert', ['class'=>'success', 'localization'=> 'post.success.edit']);
	}
}