<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;

use App\Developer\Modul\ControllerTrait;


class BaseController extends Controller
{
	use ControllerTrait; 
	
	/***
	 * Class Contructor
	 */
	public function __construct( FormBuilder $formBuilder, Request $request )
	{
		$this->init();
		$this->_request = $request;
		$this->_formBuilder = $formBuilder;
	}
	
	
	/***
	 *  This function will create table record and display.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function showTable()
	{ 
		$tables = $this->_moduClass->getRenderedTable(); 
		$template = $this->_moduClass->getTableView();
		
		return view($template, ['tables' => $tables]);
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function form( $modul, $post_id = false )
	{
		$modul = $this->_moduClass;
		if( $post_id )
		{
			$Eloquent = $modul->getEloquent();
			$post_id = $Eloquent::findOrFail($post_id);
		}

		$forms = $modul->createForm($this->_formBuilder, $post_id);
		return view($modul->getFormView(), ['forms' => $forms]);
	} 
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function deleteData( $modul, $post_id )
	{
		$Eloquent = $this->_moduClass->getEloquent();
		$Eloquent::findOrFail($post_id)->delete();
		return redirect()
			->route('admin::show', $modul)
			->with('alert', ['class'=>'success', 'localization' => 'post.success.delete']);
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function deleteRecords( $modul )
	{
		$post = $this->_request->input('checkbox');
		$key_name = $this->_moduClass->getTableKeyName(); 
		$Eloquent = $this->_moduClass->getEloquent(); 
		$Eloquent::whereIn($key_name, $post)->delete();
		return redirect()->route('admin::show', $modul)
			->with('alert', ['class'=>'success', 'localization'=> 'post.success.delete']);
	}
	
	
	/***
	 *  This function will handle form add.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function storeData( $modul, $post_id = null )
	{
		$form = $this->_moduClass->createForm($this->_formBuilder, false);
		$form = $form['form_builder'];
		if( ! $form->isValid() )
		{
			return redirect()
				->back()
				->withInput()
				->withErrors($form->getErrors())
				->with('alert', ['class'=>'danger', 'localization'=> 'post.errors.field']);
		} 
		
		$post = $this->_moduClass->filterPostRepeater($this->_request->except(['_token', 'files']));
		$Eloquent = $this->_moduClass->getEloquent();
		if( $post_id )
		{
			
			$Eloquent::findOrFail($post_id)->update($post);
			$localization = 'post.success.edit';
		}
		else
		{
			$post_id = $Eloquent::create($post)->getKey();
			$localization = 'post.success.add';
		}
		
		return redirect()->route('admin::form', [$modul, $post_id])
			->with('alert', ['class'=>'success', 'localization'=> $localization]);
	}
}