<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
class DashboardController extends Controller
{
	/***
	 *  This function will handle and display the admin dashboard.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function showDashboard()
	{
		return view('admin.page.dashboard');
	}
	
	
	/***
	 *  This function will redirect to not found.
	 * 
	 *  @date	01/10/16
	 *  @since	1.0.1
	 */
	public function page404()
	{
		return view('errors.404');
	}
}