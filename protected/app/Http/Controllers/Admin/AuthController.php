<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
	 | User registration on admin hadled by admin controller
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo;


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectAfterLogout;

    /**
     * Show the application login form.
	  *
     * @var string
     */
    protected $loginView = 'admin.page.login';

    /**
     * Username field name.
	  *
     * @var string
     */
    protected $username = 'user_login';

    /**
     * Password field name.
	  *
     * @var string
    protected $passwordField = 'password';
     */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = $this->redirectAfterLogout = config('cms.cms_uri');
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'getLogout']]);
    }

    /**
     * Override default laravel validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     
    protected function validateLogin(Request $request)
    {
		$this->validate($request, [
			$this->username => 'required', $this->passwordField => 'required',
		]);
    }*/

    /**
     * Override default laravel Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
    protected function getCredentials(Request $request)
    {
        return $request->only($this->username, $this->passwordField);
    }
     */

    /**
     * Override default laravel Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username, 'remember'))
            ->withErrors([
					'type' => 'danger',
					'title' => 'error !!!',
					'message' => $this->getFailedLoginMessage(),
            ]);
    }
}
