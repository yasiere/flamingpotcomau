<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Media;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Developer\UploadFileControllerTrait;

class UploadFileController extends Controller
{
	use UploadFileControllerTrait;

    protected $options;
	
    protected $request;

    // PHP File Upload error message codes:
    // http://php.net/manual/en/features.file-upload.errors.php
    protected $error_messages = array(
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini',
        'max_file_size' => 'File is too big',
        'min_file_size' => 'File is too small',
        'accept_file_types' => 'Filetype not allowed',
        'max_number_of_files' => 'Maximum number of files exceeded',
        'max_width' => 'Image exceeds maximum width',
        'min_width' => 'Image requires a minimum width',
        'max_height' => 'Image exceeds maximum height',
        'min_height' => 'Image requires a minimum height',
        'abort' => 'File upload aborted',
        'image_resize' => 'Failed to resize image'
    );

    protected $image_objects = array();

    public function __construct(Request $request)
	{
        $this->request = $request;
        $this->response = array(); 
		  $size = config('site.image_size');
		  $this->setOptions(['image_versions' => $size]);
    }

	/***
	 * Handle media view		
	 */
	public function home()
	{
		return view('developer.media');
	}
	
	/***
	 * Handle server method
	 */
	public function methodHead()
	{
		$this->head();
	}

	/***
	 * Handle server method
	 */
	public function methodGet()
	{ 
		$arr = $this->get(false);
		$media = Media::all()->keyBy('media_name')->toArray();
		$files = $arr['files'];
		foreach( $files as $key => $file )
		{
			if( isset($media[ $file->name ]) )
			{
				$file->id = $media[ $file->name ]['mediaId'];
				$file->meta = $media[ $file->name ]['media_meta'];
				$files[ $key ] = $file;
			}
		}
		
		$arr['files'] = $files;
		
		return collect( $arr )->toJson();
	}

	/***
	 * Handle server method
	 */
	public function methodPost()
	{
		$arr = $this->post(false);
		if( isset($arr['files']) )
		{
			$media = new Media;
			$files = $arr['files'];
			foreach( $files as $key => $file )
			{
				$meta = $this->reinitMeta(['title' => $file->name], $file);
				$media->media_meta = $meta;
				$media->media_name = $file->name;
				$media->save();
				$files[ $key ]->id = $media->getKey();
				$files[ $key ]->meta = $meta;
			}
			
			$arr['files'] = $files ;

			return collect($arr)->toJson();
		}
	}
	
	public function update( Request $request )
	{
		$id = $request->input('id');
		$post = $request->except(['_token', 'id', 'url']);
		$data = Media::find($id);
		$size = $data->first()->media_meta;
		$post['size'] = $size['size'];
		$data->update(['media_meta'=>$post]);
		
		return $data->toJson();
	}	

	public function methodDelete()
	{
		$arr = $this->delete(false);
		$files = array_where($arr, function($key, $value) {
			return ($value === true);
		});
		
		$files = array_keys($files);
		Media::whereIn('media_name', $files)->delete();
		return collect( $arr )->toJson();
	}
}
