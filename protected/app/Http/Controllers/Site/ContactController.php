<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;
use Kris\LaravelFormBuilder\FormBuilder;

class ContactController extends Controller
{
	public function page(FormBuilder $formBuilder)
	{
		$contact = \App\Page::where('pageId', 2)->first();
		$form = $formBuilder->create('App\Site\ContactForm');
		return view('theme.contact', compact(['contact','form']));
	}
	
	public function mail(FormBuilder $formBuilder, Request $request)
	{
		$form = $formBuilder->create('App\Site\ContactForm');
		if( ! $form->isValid() )
		{
			return redirect()
				->back()
				->withInput()
				->withErrors($form->getErrors());
		} 
		
		$post = $request->except(['_token']);
		/* 
		Mail::send('emails.contact', ['post'=> $post], function($message) use($post) {
			$message->to(config('cms.site.contact_mail'), 'Admin');
			$message->from($post['cemail'], $post['fname'] . $post['lname']);
			$message->subject('Contact Flamingpot '. $post['csubject']);
		}); 
		*/

		// use this old, because laravel driver mail not stabel
		$mail  = "<h4>From: {$request->fname} {$request->lname}</h4>";
		$mail .= "<h4>Email Address: {$request->cemail}</h4>";
		$mail .= "<h4>Subject: {$request->csubject}</h4>";
		$mail .= "<h4>Message:</h4><div>{$request->cmessage}</div>";

		$headers  = 'From: info@vdunia.com' . "\r\n";
		$headers .= 'Reply-To: info@vdunia.com' . "\r\n";
		$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
		$headers .= "MIME-Version: 1.0". PHP_EOL;
		$headers .= "Content-type: text/html; charset=ISO-8859-1" . PHP_EOL;
			
		mail("fruity.tester@yahoo.com", "Contact form Flamingpot", $mail, $headers);

		return redirect()->route('contact.page')->with('mail', 'success');
	}
}
