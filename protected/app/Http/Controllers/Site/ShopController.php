<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\Category;

class ShopController extends Controller
{
	public function page(Request $request)
	{
		$products = Product::orderBy('productId', 'DESC')->get();

		$categories = Category::orderBy('category_title', 'ASC')->get();
		$filter = '';
		if( $request->has('key') )
		{
			$filter = $request->get('key');
		}

		return view('theme.shop', compact(['categories', 'products', 'filter']));
	}
	
	
	public function detail( $slug, Request $request )
	{
		$product = Product::where('product_slug', $slug)->firstOrFail(); 
		$prev = Product::where('productId', '<', $product->productId)->first();
		$next = Product::where('productId', '>', $product->productId)->first();
		
		$pager = array_dot([
			'prev' => [
				'link' => '#',
				'class' => 'disabled'
			],
			'next' => [
				'link' => '#',
				'class' => 'disabled'
			],
		]);
		
		if( ! empty($prev) )
		{
			$pager['prev.class'] = '';
			$pager['prev.link'] = route('shop.detail', $prev->product_slug);
		}
		
		if( ! empty($next) )
		{
			$pager['next.class'] = '';
			$pager['next.link'] = route('shop.detail', $next->product_slug);
		}
		
		if( $request->ajax() )
		{
			return view('theme.quick-view', compact(['product', 'pager']));
		}

		return view('theme.single', compact(['product', 'pager']));
	}
	
	
	public function shoppingcart()
	{
		
		return view('theme.shopcart');
	}
}
