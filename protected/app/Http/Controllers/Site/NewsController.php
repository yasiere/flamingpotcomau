<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\News;

class NewsController extends Controller
{
	public function index()
	{
		$news = News::orderBy('created_at', 'DESC')->get();
		return view('theme.news', compact(['news']));
	}
	
	public function single($slug)
	{
		$news = News::where('news_slug', $slug)->firstOrFail();
		return view('theme.news-single', compact(['news']));
	}
}
