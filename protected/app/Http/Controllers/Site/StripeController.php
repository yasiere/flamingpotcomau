<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Cart;
use Session;
use App\Orders;
use App\Site\CheckoutTrait;
use App\Developer\InvoiceCodeTrait;

use Stripe\Stripe;
use Stripe\Charge;


class StripeController extends Controller
{
	use InvoiceCodeTrait, CheckoutTrait;
	
	/***D
	 * This function will handle customet carge use Stripe. 
	 *
	 *  @since:	1.0.3
	 *  @created: 19/10/16 
	 */
	public function carge(Request $request)
	{
		Stripe::setApiKey(config('services.stripe.secret')); 
		$cart = Cart::instance('default');
		$amount = ($cart->total(2, '.', '') + $request->old('shippingCost')) * config('services.stripe.amount');
		try {
			$charge = Charge::create([
				"amount" => $amount,
				"source" => $request->old('stripeToken'),
				"currency" => config('cms.currency.code'),
				"description" => "Charge for {$request->old('shippingName')}, {$request->old('shippingEmail')}"
			]);
		}
		catch(\Exception $e) {
			return redirect()
				->route('checkout.page')
				->with('alert', ['class'=>'danger', 'message' => $e->getMessage()])
				->send();
		}
		
		$cost = Session::get('meta_cost');
		$total = $cart->total() + $cost['shipping'];
		// payment success
		$order = [
			'invoice_code' => $this->invoiceCode('\App\Orders', 'invoice_code'),
			'order_status' => 'Paid',
			'payment' => [
				'code' => $charge->id,
				'method' => 'Stipe',
			],
			'order_meta' => [
				'shipping' => Session::get('meta_shipping'),
				'cost' => [
					'tax' => $cart->tax(),
					'total' => $total,
					'subtotal' => $cart->subtotal(),
					'discount' => '',
					'shipping' => $cost['shipping'],
					'currency' => $cost['currency'],
				]
			]
		];
		
		// store to oder table
		$orderId = Orders::create($order)->getKey();
		$this->storeDetailOrder($cart, $orderId, $order);
	}
}