<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request; 
use App\Http\Requests;
use App\Http\Controllers\Controller;

// base
use Cart;
use Session;
use Kris\LaravelFormBuilder\FormBuilder;

use App\Developer\InvoiceCodeTrait;
   
class CheckoutController extends Controller
{
	use InvoiceCodeTrait;
	
	public function page(FormBuilder $formBuilder)
	{ 
		$card_form = $formBuilder->create('App\Site\CheckoutCardForm');
		$billing_form = $formBuilder->create('App\Site\CheckoutBillingForm');
		$shipping_form = $formBuilder->create('App\Site\CheckoutShippingForm');
		
		return view('theme.checkout', compact(['card_form', 'shipping_form', 'billing_form']));
	} 
	
	
	public function success()
	{
		return view('theme.checkout-done');
	}
	
	 
	public function proses(Request $request, FormBuilder $formBuilder)
	{
		$form = $formBuilder->create('App\Site\CheckoutShippingForm');
		if( ! $form->isValid() )
		{
			return redirect()
				->back()
				->withInput()
				->withErrors($form->getErrors())
				->with('alert', ['class'=>'danger', 'message'=> trans('post.errors.field')]);
		}
		
		Session::put('meta_shipping', [
			'name' => $request->shippingName,
			'email' => $request->shippingEmail,
			'phone' => $request->shippingPhone,
			'street' => $request->shippingAddress,
			'city' => $request->shippingCity,
			'state' => $request->shippingState,
			'zip_code' => $request->shippingZip,
			'country' => $request->shippingCountry,
		]);
		Session::put('meta_cost', [
			'shipping' => $request->shippingCost,
			'currency' => config('cms.currency.code'),
		]);
		
		if( $request->has('stripeToken') && ('stripe' == $request->paymentMethod) )
		{
			return redirect()->route('stripe.carge')->withInput()->send();
		}
		
		return redirect()->route('paypal.pay')->withInput()->send();
	}
	
	
	
	public function getState( $code )
	{
		$state = config("states.{$code}");		
		if(! is_array($state) )
		{
			return '<input class="form-control" placeholder="State" name="shippingState" id="shippingState" type="text">';
		}

		$select = '';
		foreach( $state as $key => $val )
		{
			$select .= '<option value="'. $code .':'. $key .'">'. $key .'</option>';
		}
		
		return '<select id="shippingState" class="form-control" name="shippingState">'. $select .'</select>';		
	}
	
	
	
	
	public function shippingPrice( Request $request, $code )
	{
		$cost = \App\ShippingLocation::where('location_code', $code)->first();
		if( $cost )
		{
			$cost = $cost->shipping->shipping_cost;
		}
		else
		{
			$cost = \App\Shipping::where('shippingId', 1)->first();
			$cost = $cost->shipping_cost;
		}
		
		$weight = 0;
		foreach(Cart::content() as $item)
		{
			$weight += ($item->options->weight * $item->qty);
		}
		
		$cost *= $weight;
		$total = Cart::total(2, '.', '') + $cost;
		
		if( $request->ajax() )
		{ 
			return collect([
				'cost' => currency($cost),
				'total' => currency($total),
				'number_cost' => $cost
			])->toJson();
		} 
	}
}
