<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Cart;
use App\Product;
use App\Category;

class CartController extends Controller
{
	/* protected $request;
	
	public function __construct(Request $request)
	{
		$this->request = $request;
	} */
	
    public function add(Request $request)
	{
		$id  = $request->input('key');
		$qty = $request->input('qty');
		$opt = $request->input('opt');

		$variant = [];
		foreach( (array)$opt as $key => $val )
		{
			$variant[] = "{$key}: {$val}";
		}

		$cart = Cart::search(function($cartItem, $rowId) use($id){
			return $cartItem->id == $id;
		}); 

		if($cart->isEmpty())
		{
			$product = Product::findOrFail($id);
			$item = [
				'id' => $product->productId,
				'qty' => $qty,
				'name' => $product->product_title,
				'price' => $product->product_price,
				'options' => [
					'weight' => $product->product_weight,
					'variant' => $variant,
					'slug' => $product->product_slug,
					'image' => head($product->product_img), 
				]
			];
			
			Cart::add($item);
		}
		else
		{
			$cart = $cart->first();
			$qty += $cart->qty;
			$options = $cart->options;
			$options['variant'] = $variant;
			Cart::update($cart->rowId, $qty); 
			Cart::update($cart->rowId, ['options' => $options]);
		}

		
		return Cart::count();
	}
	
	
	public function update($rowId, $qty)
	{
		Cart::update($rowId, $qty); 
		$cart = Cart::instance('default');
		return collect([
			'tax' => $cart->tax( 2, '.' ,','),
			'total' => Cart::total( 2, '.' ,','),
			'qtyTotal' => $cart->count(),
			'subTotal' => $cart->subtotal( 2, '.' ,','),
			'itemTotal' =>$cart->get($rowId)->subtotal( 2, '.' ,','),
		])->toJson();
	}
	
	public function remove($rowId)
	{
		Cart::remove($rowId);
		return collect([
			'tax' => Cart::tax(),
			'total' => Cart::total( 2, '.' ,','),
			'qtyTotal' => Cart::count(),
			'subTotal' => Cart::subtotal( 2, '.' ,','),
		])->toJson();
	}
	
	public function clean()
	{
		Cart::destroy();
	}
}
