<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Cart;
use Session;
use App\Orders;
use App\Site\CheckoutTrait;
use App\Developer\InvoiceCodeTrait;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;


class PaypalController extends Controller
{
	use InvoiceCodeTrait, CheckoutTrait;
	
	protected $_apiContext;
	
	protected $_currencyCode;

	public function __construct( $requestId = null )
	{
        $token = new OAuthTokenCredential(config('services.paypal.key'), config('services.paypal.secret'));
		$this->_apiContext = new ApiContext($token, $requestId);
		$mode = config('services.paypal.mode');
		$endPoint = ('live' == $mode)? 'https://api.paypal.com': 'https://api.sandbox.paypal.com';
		
		$this->_apiContext->setConfig([
            'mode' => $mode,
            'log.LogLevel' => 'FINE',
            'service.EndPoint' => $endPoint,
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => config('services.paypal.log'),
            'log.FileName' => storage_path('logs\paypal.log'),
        ]);
		
		$this->_currencyCode = config('cms.currency.code');
	}

	
	/***D
	 * This function will handle paypal payment.
	 *
	 *  @since:	1.0.3
	 *  @created: 19/10/16 
	 */
	public function pay(Request $request)
	{
		$cart = Cart::instance('default');

		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		
		$items = [];
		foreach( $cart->content() as $content )
		{ 
			$item = new Item();
			$item->setCurrency($this->_currencyCode)
                ->setPrice($content->price)
                ->setQuantity($content->qty)
				->setName($content->name);

			$items[] = $item;
		}
		
		$state = explode(':', $request->old('shippingState'));
		$state = isset($state[1])? $state[1]: $request->old('shippingState');
		$shippingAddress = new ShippingAddress();
		$shippingAddress->setState($state)
			->setCity($request->old('shippingCity'))
			->setLine1($request->old('shippingAddress'))
			->setPostalCode($request->old('shippingZip'))
			->setRecipientName($request->old('shippingName'))
			->setCountryCode($request->old('shippingCountry'));
			
		if( $request->old('shippingPhone') )
		{
			$shippingAddress->setPhone($request->old('shippingPhone'));
		}	
			 
		// item list paypal
        $itemList = new itemList();
        $itemList->setItems($items);
		$itemList->setShippingAddress($shippingAddress);
		
		$details = new Details();
		$details->setShipping($request->old('shippingCost'))
			->setTax($cart->tax( 2, '.' ,''))
			->setSubtotal($cart->subtotal( 2, '.' ,''));
			
		$total = $cart->total(2, '.', '') + $request->old('shippingCost');
		$amount = new Amount();
		$amount->setCurrency($this->_currencyCode)
			->setTotal($total)
			->setDetails($details);
			
		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($itemList)
			->setDescription("Charge for {$request->old('shippingName')}, {$request->old('shippingEmail')}");
		
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl(route('paypal.done'));
		$redirectUrls->setCancelUrl(route('checkout.page'));		
		
		$payment = new Payment();
		$payment->setIntent('sale');
		$payment->setPayer($payer);
		$payment->setRedirectUrls($redirectUrls);
		$payment->setTransactions([$transaction]);
		
		try {
			$response = $payment->create($this->_apiContext);
			$redirectUrl = $response->links[1]->href;
			return redirect()->to($redirectUrl)->send(); 
        }
		catch (\Exception $e) {
			$errors = json_decode($e->getData(), true);
			$errors = array_get($errors, 'details', []);
			$message = '';
			foreach( $errors as $error )
			{
				$message .= $error['field'] .' '. $error['issue'] .'. '; 
			}
			
			return redirect()
				->route('checkout.page')
				->with('alert', ['class'=>'danger', 'message' => $message])
				->send();
        }
	}
	
	
	/***D
	 * This function will handle after paypal payment done.
	 *
	 *  @since:	1.0.3
	 *  @created: 19/10/16 
	 */
	public function done(Request $request)
	{
		// get request data
		$token = $request->get('token');
		$PayerId = $request->get('PayerID');
		$paymentId = $request->get('paymentId');
		
		$payment = Payment::get($paymentId, $this->_apiContext);
		
		$execution = new PaymentExecution();
		$execution->setPayerId($PayerId);
		
		try {
			$result = $payment->execute($execution, $this->_apiContext);
        }
		catch (\Exception $e) {
			return redirect()
				->route('checkout.page')
				->with('alert', ['class'=>'danger', 'message' => 'paypal payment failed.'])
				->send();
        }

		if( "approved" != $result->getState() )
		{
			// payment failed
			return redirect()
				->route('checkout.page')
				->with('alert', ['class'=>'danger', 'message' => 'paypal payment failed.'])
				->send();
		}
		
		$cart = Cart::instance('default');
		$cost = Session::get('meta_cost');
		$total = $cart->total() + $cost['shipping'];
		// payment success
		$order = [
			'invoice_code' => $this->invoiceCode('\App\Orders', 'invoice_code'),
			'order_status' => 'Paid',
			'payment' => [
				'code' => $result->getId(),
				'method' => 'Paypal',
			],
			'order_meta' => [
				'shipping' => Session::get('meta_shipping'),
				'cost' => [
					'tax' => $cart->tax(),
					'total' => $total,
					'subtotal' => $cart->subtotal(),
					'discount' => '',
					'shipping' => $cost['shipping'],
					'currency' => $cost['currency'],
				]
			]
		];
		
		// store to oder table
		$orderId = Orders::create($order)->getKey();
		$this->storeDetailOrder($cart, $orderId, $order);
	}
}