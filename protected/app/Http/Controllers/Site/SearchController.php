<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
class SearchController extends Controller
{
    
	public function page(Request $request)
	{
		$search = $request->get('sq');
		$products = Product::where('product_title', 'like', "%{$search}%")
				->orWhere('product_desc', 'like', "%{$search}%")->get();

		return view('theme.search', compact('products'));
	}
}
