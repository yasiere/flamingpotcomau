<?php
namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;


class PageController extends Controller
{
	/**
	* Show the home page.
	*
	*/
	public function home()
	{
		// Banner
		$banner = [];
		$records = \App\Home::all();
		foreach($records as $record)
		{
			$banner[$record->homeId] = [
				'link' => '',
				'image' => '',
				'text' => $record->banner_text,
				'title' => $record->banner_title,
			];
			if($record->banner_link)
			{
				$link = route('shop.page');
				if( $record->banner_category )
				{
					$link .= '?key='. $record->category->category_slug; 
				}
				
				$banner[$record->homeId]['link'] = "<a href='{$link}'>{$record->banner_link}</a>";
			} 
			
			if( $record->banner_img )
			{
				$banner[$record->homeId]['image'] = get_media_image_src( $record->banner_img, 'original', true);
			}
		}
		
		// Donnut
		$donnuts = \App\Product::where('category_ID', '5')->orderBy('productId', 'DESC')->take(2)->get();

		// cupcakes
		$cupcakes =\App\Product::where('category_ID', '2')->orderBy('productId', 'DESC')->take(2)->get();
		
		// galleries
		$galleries = \App\Product::orderBy('productId', 'DESC')->take(6)->get();
		
		return view('theme.home', compact(['banner', 'donnuts', 'cupcakes', 'galleries']));
	}
	
	
	/**
	* Show the about page.
	*
	*/
	public function about()
	{
		$about = \App\Page::where('pageId', 1)->first();
		
		return view('theme.about', compact('about'));
	} 
	
	
	/**
	* Show the about page.
	*
	*/
	public function faq()
	{
		$faq = \App\Page::where('pageId', 3)->first();
		
		return view('theme.faq', compact('faq'));
	} 
	
	
	public function subscribe(Request $request)
	{
		$name = $request->input('fname') .' '. $request->input('lname');
		
		$subscribe = new \App\Subscribe;

        $subscribe->subscribe_name = $name;
        $subscribe->subscribe_email = $request->input('email');

        $subscribe->save();
	}
	
}

