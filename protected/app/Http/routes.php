<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/**************************************
 * Make Your rute here
 *
 **************************************/
 
 
/***D 
 *----------------------------------------------------------------
 * Define route for handle home/index.
 *----------------------------------------------------------------
 */
Route::get('/', [
	'as' => 'home.page',
	'middleware' => 'site',
	'uses' => 'Site\PageController@home'
]); 

/***D 
 *----------------------------------------------------------------
 * Handle news.
 *----------------------------------------------------------------
 */

Route::get('/news',[
	'as' => 'news.index',
	'middleware' => 'site',
	'uses' => 'Site\NewsController@index'
]);
Route::get('news/{slug}',[
	'as' => 'news.single',
	'middleware' => 'site',
	'uses' => 'Site\NewsController@single'
]);

/***D 
 *----------------------------------------------------------------
 * Handle Subscribe.
 *----------------------------------------------------------------
 */

Route::post('subscribe-news',[
	'as' => 'subscribe',
	'uses' => 'Site\PageController@subscribe'
]);

/***D 
 *----------------------------------------------------------------
 * Define route for handle about.
 *----------------------------------------------------------------
 */
Route::get('about', [
	'as' => 'about.page',
	'middleware' => 'site',
	'uses' => 'Site\PageController@about'
]); 

/***D 
 *----------------------------------------------------------------
 * Define route for handle faq.
 *----------------------------------------------------------------
 */
Route::get('faq', [
	'as' => 'faq.page',
	'middleware' => 'site',
	'uses' => 'Site\PageController@faq'
]);

/***D 
 *----------------------------------------------------------------
 * Define route for handle faq.
 *----------------------------------------------------------------
 */
Route::get('search', [
	'as' => 'search.page',
	'middleware' => 'site',
	'uses' => 'Site\SearchController@page'
]);


/***D 
 *----------------------------------------------------------------
 * Define route for handle shop.
 *----------------------------------------------------------------
 */
Route::get('shop', [
	'as' => 'shop.page',
	'middleware' => 'site',
	'uses' => 'Site\ShopController@page'
]);
Route::get('shop/{slug}', [
	'as' => 'shop.detail',
	'middleware' => 'site',
	'uses' => 'Site\ShopController@detail'
]);
Route::get('shoppingcart', [
	'as' => 'shop.shoppingcart',
	'middleware' => 'site',
	'uses' => 'Site\ShopController@shoppingcart'
]);

/***D 
 *----------------------------------------------------------------
 * Define route for handle contact.
 *----------------------------------------------------------------
 */
Route::get('contact', [
	'as' => 'contact.page',
	'middleware' => 'site',
	'uses' => 'Site\ContactController@page'
]);
Route::post('contact/send-mail',[
	'as' => 'contact.mail',
	'uses' => 'Site\ContactController@mail'
]);

 
/***D 
 *----------------------------------------------------------------
 * Define route for handle checkout.
 *----------------------------------------------------------------
 */
Route::get('checkout', [
	'as' => 'checkout.page',
	'middleware' => 'site',
	'uses' => 'Site\CheckoutController@page'
]);
Route::get('checkout-success', [
	'as' => 'checkout.success',
	'uses' => 'Site\CheckoutController@success'
]);
Route::post('checkout-proses', [
	'as' => 'checkout.proses',
	'uses' => 'Site\CheckoutController@proses'
]);
Route::get(
	'shipping-price/{code}',
	'Site\CheckoutController@shippingPrice'
);
Route::get(
	'get-state/{code}',
	'Site\CheckoutController@getState'
);
/***D 
 *----------------------------------------------------------------
 * Define route for handle payment use Stripe.
 *----------------------------------------------------------------
 */
Route::get('stripe-carge',[
	'as' => 'stripe.carge',
	'uses' => 'Site\StripeController@carge'
]);


/***D 
 *----------------------------------------------------------------
 * Define route for handle payment use Paypal.
 *----------------------------------------------------------------
 */
Route::get('paypal-done',[
	'as' => 'paypal.done',
	'uses' => 'Site\PaypalController@done'
]);
Route::get('paypal-pay',[
	'as' => 'paypal.pay',
	'uses' => 'Site\PaypalController@pay'
]);

/***D 
 *----------------------------------------------------------------
 * Define route for handle cart.
 *----------------------------------------------------------------
 */
Route::get('clean-cart',[
	'as' => 'cart.clean',
	'uses' => 'Site\CartController@clean'
]);
Route::get('remove-cart-item/{rowId}',[
	'as' => 'cart.remove',
	'uses' => 'Site\CartController@remove'
]);
Route::get('update-cart-item/{rowId}/{qty}',[
	'as' => 'cart.update',
	'uses' => 'Site\CartController@update'
]);
Route::post('add-to-cart',[
	'as' => 'cart.add',
	'uses' => 'Site\CartController@add'
]);


/***D
 -----------------------------------------------------------------
 * Route for bundled Cms backend
 -----------------------------------------------------------------
 *
 * Edit only advance devoleper.
 * Don't make same name with Cms routes 
 */



/***D 
 *----------------------------------------------------------------
 * Define route for handle admin shipping.
 *----------------------------------------------------------------
 */
Route::group([
	'prefix' => config('cms.cms_uri'),
	'middleware' => 'admin',
	'namespace' => 'Admin',
	'as' => 'admin::',
	], function()
	{
		Route::post('store/shipping/{post_id?}', [
			'as' => 'storeShipping',
			'uses' => 'ShippingController@storeShipping'
		]);
		Route::get('add/shipping/{post_id}', [
			'as' => 'shippingFormEdit',
			'uses' => 'ShippingController@shippingFormEdit'
		]);
		Route::get('delete/shipping/{post_id}', [
			'as' => 'deleteShippingData',
			'uses' => 'ShippingController@deleteShippingData'
		]);
		Route::post('delete/shipping', [
			'as' => 'deleteShippingRecords',
			'uses' => 'ShippingController@deleteShippingRecords'
		]);
});

/***D 
 *----------------------------------------------------------------
 * Define route for handle admin users.
 *----------------------------------------------------------------
 */
Route::group([
	'prefix' => config('cms.cms_uri'),
	'middleware' => 'admin',
	'namespace' => 'Admin',
	'as' => 'admin::',
	], function()
	{
		Route::get('user/profile', [
			'as' => 'userProfile',
			'uses' => 'UserProfileController@profile'
		]);
		Route::post('update/user/profile', [
			'as' => 'updateProfile',
			'uses' => 'UserProfileController@updateProfile'
		]);
});
 
 
// Route for global admin for modul action 
Route::group([
	'prefix' => config('cms.cms_uri'),
	'middleware' => 'admin',
	'namespace' => 'Admin',
	'as' => 'admin::',
	], function()
	{
		// Dashboard Controller
		Route::get('/', [
			'as' => 'dashboard',
			'uses' => 'DashboardController@showDashboard'
		]);
		Route::get('page404', [
			'as' => 'page404',
			'uses' => 'DashboardController@page404'
		]);
		
		// Base Controller
		//--------------------------------------------------------------------------------
		Route::get('/{modul}', [
			'as' => 'show',
			'uses' => 'BaseController@showTable'
		]);
		Route::get('add/{modul}/{post_id?}', [
			'as' => 'form',
			'uses' => 'BaseController@form'
		]);
		Route::get('delete/{modul}/{post_id}', [
			'as' => 'delete',
			'uses' => 'BaseController@deleteData'
		]);
		Route::post('delete/{modul}', [
			'as' => 'deletes',
			'uses' => 'BaseController@deleteRecords'
		]);
		Route::post('store/{modul}/{post_id?}', [
			'as' => 'store',
			'uses' => 'BaseController@storeData'
		]); 
});

//Route for Media library
Route::group(['prefix' => 'media', 'as' => 'media::', 'middleware' => 'auth'], function()
{
	Route::get('/', ['as' => 'home', 'uses' => 'UploadFileController@home']);
	Route::get('method', ['as' => 'method', 'uses' => 'UploadFileController@methodGet']);
	Route::post('method', ['as' => 'method', 'uses' => 'UploadFileController@methodPost']);
	Route::post('update', ['as' => 'update', 'uses' => 'UploadFileController@update']);
	Route::delete('method', ['as' => 'method', 'uses' => 'UploadFileController@methodDelete']);
});

// Route for Authentication user
Route::get('login', 'Admin\AuthController@getLogin');
Route::get('logout', 'Admin\AuthController@getLogout');
Route::post('login', 'Admin\AuthController@postLogin');