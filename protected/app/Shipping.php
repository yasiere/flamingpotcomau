<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
	/**
	* The table name associated with the model.
	*
	* @var string
	*/
	//protected $table  = 'shipping';
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'shippingId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'shipping_title',
		'shipping_cost',
	];
	
	protected $casts = [
		'shipping_cost' => 'float',
	];
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function locations()
	{
		return $this->hasMany('App\shippingLocation', 'shipping_ID');
	}
}
