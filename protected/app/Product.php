<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Product extends Model
{
	use Sluggable;
	
	/**
	* The primary key table associated with the model.
	*
	* @var string
	*/
	protected $primaryKey = 'productId';
	
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'productId',
		'product_sku',
		'product_title',
		'product_price',
		'product_variant',
		'product_desc',
		'product_slug',
		'category_ID',
		'product_img',
		'product_weight',
		'product_stok',
	];
	
	protected $casts = [
		'product_img' => 'array',
		'product_variant' => 'array',
	];
	
	
	/**
	* Relation Data One To Many.
	*
	* @var array
	*/
	public function category()
	{
		return $this->belongsTo('App\Category', 'category_ID');
	}

	/**
	* Column add slug url Castings.
	*
	* @var array
	*/
	public function sluggable()
	{
		return [
			'product_slug' => [
				'source' => 'product_title'
			]
		];
	}
}

