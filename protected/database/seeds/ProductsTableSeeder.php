<?php
use Illuminate\Database\Seeder;
use App\Product;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$products = array(
			array('productId' => '1','category_ID' => '5','product_sku' => 'DN001','product_title' => 'Cocobewey Crochet Donut Cushion','product_price' => '95.24','product_variant' => '','product_desc' => '<p>This adorable donut cushion is perfect gift for baby shower &amp; adding adorable item to the nursery or simply adding cuteness to the living room. No glue or sharp items are used in this item.&nbsp; Made from 100% cotton yarn.&nbsp; Finishing size of the cushion is 44cm x 44cm x 11cm (17.5&quot; x 17.5&quot; x 4.5&quot;)&nbsp; This is made to order cushion, if you require different color, please send me a convo, I&#39;m sure I can find the color you are looking for. (For custom order, please allow 1-2 week around time for dispatched. All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)&nbsp; Washing instruction: If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer! If you have any other questions, please do not hesitate to contact me</p>
			
			<p>Original Pattern by Flaming Pot</p>
			','product_slug' => 'cocobewey-crochet-donut-cushion','product_img' => '{"1475206918472":"4"}','product_weight' => '1','product_stok' => '','created_at' => '2016-09-30 03:21:44','updated_at' => '2016-09-30 03:46:03'),
			array('productId' => '2','category_ID' => '5','product_sku' => 'DN002','product_title' => 'O-Nut Crochet Donut Cushion','product_price' => '95.00','product_variant' => '','product_desc' => '<p>This adorable donut cushion is perfect gift for baby shower &amp; adding adorable item to the nursery or simply adding cuteness to the living room. No glue or sharp items are used in this item.&nbsp;<br />
			<br />
			Made from 100% cotton yarn.&nbsp;<br />
			<br />
			Finishing size of the cushion is 44cm x 44cm x 11cm (17.5&quot; x 17.5&quot; x 4.5&quot;)&nbsp;<br />
			<br />
			This is made to order cushion, if you require different color, please send me a convo, I&#39;m sure I can find the color you are looking for. (For custom order, please allow 1-2 week around time for dispatched. All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)&nbsp;<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me</p>
			','product_slug' => 'o-nut-crochet-donut-cushion','product_img' => '{"1475207255849":"5"}','product_weight' => '1','product_stok' => '1','created_at' => '2016-09-30 03:47:51','updated_at' => '2016-09-30 03:47:51'),
			array('productId' => '3','category_ID' => '5','product_sku' => 'DN003','product_title' => 'Pink Ponk Crochet Donut Cushion','product_price' => '85.00','product_variant' => '','product_desc' => '<p>This adorable donut cushion is perfect gift for baby shower &amp; adding adorable item to the nursery or simply adding cuteness to the living room. No glue or sharp items are used in this item.&nbsp;<br />
			<br />
			Made from 100% cotton yarn.&nbsp;<br />
			<br />
			Finishing size of the cushion is 34cm x 34cm x 9cm (13.5&quot; x 13.5&quot; x 3.5&quot;)&nbsp;<br />
			<br />
			This is made to order cushion, if you require different color, please send me a convo, I&#39;m sure I can find the color you are looking for. (For custom order, please allow 1-2 week around time for dispatched. All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)&nbsp;<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me</p>
			','product_slug' => 'pink-ponk-crochet-donut-cushion','product_img' => '{"1475207339127":"6"}','product_weight' => '1','product_stok' => '1','created_at' => '2016-09-30 03:49:11','updated_at' => '2016-09-30 03:49:11'),
			array('productId' => '4','category_ID' => '5','product_sku' => 'DN004','product_title' => 'Princess Bubblegum Crochet Donut Cushion','product_price' => '85.00','product_variant' => '','product_desc' => '<p>This adorable donut cushion is perfect gift for baby shower &amp; adding adorable item to the nursery or simply adding cuteness to the living room. No glue or sharp items are used in this item.&nbsp;<br />
			<br />
			Made from 100% cotton yarn.&nbsp;<br />
			<br />
			Finishing size of the cushion is 44cm x 44cm x 11cm (17.5&quot; x 17.5&quot; x 4.5&quot;)&nbsp;<br />
			<br />
			This is made to order cushion, if you require different color, please send me a convo, I&#39;m sure I can find the color you are looking for. (For custom order, please allow 1-2 week around time for dispatched. All shipping will come with tracking no. For international order, if you would like a a cheaper shipping option without a tracking no,&nbsp;please send me a convo and I will figure out the cheaper option for you<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me</p>
			
			<p>Original Pattern by Flaming Pot</p>
			','product_slug' => 'princess-bubblegum-crochet-donut-cushion','product_img' => '{"1475207408649":"7"}','product_weight' => '1','product_stok' => '1','created_at' => '2016-09-30 03:50:36','updated_at' => '2016-09-30 03:50:36'),
			array('productId' => '5','category_ID' => '2','product_sku' => 'CC001','product_title' => 'Individual Crochet Cupcake','product_price' => '20.00','product_variant' => '','product_desc' => '<p>Do you have sweet tooth? Grab this yummy cupcakes with sprinkle or without sprinkle with cherry or strawberry on top without gaining any calories! How amazing is that?<br />
			<br />
			The cupcakes are hand crocheted filled with polyster stuffing and made with 100% soft cotton yarn. Perfect for playtime or fun tea parties or even for pincushion.&nbsp;<br />
			They come with 4 different frosting, strawberry, banana, orange &amp; lavender. Please leave me a note at the checkout the topping you&#39;d like, they come in strawberry or cherry on top. If you don&#39;t choose the topping, I&#39;ll select for you randomly.<br />
			<br />
			This is made to order, if you require different flavour colour of icing, please send me a convo, I&#39;m sure I can find the color you are looking for. Please allow 1-2 week around time for dispatched. All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me<br />
			<br />
			&quot;This doll is handmade by Flamingpot (me) from a design and pattern by lalylala handmade. Lydia Tresselt / www.lalylala.com&ldquo;</p>
			','product_slug' => 'individual-crochet-cupcake','product_img' => '{"1475207608778":"8","1475207636574":"9","1475207650767":"10"}','product_weight' => '1','product_stok' => '1','created_at' => '2016-09-30 03:54:21','updated_at' => '2016-09-30 03:54:21'),
			array('productId' => '6','category_ID' => '2','product_sku' => 'CC002','product_title' => 'Set of 4 Crochet Cupcakes','product_price' => '75.00','product_variant' => '','product_desc' => '<p>Do you have sweet tooth? Grab this yummy cupcakes with sprinkle or without sprinkle with cherry or strawberry on top without gaining any calories! How amazing is that?<br />
			<br />
			The cupcakes are hand crocheted filled with polyster stuffing and made with 100% soft cotton yarn. They come with 4 different frosting, strawberry, banana, orange &amp; lavender. Perfect for playtime or fun tea parties or even for pincushion.&nbsp;<br />
			<br />
			This is made to order, if you require different flavour colour of icing, please send me a convo, I&#39;m sure I can find the color you are looking for. Please allow 1-2 week around time for dispatched. All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me<br />
			<br />
			&quot;This doll is handmade by Flamingpot (me) from a design and pattern by lalylala handmade. Lydia Tresselt / www.lalylala.com&ldquo;</p>
			','product_slug' => 'set-of-4-crochet-cupcakes','product_img' => '{"1475208131605":"11","1475208145651":"12","1475208149271":"14","1475208150773":"15"}','product_weight' => '1','product_stok' => '1','created_at' => '2016-09-30 04:03:35','updated_at' => '2016-09-30 04:09:08'),
			array('productId' => '7','category_ID' => '1','product_sku' => 'AP001','product_title' => 'Toddler\'s cowl','product_price' => '43.00','product_variant' => '','product_desc' => '<p>This soft and warm toddlers cowl will definitely keep your little one warm this winter. This will suit 2-4 years old. There are few colours to choose from:&nbsp;<br />
			1. Pink &amp; brown<br />
			2. Pink &amp; white<br />
			3. Brown<br />
			4. White &amp; brown<br />
			&nbsp;<br />
			They are made with 100% polyester, they are very very soft and machine washable (must be placed in a laundry/lingerie bag with gentle cycle)</p>
			','product_slug' => 'toddler-s-cowl','product_img' => '{"1475208678682":"16","1475208680531":"17"}','product_weight' => '1','product_stok' => '2','created_at' => '2016-09-30 04:11:47','updated_at' => '2016-09-30 04:11:47'),
			array('productId' => '8','category_ID' => '3','product_sku' => 'CD002','product_title' => 'Hedgehog Crochet Cushion','product_price' => '85.00','product_variant' => '','product_desc' => '<p>This adorable crochet cushion is perfect gift for baby shower or ading adorable &amp; cute item to the nursery. They are suitable for baby as I embroidered the eyes instead of using amigurumi eyes.&nbsp;<br />
			<br />
			Made from 100% cotton yarn.&nbsp;<br />
			<br />
			Finishing size of the doll 29cm x23cm (11.5&quot; x 9&quot;)<br />
			<br />
			This particular color on photo display is ready to dispatched but If there&#39;s any other particular color you&#39;d like, please send me a convo, I&#39;m sure I can find the color you are looking for. (For custom order, please allow 1-2 week around time for dispatched). All shipping will come with tracking no. If you would like a cheaper option (without tracking for international order, please send me a convo and I will figure out the cheaper option for you)<br />
			<br />
			Washing instruction:<br />
			If spotcleaning doesn&rsquo;t do the trick you can carefully wash in the sink, using cool water and a mild soap. Reshape and air dry. Do not use a dryer!<br />
			<br />
			If you have any other questions, please do not hesitate to contact me</p>
			','product_slug' => 'hedgehog-crochet-cushion','product_img' => '["18"]','product_weight' => '1','product_stok' => '2','created_at' => '2016-09-30 04:13:39','updated_at' => '2016-10-07 03:50:15')
		);
		
		Product::truncate();
		foreach( $products as $product )
		{
			Product::create($product);
		}
    }
}
