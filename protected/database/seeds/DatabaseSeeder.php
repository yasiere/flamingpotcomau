<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class DatabaseSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		Model::unguard();
		$this->call(UsersTableSeeder::class);
		//$this->call(MediaTableSeeder::class);
		$this->call(PagesTableSeeder::class);
		$this->call(ShippingsTableSeeder::class);
		$this->call(CategoriesTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
	}
}