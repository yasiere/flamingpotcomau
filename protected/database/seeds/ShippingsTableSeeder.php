<?php
use Illuminate\Database\Seeder;
use App\Shipping;


class ShippingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		Shipping::create([
			'shipping_title' => 'Rest of the World'
		]);
    }
}
