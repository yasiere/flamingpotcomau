<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		\App\User::create([
			'user_login' => 'info@flamingpot.com.au',
			'user_role' => 'admin',
			'user_avatar' => 'avatars.png',
			'display_name' => 'Administrator',
			'user_nicename' => 'Admin',
			'user_email' => 'info@flamingpot.com.au',
			'password' => bcrypt('lopassword'),
		]);
    }
}
