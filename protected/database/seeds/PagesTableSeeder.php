<?php
use Illuminate\Database\Seeder;
use App\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$pages = [
			[
				'page_title' => 'About',
				'page_content' => '',
			],
			[
				'page_title' => 'Contact',
				'page_content' => '',
			],
			[
				'page_title' => 'FAQ',
				'page_content' => '',
			],
		];
		
		Page::truncate();
		foreach( $pages as $page )
		{
			Page::create($page);
		}
    }
}