<?php
use Illuminate\Database\Seeder;
use App\Media;


class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$media = array(
			array('mediaId' => '1','media_name' => 'banner-everything.jpg','media_type' => '','media_meta' => '{"title":"banner-everything.jpg","caption":"","alt_text":"","description":"","size":{"largeimage":["\\/largeimage\\/banner-everything.jpg",800,800],"smallimage":["\\/smallimage\\/banner-everything.jpg",458,458],"thumbnail":["\\/thumbnail\\/banner-everything.jpg",150,150],"tumbimage":["\\/tumbimage\\/banner-everything.jpg",80,80]}}','created_at' => '2016-09-30 01:37:00','updated_at' => '2016-09-30 01:37:00'),
			array('mediaId' => '2','media_name' => 'banner-donut.jpg','media_type' => '','media_meta' => '{"title":"banner-donut.jpg","caption":"","alt_text":"","description":"","size":{"largeimage":["\\/largeimage\\/banner-donut.jpg",800,800],"smallimage":["\\/smallimage\\/banner-donut.jpg",458,458],"thumbnail":["\\/thumbnail\\/banner-donut.jpg",150,150],"tumbimage":["\\/tumbimage\\/banner-donut.jpg",80,80]}}','created_at' => '2016-09-30 01:37:04','updated_at' => '2016-09-30 01:37:04'),
			array('mediaId' => '3','media_name' => 'banner-apparel.jpg','media_type' => '','media_meta' => '{"title":"banner-apparel.jpg","caption":"","alt_text":"","description":"","size":{"largeimage":["\\/largeimage\\/banner-apparel.jpg",800,800],"smallimage":["\\/smallimage\\/banner-apparel.jpg",458,458],"thumbnail":["\\/thumbnail\\/banner-apparel.jpg",150,150],"tumbimage":["\\/tumbimage\\/banner-apparel.jpg",80,80]}}','created_at' => '2016-09-30 01:37:07','updated_at' => '2016-09-30 01:37:07'),
			array('mediaId' => '4','media_name' => 'cocobewey-crochet-donut-cushion.jpg','media_type' => '','media_meta' => '{"title":"cocobewey-crochet-donut-cushion.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/cocobewey-crochet-donut-cushion.jpg",640,640],"thumbnail":["\\/thumbnail\\/cocobewey-crochet-donut-cushion.jpg",150,150]}}','created_at' => '2016-09-30 03:43:26','updated_at' => '2016-09-30 03:43:26'),
			array('mediaId' => '5','media_name' => 'o-nut-crochet-donut-cushion.jpg','media_type' => '','media_meta' => '{"title":"o-nut-crochet-donut-cushion.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/o-nut-crochet-donut-cushion.jpg",640,640],"thumbnail":["\\/thumbnail\\/o-nut-crochet-donut-cushion.jpg",150,150]}}','created_at' => '2016-09-30 03:47:42','updated_at' => '2016-09-30 03:47:42'),
			array('mediaId' => '6','media_name' => 'pink-ponk-crochet-donut-cushion.jpg','media_type' => '','media_meta' => '{"title":"pink-ponk-crochet-donut-cushion.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/pink-ponk-crochet-donut-cushion.jpg",640,640],"thumbnail":["\\/thumbnail\\/pink-ponk-crochet-donut-cushion.jpg",150,150]}}','created_at' => '2016-09-30 03:49:06','updated_at' => '2016-09-30 03:49:06'),
			array('mediaId' => '7','media_name' => 'princess-bubblegum-crochet-donut-cushion.jpg','media_type' => '','media_meta' => '{"title":"princess-bubblegum-crochet-donut-cushion.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/princess-bubblegum-crochet-donut-cushion.jpg",640,640],"thumbnail":["\\/thumbnail\\/princess-bubblegum-crochet-donut-cushion.jpg",150,150]}}','created_at' => '2016-09-30 03:50:14','updated_at' => '2016-09-30 03:50:14'),
			array('mediaId' => '8','media_name' => 'individual-crochet-cupcake.jpg','media_type' => '','media_meta' => '{"title":"individual-crochet-cupcake.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/individual-crochet-cupcake.jpg",640,640],"thumbnail":["\\/thumbnail\\/individual-crochet-cupcake.jpg",150,150]}}','created_at' => '2016-09-30 03:53:36','updated_at' => '2016-09-30 03:53:36'),
			array('mediaId' => '9','media_name' => 'individual-crochet-cupcake-2.jpg','media_type' => '','media_meta' => '{"title":"individual-crochet-cupcake-2.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/individual-crochet-cupcake-2.jpg",640,640],"thumbnail":["\\/thumbnail\\/individual-crochet-cupcake-2.jpg",150,150]}}','created_at' => '2016-09-30 03:53:41','updated_at' => '2016-09-30 03:53:41'),
			array('mediaId' => '10','media_name' => 'individual-crochet-cupcake3.jpg','media_type' => '','media_meta' => '{"title":"individual-crochet-cupcake3.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/individual-crochet-cupcake3.jpg",640,640],"thumbnail":["\\/thumbnail\\/individual-crochet-cupcake3.jpg",150,150]}}','created_at' => '2016-09-30 03:53:44','updated_at' => '2016-09-30 03:53:44'),
			array('mediaId' => '11','media_name' => 'set-of-4-crochet-cupcakes.jpg','media_type' => '','media_meta' => '{"title":"set-of-4-crochet-cupcakes.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/set-of-4-crochet-cupcakes.jpg",640,640],"thumbnail":["\\/thumbnail\\/set-of-4-crochet-cupcakes.jpg",150,150]}}','created_at' => '2016-09-30 04:02:22','updated_at' => '2016-09-30 04:02:22'),
			array('mediaId' => '12','media_name' => 'set-of-4-crochet-cupcakes2.jpg','media_type' => '','media_meta' => '{"title":"set-of-4-crochet-cupcakes2.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/set-of-4-crochet-cupcakes2.jpg",640,640],"thumbnail":["\\/thumbnail\\/set-of-4-crochet-cupcakes2.jpg",150,150]}}','created_at' => '2016-09-30 04:02:36','updated_at' => '2016-09-30 04:02:36'),
			array('mediaId' => '14','media_name' => 'set-of-4-crochet-cupcakes5.jpg','media_type' => '','media_meta' => '{"title":"set-of-4-crochet-cupcakes5.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/set-of-4-crochet-cupcakes5.jpg",640,640],"thumbnail":["\\/thumbnail\\/set-of-4-crochet-cupcakes5.jpg",150,150]}}','created_at' => '2016-09-30 04:02:53','updated_at' => '2016-09-30 04:02:53'),
			array('mediaId' => '15','media_name' => 'set-of-4-crochet-cupcakes3.jpg','media_type' => '','media_meta' => '{"title":"set-of-4-crochet-cupcakes3.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/set-of-4-crochet-cupcakes3.jpg",640,640],"thumbnail":["\\/thumbnail\\/set-of-4-crochet-cupcakes3.jpg",150,150]}}','created_at' => '2016-09-30 04:03:27','updated_at' => '2016-09-30 04:03:27'),
			array('mediaId' => '16','media_name' => 'toddler-s-cowl.jpeg','media_type' => '','media_meta' => '{"title":"toddler-s-cowl.jpeg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/toddler-s-cowl.jpeg",640,640],"thumbnail":["\\/thumbnail\\/toddler-s-cowl.jpeg",150,150]}}','created_at' => '2016-09-30 04:11:28','updated_at' => '2016-09-30 04:11:28'),
			array('mediaId' => '17','media_name' => 'toddler-s-cowl-2.jpeg','media_type' => '','media_meta' => '{"title":"toddler-s-cowl-2.jpeg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/toddler-s-cowl-2.jpeg",640,640],"thumbnail":["\\/thumbnail\\/toddler-s-cowl-2.jpeg",150,150]}}','created_at' => '2016-09-30 04:11:42','updated_at' => '2016-09-30 04:11:42'),
			array('mediaId' => '18','media_name' => 'hedgehog-crochet-cushion.jpg','media_type' => '','media_meta' => '{"title":"hedgehog-crochet-cushion.jpg","caption":"","alt_text":"","description":"","size":{"smallimage":["\\/smallimage\\/hedgehog-crochet-cushion.jpg",640,640],"thumbnail":["\\/thumbnail\\/hedgehog-crochet-cushion.jpg",150,150]}}','created_at' => '2016-09-30 04:13:34','updated_at' => '2016-09-30 04:13:34'),
			array('mediaId' => '19','media_name' => 'static1-squarespace-com.jpg','media_type' => '','media_meta' => '{"title":"static1-squarespace-com.jpg","caption":"","alt_text":"","description":"","size":{"mediumimage":["\\/mediumimage\\/static1-squarespace-com.jpg",800,800],"smallimage":["\\/smallimage\\/static1-squarespace-com.jpg",600,600],"thumbnail":["\\/thumbnail\\/static1-squarespace-com.jpg",150,150]}}','created_at' => '2016-09-30 07:42:18','updated_at' => '2016-09-30 07:42:18')
		);
		
		Media::truncate(); 
		foreach( $media as $img )
		{
			Media::create($img);
		}
    }
}
