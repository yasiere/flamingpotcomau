<?php
use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$categories = [
			[
				'category_title' => 'Apparel',
			],
			[
				'category_title' => 'Cupcake',
			],
			[
				'category_title' => 'Cushion ',
			],
			[
				'category_title' => 'Doll',
			],
			[
				'category_title' => 'Donut',
			],
			[
				'category_title' => 'Others',
			],
		];
		
		Category::truncate();
		foreach( $categories as $category )
		{
			Category::create($category);
		}
    }
}
