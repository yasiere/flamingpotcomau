<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('pages', function (Blueprint $table) {
			$table->bigIncrements('pageId');
			$table->text('page_title');
			$table->text('page_content');
			$table->string('page_img', 5);
			$table->text('page_slug');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('pages');
    }
}
