<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('order_details', function (Blueprint $table) {
			$table->bigIncrements('orderDetailId');
			$table->longText('order_ID');
			$table->longText('product_ID');
			$table->longText('detail_meta');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('order_details');
    }
}
