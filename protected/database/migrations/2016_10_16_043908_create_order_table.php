<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
	 *
	 * meta: shipping addres, nama, alamant dll
	 * price: shipping price, total price, discount price, tax
     */
    public function up()
    {
		Schema::create('orders', function (Blueprint $table) {
			$table->bigIncrements('orderId');
			$table->text('payment');
			$table->string('invoice_code');
			$table->longText('order_meta');
			$table->string('order_status', 45);
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('orders');
    }
}
