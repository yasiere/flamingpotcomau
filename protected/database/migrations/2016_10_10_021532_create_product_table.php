<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('products', function (Blueprint $table) {
			$table->bigIncrements('productId');
			$table->integer('category_ID');
			$table->string('product_sku', 50)->unique();
			$table->string('product_title');
			$table->float('product_price');
			$table->text('product_variant');
			$table->text('product_desc');
			$table->string('product_slug');
			$table->string('product_img');
			$table->integer('product_weight');
			$table->integer('product_stok');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('products');
    }
}
