<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('homes', function (Blueprint $table) {
			$table->bigIncrements('homeId');
			$table->string('banner_title');
			$table->integer('banner_img');
			$table->string('banner_link');
			$table->integer('banner_category');
			$table->text('banner_text');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('homes');
    }
}
