<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('shipping_locations', function (Blueprint $table) {
			$table->increments('locationId');
			$table->integer('shipping_ID');
			$table->string('location_code'); 
			$table->string('location_type', 40); 
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shipping_locations');
    }
}
