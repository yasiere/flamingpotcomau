<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('news', function (Blueprint $table) {
			$table->bigIncrements('newsId');
			$table->string('news_title');
			$table->text('news_content');
			$table->text('news_excerpt');
			$table->integer('news_img');
			$table->string('news_slug');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('news');
    }
}
