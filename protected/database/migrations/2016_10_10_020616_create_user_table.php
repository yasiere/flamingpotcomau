<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('ID');
			$table->string('user_login', 60);
			$table->string('password', 64);
			$table->string('user_nicename', 50);
			$table->string('user_email', 100);
			$table->string('user_role', 100);
			$table->string('display_name');
			$table->string('user_avatar');
			$table->rememberToken();
			$table->dateTime('last_login');
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('users');
    }
}
