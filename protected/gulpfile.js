var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 
elixir(function(mix) {
    mix.sass(['site.scss'], '../assets/css/style.css');
}); 
/* 
elixir(function(mix) {
    mix.sass(['admin.scss'], '../assets/css/admin.css');
}); */