<?php
/*
 |--------------------------------------------------------------------------
 | This file will use to set default configuration for all(frontend or backend)
 | for cms based package.
 |
 | @date: 19/10/16
 | @since: 1.0.3
 | @author: Wimbo denmazwimbo@yahoo.com
 |--------------------------------------------------------------------------
 */
return [
	
    /*
    |--------------------------------------------------------------------------
    | Site Config
    |--------------------------------------------------------------------------
    |
    | This value determines the setting of site like Email, name, etc.
    |
    */
	'site' =>[
		'title' => 'Flamingpot',
		'contact_mail' => env('SHOP_MAIL'),
		'shop_mail' => env('CONTACT_MAIL'),
		'shop_title' => 'Flamingpot',
		'street' => 'Mariland st.',
		'city' => 'Auckland',
		'state' => 'Mexico City',
		'zip_code' => '038749',
		'country' => 'Indonesia',
		'phone' => '0987455-9875',
	],
	
	
    /*
    |--------------------------------------------------------------------------
    | Helper configuration
    |--------------------------------------------------------------------------
    |
    | This value determines the "developer helper" your application is currently
    | running in. This may determine how you prefer to configure various
    | helper package application utilizes.
    |
    */
	'helper' => [],
	
    /*
    |--------------------------------------------------------------------------
    | Local Info
    |--------------------------------------------------------------------------
    |
    | This value determines local info like currency etc.
    |
    */
	'currency' => [
		'code' => 'AUD',
		'position' => 'right',
		'decimal_sep' => '.',
		'thousand_sep' => ',',
		'num_decimals' => 2,
	],
	'weight_unit'    => 'Kg',
	'dimension_unit' => 'cm',
	
	/*
    |--------------------------------------------------------------------------
    | Rezise image
    |--------------------------------------------------------------------------
    |
    | This value determines all image size needed when upload on library.
	| Thumbnail size is set by default, see on App\Cms\UploadFileControllerTrait option->thumbnail
    |
    */
	'image_size' => [
		'largeimage' => [
			'max_width' => 800,
			'max_height' => 800,
		],
		'smallimage' => [
			'max_width' => 458,
			'max_height' => 458,
		],
		'tumbimage' => [
			'max_width' => 80,
			'max_height' => 80,
		],
	],
	
	/*
    |--------------------------------------------------------------------------
    | Invoice code
    |--------------------------------------------------------------------------
    |
    | This value determines prefix of invoice code. 
    |
    */
	'invoice_code' => 'FLP',

	/*
	|--------------------------------------------------------------------------
	| Admin prefix 
	|--------------------------------------------------------------------------
	| 
	| This value determines prefix to acces  admin/cms/backend page. 
	| like http:\\base.com\cms_url -> will redirect to admin/backend page. 
	|
	*/
	'cms_uri' => 'adminftss',
	
	/*
	|--------------------------------------------------------------------------
	| Admin User Role
	|--------------------------------------------------------------------------
	| 
	| Register modul access role here:
	| admin is auto set to can access all. if some modul has spesific acces just set in here whith named 'admin'.
	| 
	| Role structure :
	| name => [
	|		title => 'Role title',
	|		action => [
	|			action-name =>[
	|				modul-class-name,
	|				modul-class-name,	
	|			],
	|			action-name =>[
	|				modul-class-name,
	|				modul-class-name,	
	|			],
	|		]
	| ]
	|
	| Role structure description :
	|	- name => name of role saved on database and assign on register user
	|	- title => title of role, display on user profile and select option when registered user
	|	- action => role action :
	|	- - action-name can be filled with array('add', 'edit', 'delete') or * (* can add, edit or delete all available modul)
	| - modul-class-name => name of modul :
	| - - modul-class-name can be filled with array('modul-class-name') or * (* all available modul)
	---------------------------------*/
	'cms_roles' => [ 
		'admin' => [
			'title' => 'Administrator',
			'action' => '*',
		]
	],
];
