@extends('admin.layout.dashboard')
 
@push('style')
	<link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.css') }}" type="text/css" media="all"/>
	@include('developer.media-head')
	<style>
		.fixed-layout-width{
			max-width: 900px !important;
		}
	</style>
@endpush

@section('header-type', 'content-layout-form fixed-layout-width')

@section('conten')
<section class="content fixed-layout-width">
	<div class="row">
		<form id="master-form" action="{{ route('admin::store', ['modul'=> $dashboard['modul_url'], 'post_id' => $dashboard['post_id'] ]) }}" method="POST">
			{!! csrf_field() !!}
			<?php $form_builder = $forms['form_builder']; ?>
			<section class="col-sm-12">
				<div class="row">
					<div class="col-xs-12">
						@foreach( $forms['primary'] as $key => $form )
						<div class="box box-widget">
							@if( $key > 0)
							<div class="box-header"> 
								<h3 class="box-title">{{ $form['title'] }}</h3>
							</div>
							@endif
							<div class="box-body">
							@foreach( $form['fields'] as $field )
								{!! form_row($form_builder->$field['name']) !!}
							@endforeach
							</div>
						</div> 
						@endforeach
					</div>
					<div class="col-xs-12">
						@foreach( $forms['default'] as $form )
							@include('admin.part.form-box', ['form_builder' => $form_builder, 'form' => $form])
						@endforeach
					</div>
				</div>
			</section>
			<section class="col-sm-12">
				@foreach( $forms['side'] as $form )
					@include('admin.part.form-box', ['form_builder' => $form_builder, 'form' => $form])
				@endforeach
			</section>
			<section class="col-sm-12">
				<div class="content-layout-form content-footer">
				@include('admin.part.delete-button')
				@include('admin.part.action-button')
				</div>
			</section>
		</form>
	</div>
</section>
@stop

@push('footer')
	@include('admin.part.script-form')
	@include('developer.media-footer')
@endpush