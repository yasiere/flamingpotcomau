<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	 <meta content="Denmaz" name="author">
	<link href="{{ asset('assets/img/favicon.ico') }}" rel="shortcut icon">
    @yield('head')
    <title> FlamingpotCMS</title>
    @show
	@stack('style')
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.css') }}" type="text/css" media="all" />
	 <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}" type="text/css" media="all" />
    
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.ui.1.11.4.min.js') }}"></script>
    @stack('script')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-black sidebar-mini slimScrollDiv">
    <div class="wrapper">
        @include('admin.part.header')
        @include('admin.part.sidebar')
        <div class="content-wrapper">
            <section class="@yield('header-type') content-header">
                @if(Session::has('alert'))
				<?php 
				switch(Session::get('alert.class'))
				{
					case 'danger':
						$alert = '<i class="icon fa fa-ban"></i> Error!';
						break;
					case 'success':
						$alert = '<i class="icon fa fa-check"></i> Success!';
						break;
					case 'warning':
						$alert = '<i class="icon fa fa-warning"></i> Warning!';
						break;
					default:
						$alert = '<i class="icon fa fa-info"></i> Info!';
				}
				?>
				<div class="alert alert-{{ Session::get('alert.class') }} alert-dismissible text-left">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
					<h4>{!! $alert !!}</h4>
					{{ trans(Session::get('alert.localization')) }}
				</div>
                @endif
				@if(Breadcrumbs::exists())
					@include('admin.part.breadcrumbs', ['breadcrumbs'=> Breadcrumbs::generate()])
					@include('admin.part.action-button')
				@endif 
            </section>
				<div class="scroll-dummy-section"></div>
            @yield('conten')
        </div>
        <footer class="main-footer">
            <b>Version</b> 1.0.3
        </footer>
    </div>
    <!-- ./wrapper -->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script type="text/javascript">
        $.widget.bridge('uibutton', $.ui.button);
		var asset_vendor_url = '{{ asset("assets/vendor") }}';  
    </script>
    <script type="text/javascript" src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/admin.min.js') }}"></script>
    @stack('footer')
</body>
</html>