@extends('admin.layout.dashboard')

@push('style')
	<link rel="stylesheet" href="{{ asset('assets/vendor/datatables/dataTables.bootstrap.css') }}" type="text/css" media="all"/>
	<style>
		.box-body table > thead > tr > th:last-child{
			width: 30px;
		}
	</style>
@endpush

@section('conten')
<section class="content">
<div class="row">
	<section class="col-xs-12">
		<div class="box box-widget">
			<div class="box-body">
			<form id="post_records" action="{{ route('admin::deletes', ['modul'=>$dashboard['modul_url'] ]) }}" method="POST"> 
			@if( $dashboard['button.delete'] )
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@endif
			
			<table class="table table-hover dataTable text-left">
				<thead>
				<tr> 
					<th class="bulk-column"><input type="checkbox" style="opacity: 0;"></th> 
					@foreach( $tables['column'] as $column )
						<th>{{ $column['title'] }}</th>
					@endforeach
				</tr>
				</thead>
				<tbody>
				<?php
				$edit_url = route('admin::form', ['modul'=>$dashboard['modul_url']]);
				?>
				@foreach( $tables['records'] as $record )
					<tr>
						<?php
							$postId = $record->$tables['post_id'];
							$first = true;
						?>
						<td>
							@if( $dashboard['button.delete'] )
							<input class="bulkTableRow" name="checkbox[]" type="checkbox" value="{{ $postId }}">
							@endif
						</td>
						@foreach( $tables['column'] as $column )
							<td>
								<?php $edit_tag = '';?>
								@if( $dashboard['button.edit'] && $first )
									<a href="{{ $edit_url }}/{{ $postId }}" class="hover-underline">
									<?php
										$edit_tag = '</a>';
										$first = false;
									?>
								@endif
								
								@if( is_callable($column['column']) )
									<?php echo $column['column']($record); ?>
								@else
									<?php echo $record->$column['column']; ?>
								@endif
								
								{!! $edit_tag !!}
							</td>
						@endforeach
					</tr>
				@endforeach
				</tbody>
			</table>
			</form>
			</div>
		</div>
	</section>
</div>
</section>
@stop

@push('footer')
	@include('admin.part.script-table')
@endpush
