<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <!--<section class="sidebar" style="height: auto;">-->
    <section class="sidebar" style="height: auto;">
      <!-- Sidebar user panel -->
      <div class="user-panel">
		<img class="img-responsive" src="{{ asset('assets/img/admin.logo.png') }}" alt="" style="width: 100%;">
	  <!--
        <div class="pull-left image">
          <img alt="User Image" class="img-circle" src="{{-- echo admin_url('images/avatar/') . $_SESSION['user_data']['user_avatar']; --}}">
          
        </div>
        <div class="pull-left info">
			<p>{{-- echo $_SESSION['user_data']['name']; --}}</p>
			<div class="text-sm">
				<span class="pull-left icon"><i class="fa fa-circle text-success-light fa-blink fa-lg"></i></span>
				<div class="text-overflow"><span> Login Terakir :</span>  {{-- echo $_SESSION['user_data']['last_login']; --}}</div>
           <a class="hidden" href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
		</div>-->
      </div>
		<ul class="sidebar-menu">
			<li class="header"><strong>MAIN NAVIGATION</strong></li> 
			@include('admin.part.menuItems', ['items' => $adminMenu->roots()])
		</ul>
      <!--  menu admin -->
    </section>
    <!-- /.sidebar -->
  </aside>
