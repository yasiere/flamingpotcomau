@if ($breadcrumbs)
	<ol class="breadcrumb text-left">
		<i class="fa fa-{{ $dashboard['icon'] }}"></i>
		@foreach ($breadcrumbs as $breadcrumb)
            @if( $breadcrumb->last)
					<li class="active">{{ $breadcrumb->title }}</li>
            @else 
					<li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @endif
        @endforeach
	</ol> 
@endif