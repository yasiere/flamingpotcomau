<script type="text/javascript">
	var FLTableLayout = {
		'i18n': {
			'bulk_button': "Delete selected",
			'bulk_selected': "{{ str_plural($dashboard['title']) }} selected",
			'bulk_message': "Deleted {{ str_plural($dashboard['title']) }} cannot be recovered.\n Do you still want to continue?"
		}
	};
</script>
<script type="text/javascript" src="{{ asset('assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/admin.table.layout.js') }}"></script>

