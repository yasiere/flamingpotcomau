<div class="action-button">
	@if( ! $dashboard['post_id'] )
	<div class="btn-group">
		<a href="{{ route('admin::show', $dashboard['modul_url']) }}" class="btn btn-default">
			<span class="text-primary">Cancel</span>
		</a>
	</div>
	@endif
	@if( $dashboard['post_id'] && $dashboard['button.add'] )
	<div class="btn-group">
		<a class="btn btn-default" href="{{ route('admin::form', ['modul'=>$dashboard['modul_url']]) }}">
			<span class="text-primary">Add New {{ str_singular($dashboard['title']) }}</span>
		</a>
	</div>
	@endif
	<div class="btn-group">
		<button class="btn btn-primary" type="submit">Save @if( ! $dashboard['post_id'] ){{ str_singular($dashboard['title']) }} @endif</button>
	</div> 
	@if( $dashboard['button.add'] )
		<a class="btn btn-primary" href="{{ route('admin::form', ['modul'=>$dashboard['modul_url']]) }}">
			Add {{ str_singular($dashboard['title']) }}
		</a>
	@endif
</div>