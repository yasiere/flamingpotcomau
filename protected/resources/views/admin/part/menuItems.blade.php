@foreach($items as $item)
<li@lm-attrs($item) @if($item->hasChildren()) class="treeview" @endif @lm-endattrs>
<a href="{!! $item->url() !!}">
	<i class="fa fa-{{ $item->data['icon'] }} "></i>
	<span>{!! $item->title !!}</span>
	@if($item->hasChildren())
	<span class="pull-right-container">
		<i class="fa fa-angle-left pull-right"></i>
	</span>
	@endif
</a>
@if($item->hasChildren())
<ul class="treeview-menu">
@include('admin.part.menuItems', array('items' => $item->children()))
</ul>
@endif
</li>
@endforeach