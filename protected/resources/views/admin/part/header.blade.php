<?php $active_user = Auth::user();?>
<header class="main-header">
    <a class="logo" href="{{ route('admin::dashboard') }}">
      <span class="logo-mini"><b>F</b>MP</span>
      <span class="logo-lg"><b>Flamingpot</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a role="button" data-toggle="offcanvas" class="sidebar-toggle" href="#">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav"> 
          <li class="dropdown user user-menu">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="true">
              <img src="{{ asset('assets/img/avatar/'. $active_user->user_avatar) }}" alt="picture" class="user-image">
              <span class="hidden-xs">{{ $active_user->user_nicename }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img alt="picture" class="img-circle" src="{{ asset('assets/img/avatar/' . $active_user->user_avatar) }}">
                <p>
					{{ $active_user->display_name }}<br>
					<strong>{{ array_get( config('cms.cms_roles'), $active_user->user_role . '.title', 'default') }}</strong>
				</p>
              </li>
				  <li class="user-body">
                 <p class="text-center">
                  <!--<small>Terdaftar sejak </small>-->
                </p>
                <!-- /.row -->
              </li>
              <li class="user-footer">
               <div class="pull-left">
                  <a class="btn btn-default" href="{{ route('admin::userProfile') }}">Profile</a>
                </div>
                <div class="pull-right">
                  <a class="btn btn-default" href="{{ url('logout') }}">Log Out</a>
                </div>
              </li>
            </ul>
          </li>
		  <li class="dropdown">
			<a href="{{ url('/') }}" class="show-on-hover">
				<i class="fa fa-globe fa-lg"></i> <span class="hover-show">Visit Site<span>
			</a>
			</li>
        </ul>
      </div>
    </nav>
  </header>
