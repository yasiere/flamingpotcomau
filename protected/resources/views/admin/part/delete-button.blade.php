@if( $dashboard['post_id'] && $dashboard['button.delete'] )
	<div class="btn-group delete-button pull-left">
		<a class="btn btn-default" href="{{ route('admin::delete', ['modul'=>$dashboard['modul_url'], 'post_id'=>$dashboard['post_id']]) }}">
			<span class="text-danger">Delete</span>
		</a>
	</div>
@endif