<div class="box box-solid" id="{{ $form['id'] }}">
	<div class="box-header with-border">
		<h3 class="box-title">{{ $form['title'] }}</h3>
		<div class="box-tools pull-right">
			<button data-widget="collapse" class="btn btn-box-tool" type="button">
				<i class="fa fa-minus"></i>
			</button>
		</div>
	</div>
	<div class="box-body no-padding-side">
		@foreach( $form['fields'] as $field )
			{!! form_row($form_builder->$field['name']) !!}
		@endforeach
	</div>
</div>