<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FruityLDCMS | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 
  <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}" type="text/css" media="all" />
  <link rel="stylesheet" href="{{ asset('assets/css/admin.css') }}" type="text/css" media="all" />
 <style>
 
 
.fa-spin {
  -webkit-animation: fa-spin 2s infinite linear;
  animation: fa-spin 2s infinite linear;
}
.fa-pulse {
  -webkit-animation: fa-spin 1s infinite steps(8);
  animation: fa-spin 1s infinite steps(8);
}
@-webkit-keyframes fa-spin {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
    transform: rotate(359deg);
  }
}
@keyframes fa-spin {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
    transform: rotate(359deg);
  }
}
 
 </style>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>FruityLD</b>CMS</a>
  </div>
  <div class="login-box-heading">
    <p class="login-box-msg"><strong>Please Log In</strong></p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
		@if( $errors->has('type') )
		<div class="text-{{ $errors->first('type') }} text-center">
			<p class="text-uppercase"><strong>{{ $errors->first('title') }}</strong></p>
			<p>{{ $errors->first('message') }}</p>
		</div>
		@endif
		<br>
    <form action="{{ url('login') }}" method="post">
	 {!! csrf_field() !!}
      <div class="form-group has-feedback">
        <input type="text" class="form-control field-shadow-default" placeholder="Username" name="user_login" value="{{ old('user_login') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control field-shadow-default" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember" value="1" ><strong>Remember Me</strong>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p><!-- - OR - --></p>
    </div>
  </div>
  <div class="panel-footer">
    <a href="#">&nbsp;<!--Forgot your password?--></a>
	 <!--<i class="glyphicon glyphicon-cd pull-right fa-spin"></i>-->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
</body>
</html>
