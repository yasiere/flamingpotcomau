@extends('admin.layout.dashboard')
 
@push('style')
	<link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.css') }}" type="text/css" media="all"/>
	@include('developer.media-head')
@endpush

@section('header-type', 'content-layout-form fixed-layout-width')

@section('conten')
<section class="content fixed-layout-width">
	<div class="row">
		<form id="master-form" action="{{ route('admin::updateProfile') }}" method="POST">
			{!! csrf_field() !!}
			<?php $form_builder = $forms['form_builder']; ?>
			<section class="col-md-12 ">
				<div class="row">
					<div class="col-xs-12">
						@foreach( $forms['primary'] as $key => $form )
						<div class="box box-widget">
							@if( $key == 0)
							<div class="box-header"> 
								<h3 class="box-title">Your Profile</h3>
							</div>							
							@endif
							<div class="box-body">
							@foreach( $form['fields'] as $field )
								{!! form_row($form_builder->$field['name']) !!}
							@endforeach
							</div>
						</div> 
						@endforeach
					</div> 
				</div>
			</section>
			<section class="col-md-12 ">
				<div class="content-layout-form content-footer">
					<div class="btn-group">
						<button class="btn btn-primary" type="submit">Update Profile</button>
					</div> 
				</div>
			</section>
		</form>
	</div>
</section>
@stop

@push('footer')
	@include('admin.part.script-form')
	@include('developer.media-footer')
@endpush