<?php
$clone = '';/* ' 
<li class="panel panel-default row_item">
    <div class="panel-heading field_order" role="tab">
        <a class="panel-title" href="#' . $options['real_name'] .'-__NAME__" role="button" data-toggle="collapse"><span class="order_number"></span>. '.  $options['button_label']  .'</a>
        <a href="javascript:;" class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
    </div>
    <div class="panel-collapse collapse in" id="'.  $options['real_name']  .'-__NAME__">
        <div class="repeater-content">
            '. $options['repeater-clone']->render([], false) .'
        </div>
    </div>
</li>'; */
$index = 0;
?>
<ul class="panel-group repeater_fields" data-clone="{{ $clone }}">
@foreach ((array)$options['children'] as $child)
    <?php $index++;?>
	<li class="panel panel-default row_item">
		<div class="panel-heading field_order" role="tab">
			<a class="panel-title" href="#{{ $options['real_name'] }}-{{ $index }}" role="button" data-toggle="collapse">
				<span class="order_number">{{ $index }}</span>. {{ $options['button_label'] }}</a>
			<a href="javascript:;" class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
		</div>
		<div class="panel-collapse collapse in" id="{{ $options['real_name'] }}-{{ $index }}">
			<div class="repeater-content">
				<?php echo $child->render([], false); ?>
			</div>
		</div>
	</li>
@endforeach
	<li class="panel panel-default row_clone">
		<div class="panel-heading field_order" role="tab">
			<a class="panel-title" href="#{{ $options['real_name'] }}-__NAME__" role="button" data-toggle="collapse">
				<span class="order_number"></span>. {{ $options['button_label'] }}</a>
			<a href="javascript:;" class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
		</div>
		<div class="panel-collapse collapse in" id="{{ $options['real_name'] }}-__NAME__">
			<div class="repeater-content">
					{{ $options['repeater-clone']->render([], false) }}
			</div>
		</div>
	</li>';
</ul>