@if($showLabel && $showField)
	@if( $options['wrapper'] !== false )
		<div {!! $options['wrapperAttrs'] !!}>
    @endif
@endif

@if($showLabel && $options['label'] !== false && $options['label_show'])
	{!! Form::label($name, $options['label'], $options['label_attr']) !!} 
@endif

@if( $showField )
		@if( $options['label_before'] || $options['label_after'] )
			<div class="input-group">
		@endif
		
		@if( $options['label_before'] )
			<span class="input-group-addon"><label>{{ $options['label_before'] }}</label></span>
		@endif
		{!! Form::input($type, $name, $options['value'], $options['attr']) !!} 
		@if( $options['label_after'] )
			<span class="input-group-addon"><label>{{ $options['label_after'] }}</label></span>
		@endif
		@if( $options['label_before'] || $options['label_after'] )
			</div> 
		@endif
	@include('vendor.laravel-form-builder.help_block') 
@endif

@include('vendor.laravel-form-builder.errors')

@if ($showLabel && $showField)
	@if ($options['wrapper'] !== false)
    </div>
   @endif
@endif