@if($showLabel && $showField)
	@if( $options['wrapper'] !== false )
		<div {!! $options['wrapperAttrs'] !!}>
    @endif
@endif

@if($showLabel && $options['label'] !== false && $options['label_show'])
	{!! Form::label($name, $options['label'], $options['label_attr']) !!} 
@endif

@if( $showField )
		<?php
		$o = array( 'class' => '', 'url' =>	'' );
		if( $options['value'] )
		{
			$url = get_media_image_src($options['value']);
			$o['url']	= $url[0];
			$o['class'] = 'active'; 
		}
		?>
		<div class="image-uploader {{ $o['class'] }} clearfix">
			<input type="hidden" name="{{ $name }}" value="{{ $options['value'] }}">
			<div class="has-img">
				<ul class="tools">
					<li class="fa btn-i-edit open-media-library"></li>
					<li class="fa btn-i-delete remove-image"></li>
				</ul> 
				<img alt="" src="<?php echo $o['url']; ?>">
			</div>
			<div class="no-img">
				<summary>No image selected &nbsp;
					<input type="button" value="Add Image" class="btn btn-sm btn-default open-media-library">
				</summary>
			</div>
		</div>
		@include('vendor.laravel-form-builder.help_block') 
@endif

@include('vendor.laravel-form-builder.errors')

@if ($showLabel && $showField)
	@if ($options['wrapper'] !== false)
    </div>
   @endif
@endif