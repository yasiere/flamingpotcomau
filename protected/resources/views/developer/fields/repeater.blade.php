@if($showLabel && $showField)
	@if( $options['wrapper'] !== false )
		<div {!! $options['wrapperAttrs'] !!}>
    @endif
@endif

@if($showLabel && $options['label'] !== false && $options['label_show'])
	{!! Form::label($name, $options['label'], $options['label_attr']) !!} 
@endif

@if($showField)
	@include('vendor.laravel-form-builder.help_block') 
	<div class="repeater clearfix"> 
	@if( $options['display'] == 'block' )
		@include('developer.fields.repeater-block', ['options' => $options])
	@else
		@include('developer.fields.repeater-inline', ['options' => $options])
	@endif
		<a href="javascript:;" class="btn btn-primary btn-sm pull-right new-repeater-item"><strong>Add {{ $options['button_label'] }}</strong></a>
	</div>
@endif

@include('vendor.laravel-form-builder.errors')

@if($showLabel && $showField)
	@if ($options['wrapper'] !== false)
    </div>
   @endif
@endif