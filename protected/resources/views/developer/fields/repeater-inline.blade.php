<?php $clone = '';
/* $clone = '
<tr class="row_item">
	<td class="field_order"><span class="order_number">__NAME__</span>.</td>';
	if( $options['repeater-clone']->getType() == 'form' ):
		foreach( $options['repeater-clone']->getChildren() as $children ):
			$clone .= '<td>'. $children->render([], false) .'</td>';
			endforeach;
		else:
			$clone .= '<td>'. $options['repeater-clone']->render([], false) .'</td>';
		endif;
$clone .= '
	<td class="field_action">
		<a class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
	</td>
</tr>'; */
$index = 0;?>
<div class="table-responsive">
<table class="table">
	<thead>
      <tr>
			<th class="field_order">
				<label class="control-label"> No.</label>
			</th>
		@if( $options['repeater-clone']->getType() == 'form' )
			@foreach( $options['repeater-clone']->getChildren() as $children )
			<th>
				<?php echo $children->render([], true, false, false); ?>
			</th>
			@endforeach 
		@else
			<th>
				<?php echo $options['repeater-clone']->render([], true, false, false); ?>
			</th>
		@endif
			<th class="field_action">
			</th>
      </tr>
    </thead>
    <tbody class="repeater_fields" data-clone="{{ $clone }}">
	 @foreach((array)$options['children'] as $childrens)
		<?php $index++;?>
      <tr class="row_item">
			<td class="field_order"><span class="order_number">{{ $index }}</span>.</td>
		@if( $childrens->getType() == 'form' )
			@foreach( $childrens->getChildren() as $children )
			<td>
				<?php echo $children->render([], false); ?>
			</td>
			@endforeach 
		@else
			<td>
				<?php echo $childrens->render([], false); ?>
			</td>
		@endif
			<td class="field_action">
				<a class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
			</td>
      </tr>
	 @endforeach
	<tr class="row_clone">
		<td class="field_order"><span class="order_number"></span>.</td>
		@if( $options['repeater-clone']->getType() == 'form' )
			@foreach( $options['repeater-clone']->getChildren() as $children )
				<td><?php echo $children->render([], false) ;?></td>
				@endforeach
			@else
				<td><?php echo $options['repeater-clone']->render([], false);?></td>
			@endif
			<td class="field_action">
				<a class="btn btn-xs btn-social-icon btn-google remove-repeater-item"><i class="fa fa-bitbucket"></i></a>
			</td>
	</tr> 
	</tbody>
</table>
</div>