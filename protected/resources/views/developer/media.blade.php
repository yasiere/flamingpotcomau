<div class="media-modal" id="media-modal">
   <div class="supports-drag-drop">
      <div class="media-frame md-core-ui hide-menu">
         <div class="media-frame-title">
            <h1>Media Image Library</h1>
            <!--
               <button class="button-link media-modal-close" type="button"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></button>-->
         </div>
         <div class="media-frame-router">
            <div class="media-router hidden"><a href="#" class="media-menu-item">Upload Files</a><a href="#" class="media-menu-item active">Media Library</a></div>
				<div class="nav-tabs-custom">
            <ul class="nav nav-tabs nav-defaults">
              <li><a href="#tab-attachments-upload" data-toggle="tab">Upload Files</a></li>
              <li class="active"><a href="#tab-attachments-view" data-toggle="tab">Media Library</a></li>
            </ul>
            <!-- /.tab-content -->
          </div>
         </div>
         <div class="media-frame-content" data-columns="5">
            <div class="attachments-browser tab-content">
               <div class="uploader-inline tab-pane" id="tab-attachments-upload">
                  <div class="uploader-inline-content no-upload-message">
                     <h3 class="upload-message hidden">No items found.</h2>
                     <div class="upload-ui">
                        <h3 class="upload-instructions drop-instructions">Drop files anywhere to upload</h2>
                        <p class="upload-instructions drop-instructions">or</p>	
								<form method="POST">
									{{ csrf_field() }}
									<span class="btn btn-default btn-lg fileinput-button">
										<span>Select Files</span>
										<input type="file" name="files[]" multiple>
									</span>
								</form>
                     </div>
                     <div class="upload-inline-status"></div>
                     <div class="post-upload-ui">
                        <p class="max-upload-size">Maximum upload file size: 2 MB.</p>
                     </div>
                  </div>
               </div>
					<div id="tab-attachments-view" class="tab-pane active">
						<div class="media-toolbar">
							<div class="media-toolbar-secondary hidden">
								<label for="media-attachment-filters" class="screen-reader-text">Filter by type</label>
								<select id="media-attachment-filters" class="attachment-filters">
									<option value="all">Images</option>
									<option value="uploaded">Uploaded to this page</option>
									<option value="unattached">Unattached</option>
								</select>
								<label for="media-attachment-date-filters" class="screen-reader-text">Filter by date</label>
								<select id="media-attachment-date-filters" class="attachment-filters">
									<option value="all">All dates</option>
									<option value="0">July 2016</option>
									<option value="1">June 2016</option>
								</select>
								<span class="spinner"></span>
							</div>
							<div class="media-toolbar-primary">
								<label class="screen-reader-text hidden-sm hidden-xs">Search Media</label>
								<input type="search" placeholder="Search" id="media-search-input" class="search">
							</div>
						</div>
						<div class="media-sidebar">
							<!--<div class="media-uploader-status" style="display: none;">
								<h2>Uploading</h2>
								<button class="button-link upload-dismiss-errors" type="button"><span class="screen-reader-text">Dismiss Errors</span></button>
								<div class="media-progress-bar">
									<div></div>
								</div>
								<div class="upload-details">
									<span class="upload-count">
									<span class="upload-index"></span> / <span class="upload-total"></span>
									</span>
									<span class="upload-detail-separator">&ndash;</span>
									<span class="upload-filename"></span>
								</div>
								<div class="upload-errors"></div>
							</div>-->
							<div class="attachment-details">
								<h2>
									Image File Info
									<span class="settings-save-status">
										<span class="spinner"></span>
										<span class="saved">
											<small class="label label-success">
												<i class="fa fa-fw fa-save"></i>
												Saved
											</small>
										</span>
									</span>
								</h2>
								<div class="attachment-info">
									<div class="thumbnails thumbnail-image">
										<img alt="" src="">
									</div>
									<div class="details">
										<div class="filename"></div>
										<div class="uploaded hidden">July 30, 2016</div>
										<div class="file-size"></div>
										<div class="dimensions hidden">700 � 933</div>
										<a target="_blank" class="edit-attachment hidden" href="">Edit Image</a>
										<button class="hidden button-link delete-attachment" type="button">Delete Permanently</button>
										<div class="hidden compat-meta"></div>
									</div>
								</div>
								<div class="setting-token hidden">
								{!! csrf_field() !!}
								</div>
								<label data-setting="id" class="setting hidden">
									<input type="hidden" class="input-setting" readonly="readonly">
								</label>
								<label data-setting="url" class="setting">
									<span class="name">URL</span>
									<input type="text" class="input-setting" readonly="">
								</label>
								<label data-setting="title" class="setting">
									<span class="name">Title</span>
									<input type="text" class="input-setting">
								</label>
								<label data-setting="caption" class="setting">
									<span class="name">Caption</span>
									<textarea class="input-setting"></textarea>
								</label>
								<label data-setting="alt" class="setting">
									<span class="name">Alt Text</span>
									<input type="text" class="input-setting">
								</label>
								<label data-setting="description" class="setting">
									<span class="name">Description</span>
									<textarea class="input-setting"></textarea>
								</label>
								<button type="button" class="btn btn-default btn-sm pull-right update">Upadate</button>
							</div>
						</div>
						<div class="fileupload-processing first-loading">
							<div class="fileupload-process"></div>
						</div>
						<ul class="attachments files multiple-not-ready"></ul>
					</div>
            </div>
         </div>
         <div class="media-frame-toolbar">
            <div class="media-toolbar hidden">
               <div class="media-toolbar-secondary" style="width:auto;">
                  <button type="button" class="btn btn-danger btn-sm media-btn-delete" style="margin-top: 15px" disabled="disabled">Delete Selected</button>
              </div>
               <div class="media-toolbar-primary ">
                  <button type="button" class="btn btn-primary btn-sm media-btn-select" style="margin-top: 15px" disabled="disabled">Use Selected</button>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>