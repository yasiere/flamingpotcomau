<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<li role="checkbox" class="attachment save-ready template-upload fade">
			<div class="attachment-preview">
				<div class="thumbnails"> </div>
			</div>
			<div class="attachment-infos">
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-primary" style="width:0%;"></div>
				</div>
				<p class="error text-danger"></p>
			</div>
      	{% if (!i) { %}
			<div class="attachment-tools">
				<button class="cancel btn btn-info btn-outline btn-sm">
					<i class="glyphicon glyphicon-ban-circle"></i>
      			<span>Remove</span>
      		</button>
			</div>
			{% } %}
      </li>
	{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
	{% for (var i=0, file; file=o.files[i]; i++) { %}
		<li role="checkbox" data-id="{%=file.id%}" title="{% if (file.meta) { %} {%=file.meta.title%} {% } %}" class="attachment {% if (! file.error) { %} save-ready {% } %} template-download fade">
		<div class="attachment-preview">
			<div class="thumbnails">
				<div class="centered preview">
					{% if (file.thumbnailUrl) { %}
						<img src="{%=file.thumbnailUrl%}">
					{% } %}
				</div> 
      	</div>
		</div>
		<button type="button" class="button-link check">
			<!--<span class="glyphicon glyphicon-ok"></span>-->
			<span class="media-modal-icon"></span>
			<span class="sr-only">Deselect</span>
		</button>
		{% if (file.error) { %}
		<div class="attachment-infos">
			<p class="error text-danger">{%=file.error%}</p>
		</div>
		{% } %} 
		<div class="attachment-tools {% if (file.deleteUrl) { %} hide-seek {% } %}">
			{% if (file.deleteUrl) { %}
				<button class="delete btn btn-outline btn-danger btn-sm" data-type="{%=file.deleteType%}" data-token="{{ csrf_token() }}" data-url="{%=file.deleteUrl%}" {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				</button>
      	{% } else { %}
				<button class="btn btn-outline btn-info btn-sm cancel">
					<i class="glyphicon glyphicon-ban-circle"></i>
					<span>Remove</span>
				</button>
			{% } %}
			
			{% if (file.url) { %}
				<button class="btn btn-outline btn-info btn-sm select">
					<i class="glyphicon glyphicon-download"></i>
					<span>Select</span>
				</button>
			{% } %}
		</div> 
		</li>
	{% } %}
</script>
<script type="text/javascript">
	var mediaUrl = "{{ route('media::home') }}";
</script>
<script type="text/javascript" src="{{ asset('assets/vendor/fancybox/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/hideseek/jquery.hideseek.min.js') }}"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/vendor/jquery.ui.widget.js') }}"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/tmpl.min.js') }}"></script> 
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/load-image.all.min.js') }}"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/canvas-to-blob.min.js') }}"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload video preview plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-video.js') }}"></script>
<!-- The File Upload validation plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-validate.js') }}"></script>
<!-- The File Upload user interface plugin -->
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/jquery.fileupload-ui.js') }}"></script>
<!-- The main application script -->
<script type="text/javascript" src="{{ asset('assets/vendor/media/media.js') }}"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script type="text/javascript" src="{{ asset('assets/vendor/fileuploader/js/cors/jquery.xdr-transport.js') }}"></script>
<![endif]-->