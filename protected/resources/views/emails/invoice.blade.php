<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Invoice</title>
	<style>
		.invoice-box{
		max-width:800px;
		margin:auto;
		padding:30px;
		border:1px solid #eee;
		box-shadow:0 0 10px rgba(0, 0, 0, .15);
		font-size:16px;
		line-height:24px;
		font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
		color:#555;
		}
		.invoice-box table{
		width:100%;
		line-height:inherit;
		text-align:left;
		}
		.invoice-box table td{
		padding:5px;
		vertical-align:top;
		}
		.invoice-box table tr td:nth-child(2){
		text-align:right;
		}
		.invoice-box table tr.top table td{
		padding-bottom:20px;
		}
		.invoice-box table tr.top table td.title{
		font-size:45px;
		line-height:45px;
		color:#333;
		}
		.invoice-box table tr.information table td{
		padding-bottom:40px;
		}
		.invoice-box table tr.heading td{
		background:#eee;
		border-bottom:1px solid #ddd;
		font-weight:bold;
		}
		.invoice-box table tr.details td{
		padding-bottom:20px;
		}
		.invoice-box table tr.item td{
		border-bottom:1px solid #eee;
		vertical-align: middle;
		}
		.invoice-box table tr.item td:last-child,
		.invoice-box table tr.heading td:last-child{
		text-align: right;
		}
		.invoice-box table tr.item.last td{
		border-bottom:none;
		}
		.invoice-box table tr.subtotal td table td{
			text-align: right;
			font-size: 93%;
			padding: 0;
		} 
		.invoice-box table tr.subtotal td table td:last-child{
			 width: 30%;
		} 
		.invoice-box table tr.total td:nth-child(2){
		border-top:2px solid #eee;
		font-weight:bold;
		}
		@media only screen and (max-width: 600px) {
		.invoice-box table tr.top table td{
		width:100%;
		display:block;
		text-align:center;
		}
		.invoice-box table tr.information table td{
		width:100%;
		display:block;
		text-align:center;
		}
		}
		.no-shadow {
		box-shadow: none !important;
		}
		.well-sm {
		border-radius: 3px;
		padding: 9px;
		}
		.well {
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05) inset;
		margin-bottom: 20px;
		min-height: 20px;
		padding: 19px;
		}
		.text-muted {
		color: #777;
		}
	</style>
</head>
<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="3">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ asset('assets/img/logo.png')}}" style="width:100%; max-width:70px;">
                            </td>
							<td>
                                <strong>Invoice #: {{ $order['invoice_code'] }}</strong><br>
                                <strong>Date: <?php echo(date('m/d/Y')); ?></strong><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="information">
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
                                To<br>
                                <strong>{{ $order['order_meta.shipping.name'] }}</strong><br>
                                {{ $order['order_meta.shipping.street'] }}<br>
                                {{ $order['order_meta.shipping.city'] }},&nbsp; {{ $order['order_meta.shipping.state'] }} {{ $order['order_meta.shipping.zip_code'] }}<br>
                                {{ config('countries.'.$order['order_meta.shipping.country']) }}
                            </td>
                            <td colspan="2">
								From<br>
                                <strong>{{ config('cms.site.shop_title') }}</strong><br>
                                {{ config('cms.site.street') }},&nbsp; {{ config('cms.site.city') }}<br>
                                {{ config('cms.site.state') }} {{ config('cms.site.zip_code') }} <br>
                                {{ config('cms.site.country') }}<br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="heading">
                <td>
                    Item
                </td>
                <td style="width: 70px">
                    Qty
                </td>
                <td>
                    Price
                </td>
            </tr>
			{!! $items !!}
            <tr class="subtotal">
                <td></td>
                <td colspan="2">
					<table>
						<tbody>
						<tr> 
							<td>Subtotal :</td>
							<td>${{ currency($order['order_meta.cost.subtotal']) }} {{ $order['order_meta.cost.currency'] }}</td>
						</tr>
						<tr> 
							<td>Shipping :</td>
							<td>${{ currency($order['order_meta.cost.shipping']) }} {{ $order['order_meta.cost.currency'] }}</td>
						</tr>
						<tr> 
							<td>Tax :</td>
							<td>${{ currency($order['order_meta.cost.tax']) }} {{ $order['order_meta.cost.currency'] }}</td>
						</tr>
					</tbody>
				</table>
				</td>
            </tr>
            <tr class="total">
                <td></td>
                <td colspan="2">
                    TOTAL : &nbsp; ${{ currency($order['order_meta.cost.total']) }} {{ $order['order_meta.cost.currency'] }}
                </td>
            </tr>
        </table>
        <div style="width: 50%;text-align: justify;margin-top: 110px;font-size: 80%">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;margin-bottom: 5px">
				<strong>Information:</strong><br>
				Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg             dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
			</p>
			<p style="margin-top: 5px;font-size: 120%">
				<small>Email: {{ config('cms.site.shop_mail') }}, Phone: {{ config('cms.site.phone') }}</small>
			</p>
        </div>
    </div>
</body>
</html> 