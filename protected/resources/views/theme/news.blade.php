@extends('theme.layout.full')

@section('title', 'News')
@section('keywords', 'News')
@section('description', 'News')

@push('style')
<style>

.bloglist-item-readmore,
.bloglist-item-title {
    color: #1d1d1d;
    font-family: "Rubik";
    font-style: normal;
    font-weight: 300;
    line-height: 1.3em;
    margin-bottom: 10px;
    margin-top: -5px;
    letter-spacing: 0;
		
}
	
.bloglist-item-title { 
    display: block; 
    font-size: 22px; 
}
.bloglist-item-readmore  { 
    display: inline-block; 
    font-size: 14px; 
}

.bloglist-item-content{
	display: block;
	margin-bottom: 5px;
    color: #1d1d1d;
}
.bloglist-item-meta { 
    color: #1d1d1d;
    display: block;
    font-family: "Rubik";
    font-size: 10px;
    font-style: normal;
    font-weight: 300;
    letter-spacing: 0.15em;
    text-align: center;
    text-transform: uppercase;
}

a.grid-link{
	outline:medium none !important;
}
</style>


@endpush

@push('script')
@endpush

@section('body', '')

@section('content')
<section class="content-box">
	<div class="grid-row-40" id="gallery-products">
	@foreach($news as $paper)
		<div class="col-xs-12 col-sm-6 col-md-4 grid-col">
			<a href="{{ route('news.single',['slug'=> $paper->news_slug ]) }}" class="grid-link">
			<div class="grid-inner">
				<figure class="grid-img">
					<div class="grid-img-inner">
						<?php $image = get_media_image_src($paper->news_img, 'smallimage', true);?>
						<img alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}">
					</div>
				</figure>
			</div>
				<section class="text-center">
					<span class="bloglist-item-title">{{ $paper->news_title }}</span>
				</section>
			</a>
			<section class="text-center">
				<!-- <div class="bloglist-item-content">
				@if($paper->news_excerpt)
					{{ strip_tags($paper->news_excerpt) }}
				@else
					{{ str_limit(strip_tags($paper->news_content), 100) }}
				@endif
				</div> -->
				<a href="{{ route('news.single',['slug'=> $paper->news_slug ]) }}" class="bloglist-item-readmore">Read More &#8594; </a>
				<div class="bloglist-item-meta">
					<time>{{ Carbon\Carbon::parse($paper->created_at)->format('F j, Y') }}</time>
				</div>
			</section> 
		</div> 
	@endforeach 
	</div>
</section>
@stop
@push('footer-script')
@endpush 