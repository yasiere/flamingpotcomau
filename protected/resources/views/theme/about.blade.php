@extends('theme.layout.full')

@section('title', 'About') 
@section('keywords', 'About') 
@section('description', 'About')

@section('body', '')

@section('content')
<section class="content-box">
    <div class="row">
        <div class="col-xs-12">
            <div class="fl-block fl-block-html">
                <div class="hentry--content">
                    <h1 class="text-center">{{ $about->page_title }}</h1>
                </div>
            </div>
            <div class="fl-block">
                <div class="hentry--content">
                    <div class="about-image-container">
                        <div class="about-image-inner">
                            <?php $image = get_media_image_src($about->page_img, 'mediumimage', true);?>
                            <img alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="fl-block fl-block-html">
                <div class="hentry--content">
                    {!! $about->page_content !!}
                </div>
            </div>
        </div>
    </div>
</section>
@stop

