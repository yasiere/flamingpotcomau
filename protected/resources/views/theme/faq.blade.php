@extends('theme.layout.full')

@section('title', 'Faq') 
@section('keywords', 'Faq') 
@section('description', 'Faq')

@section('body', '')

@section('content')
<section class="content-box">
    <div class="row"> 
		<div class="col-xs-12">
			<div class="fl-block fl-block-html">
				<div class="hentry--content">
					<h1 class="text-center">{{ $faq->page_title }}</h1>
				</div>
			</div>
			<div class="fl-block fl-block-html">
				<div class="hentry--content">
					<h3>Shipping Policy</h3>
					{!! $faq->page_content !!}
				</div>
			</div>
		</div> 
    </div>
</section>
@stop