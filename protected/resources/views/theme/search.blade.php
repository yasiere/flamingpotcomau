@extends('theme.layout.full')

@section('title', 'Search') 
@section('keywords', 'Search') 
@section('description', 'Search')

@section('body', '')

@section('content')
<section class="content-box">
    <div class="row">
        <div class="col-xs-12">
            <div class="search-page">
                <div class="search-page__find">
                    <div class="spinner-wrapper hidden">
                        <i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>					
                    </div>
                    <form method="get" class="header-page__form" action="{{ route('search.page') }}">
                        <input type="text" placeholder="Type to search..." spellcheck="false" id="header-page__input" name="sq">
                    </form>
                </div>
                <div class="search-page__result">
                    <div class="result-notice hidden">Your search did not match any documents.</div>
                    <div class="result-container">
                        @foreach( $products as $product )
                        <div class="media">
                            <div class="media-left">
                                <a href="{{ route('shop.detail',['slug'=> $product->product_slug ]) }}" class="media-object">
									<?php $image = get_media_image_src(head($product->product_img), 'mediumimage', true);?>
									<img class="img-responsive" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}"> 
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">
                                    <a href="{{ route('shop.detail',['slug'=> $product->product_slug ]) }}">{{ $product->product_title }}</a>
                                </h4>
                                <div class="hentry--content">
                                    {{ str_limit(strip_tags($product->product_desc), 355) }}
                                </div>
                            </div>
                        </div>
                        @endforeach 
                    </div>
                </div>
                <div class="search-page__more hidden">
                    <button class="btn btn-fl btn-fl-lg btn-pink" value="Send" type="submit">See more</button>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 