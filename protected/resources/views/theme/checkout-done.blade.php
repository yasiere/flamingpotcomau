<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1">
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png"/>
    <title>Checkout - Flamingpot</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <script type="text/javascript" src="{{ asset('assets/vendor') }}/typekit/ik/yiTcTx9SAJKwZfzr53wUic2zxGLSm8EW_1Bj6h_H6fbfeGM2fFHN4UJLFRbh52jhWD9aFQycweJUwcZ8wQwUZemyFRJXjAFqwU7sMPG0SeBudeIlSc8DjAoXdkoDSWmyScmDSeBRZPoRdhXCZcUydA80ZagzifoRdhX0jW9Cde"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Rubik:400,300,300italic,700normal,700italic,500"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"/>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.ui.1.11.4.min.js') }}"></script>
    <!--[if IE 8]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-checkout">
<section class="container checkout-container">
<div class="row">
	<div class="col-sm-12">
        <div class="store-brand">
            <a href="{{ url('') }}"><img src="{{ asset('assets/img/logo.png') }}"></a>
        </div>
    </div>
	@if(Session::has('alert'))
	<div class="col-sm-12">
		@include('theme.part.content.alert')
	</div>
	@endif
</div>
<div class="row">
	<div class="box box-white empty">
		<div class="box-header">
			<h2 class="box-title">Checkout</h2>
		</div>
		<div class="box-body">
			<div class="empty-message">
				Thanks for checkout.&nbsp;
				<a href="{{ route('shop.page') }}">Continue Shopping</a>
			</div>
		</div>
	</div> 
</div>
</section> 
</body>
</html>