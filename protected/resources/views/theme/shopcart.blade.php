@extends('theme.layout.full')

@section('title', 'Shopping Cart')
@section('keywords', 'Shopping Cart')
@section('description', 'Shopping Cart')

@section('body', 'class="page-shoppingcart"')

@section('content')
	<section class="content-box">
		<div class="row">
			<div class="col-xs-12 shoppingcart-_wrapper @if(Cart::content()->isEmpty()) empty @endif">
				<h3 class="page-title">Shopping Cart</h3>
				<div class="loading-spinner">
					<div class="fl-spin dark">
						<div class="fl-spin__content"></div>
					</div>
				</div>
				@if( ! Cart::content()->isEmpty() ) 
					<table class="table cart-table">
						<thead>
						<tr>
							<th width="50px"></th>
							<th class="item">Item</th>
							<th></th>
							<th class="quantity">Quantity</th>
							<th class="price">Price</th>
							
						</tr>
						</thead>
						<tbody>
							@foreach(Cart::content() as $item) 
							<tr class="cart-row" data-id="{{ $item->rowId }}" >
								<td class="remove text-center">
									<div class="remove-item">×</div>
								</td>
								<td class="item">
									<div class="image-box image-box-100">
										<div class="image-box--wrapper">
											<div class="image-box--inner">
												<img src="{{ product_img_src($item->options->image) }}" alt="image">
											</div>
										</div>
									</div>
								</td>
								<td class="item-desc">
									<a href="{{ route('shop.detail', ['slug'=> $item->options->uri ])}}">{{ $item->name }}</a>
									<div class="variant-info">
									@foreach( $item->options->variant as $variant )
										{{ $variant }}</br>
									@endforeach
									</div>
								</td>
								<td class="quantity">
									<input name="quantity" value="{{ $item->qty }}" size="1">
								</td>
								<td class="price">
									<span class="currency-code-aud">
										<span class="money-native">{{ $item->subtotal( 2, '.' ,',') }}</span>
									</span>
								</td>
								
							</tr>
							@endforeach 
						</tbody>
					</table>
					<div class="subtotal cart_subtotal">
						<span class="label">Subtotal</span>
						<span class="price currency-code-aud"><span class="money-native">{{ Cart::subtotal(2, '.' ,',') }}</span></span>
					</div>
					<div class="checkout">
						<a href="{{ route('checkout.page') }}" class="btn-fl btn-pink btn-sm">CHECKOUT</a>
					</div> 
				@endif
				<div class="empty-message">
					You have nothing in your shopping cart.&nbsp;
					<a href="{{ route('shop.page') }}">Continue Shopping</a>
				</div>
			</div>
		</div> 
	</section>
@stop

@push('footer-script')
<script type="text/javascript" src="{{ asset('assets/js/cart.dmz.js') }}"></script>
@endpush