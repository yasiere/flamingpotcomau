<section class="quick-view-wrapper hentry">
    <div class="row">
        <div class="col-md-7">
            <div class="clearfix">
                <div class="detail-images">
                    <span class="zoompict"> 
						<?php $image = get_media_image_src(head($product->product_img), 'mediumimage', true);?>
						<img class="img-responsive" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}">							
                    </span>
                </div>
                <div id="owl-demo" class="owl-carousel">
                    @foreach( $product->product_img as $images) 
						<?php $src = get_media_image_src($images, 'original');?>
						<div class="item" data-src="{{ asset($src[0]) }}">
							<?php $image = get_media_image_src($images, 'thumbnail', true);?>
							<img class="lazyOwl" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" data-src="{{ asset($image[0]) }}">							
						</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <h1 class="product-title">{{ $product->product_title }}</h1>
            <div class="product-price currency-code-aud">
                <span class="money-native">{{ $product->product_price }}</span>
            </div>
            <div class="product-detail">
                {{ str_limit(strip_tags($product->product_desc), 155) }}
            </div>
						@foreach((array)$product->product_variant as $variant)
						<div class=" option-cart option-variant">
							<label>{{ $variant['variant_name'] }}:</label>
							@if( isset($variant['variant_option']) )
								<select class="field-select2" style="min-width: 133px" data-name="{{ $variant['variant_name'] }}">
								@foreach((array)$variant['variant_option'] as $option) 
									<option value="{{ $option }}">{{ $option }}</option> 
								@endforeach
								</select>
							@endif
						</div>
						@endforeach
            <div class="option-cart-addbtn">
                <div class="option-cart option-cart-qty">
                    <label>Quantity :</label>
                    <input size="4" max="9999" min="1" value="1" step="1" type="number">
                </div>
                <button class="btn btn-fl btn-pink" cart="add-to" data-id="{{ $product->productId }}">Add to Cart</button>
            </div>
            <a class="quick-view-fullitem-link" href="{{ route('shop.detail',['slug'=> $product->product_slug ]) }}">
				<span>View Full Item</span>
            </a>
        </div>
    </div>
</section>