<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="initial-scale=1">
	<meta charset="utf-8" />
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.png') }}"/>
	<title>@yield('title') — Flamingpot</title>
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="@yield('keywords')">
    @yield('head')
	<script type="text/javascript" src="{{ asset('assets/vendor') }}/typekit/ik/yiTcTx9SAJKwZfzr53wUic2zxGLSm8EW_1Bj6h_H6fbfeGM2fFHN4UJLFRbh52jhWD9aFQycweJUwcZ8wQwUZemyFRJXjAFqwU7sMPG0SeBudeIlSc8DjAoXdkoDSWmyScmDSeBRZPoRdhXCZcUydA80ZagzifoRdhX0jW9Cde"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Rubik:400,300,300italic,700normal,700italic,500"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"/>
	@stack('style')
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.ui.1.11.4.min.js') }}"></script>
    @stack('script')

	<meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="@yield('title')" />
	<meta property="og:description"   content="@yield('description')" />
	<meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />

    <!--[if IE 8]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

	
    <![endif]-->
	<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
</head>
<body @yield('body')>
	<div class="boxed-container">
		<header class="header stieku">
			<div class="header-inner">
				<div class="header-inner__left stimenu">
					<nav class="main-nav">
						<div class="main-nav__inner">
							@include('theme.part.header.nav')
						</div>
					</nav>
				</div>
				<div class="header-inner__center">
					<a class="site-brand hidden-xs" href="{{ url('') }}" style="outline: 0">
						<img class="img-responsive" alt="Flamingpot" src="{{ asset('assets/img/logo.png') }}" data-pin-nopin="true">
					</a>
				</div>
				<div class="header-inner__right">
					<div class="header-search">
						<form method="get" action="{{ route('search.page') }}" class="header-search__form">
							<input type="text" class="header-search__text" placeholder="Search" autocomplete="off" spellcheck="false" name="sq">
							<input type="submit" class="header-search__submit" value=" ">&nbsp;
						</form>
					</div>
					<a href="#" class="searchbar-toggle">
						<svg class="icon icon--search" viewBox="0 0 20 20">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#search-icon"></use>
						</svg>
						<svg viewBox="0 0 16 16" class="icon icon--close">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#close-icon"/>
						</svg>
					</a>
					<a class="header-cart" href="{{ route('shop.shoppingcart') }}">
						<span class="header-cart__inner">
							<span class="header-cart__label"></span>
							<svg viewBox="0 0 34 38" class="icon icon--bag">
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--odd" class="use--odd"/>
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--even" class="use--even"/>
							</svg>
							<svg viewBox="0 0 31 26" class="icon icon--cart hidden">
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--odd" class="use--odd"/>
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--even" class="use--even"/>
							</svg>
							<span class="header-cart__quantity">{{ Cart::count() }}</span>
						</span>
					</a>
					 <button class="menubar-toggle">
						<svg class="icon icon--hamburger" viewBox="0 0 24 18">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--even" class="use--even"></use>
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--odd" class="use--odd"></use>
						</svg>
					</button>
				</div>
			</div>
		</header>	



		<header class="header headerku">
			<div class="header-inner">
				<div class="header-inner__left">
					<nav class="main-nav">
						<div class="main-nav__inner">
                            @include('theme.part.header.nav')
						</div>
					</nav>
					<a class="site-brand visible-xs-block" href="{{ url('') }}">
						<img class="img-responsive" alt="Flamingpot" src="{{ asset('assets/img/logo.png') }}" data-pin-nopin="true">
					</a>
				</div>

				<div class="header-inner__center">
					<a class="site-brand hidden-xs" href="{{ url('') }}" style="outline: 0">
						<img class="img-responsive" alt="Flamingpot" src="{{ asset('assets/img/logo.png') }}" data-pin-nopin="true">
					</a>
				</div>
				<div class="header-inner__right">
					<div class="header-search">
						<form method="get" action="{{ route('search.page') }}" class="header-search__form">
							<input type="text" class="header-search__text" placeholder="Search" autocomplete="off" spellcheck="false" name="sq">
							<input type="submit" class="header-search__submit" value=" ">&nbsp;
						</form>
					</div>
					<a href="#" class="searchbar-toggle">
						<svg class="icon icon--search" viewBox="0 0 20 20">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#search-icon"></use>
						</svg>
						<svg viewBox="0 0 16 16" class="icon icon--close">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#close-icon"/>
						</svg>
					</a>
					<a class="header-cart" href="{{ route('shop.shoppingcart') }}">
						<span class="header-cart__inner">
							<span class="header-cart__label"></span>
							<svg viewBox="0 0 34 38" class="icon icon--bag">
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--odd" class="use--odd"/>
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--even" class="use--even"/>
							</svg>
							<svg viewBox="0 0 31 26" class="icon icon--cart hidden">
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--odd" class="use--odd"/>
								<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--even" class="use--even"/>
							</svg>
							<span class="header-cart__quantity">{{ Cart::count() }}</span>
						</span>
					</a>
					 <button class="menubar-toggle">
						<svg class="icon icon--hamburger" viewBox="0 0 24 18">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--even" class="use--even"></use>
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--odd" class="use--odd"></use>
						</svg>
					</button>
				</div>
				<div id="mySidenav" class="mobile-nav">
					<div class="mobile-nav__inner">
					<nav class="mobile-nav__primary">
                        @include('theme.part.header.nav')
					</nav>
					</div>
					<button class="mobile-nav__closebtn">
						<svg viewBox="0 0 16 16" class="icon icon--close">
							<use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#close-icon"/>
						</svg>
					</button>
				</div>
			</div>
		</header>