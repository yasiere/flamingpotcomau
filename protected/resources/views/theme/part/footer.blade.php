		<footer class="footer">
            <div class="footer-inner">
                <div class="footer-inner__middle"> 
                    <nav class="footer-nav">
                        <div class="Footer-nav-group text-center">
							<a href="{{ route('faq.page') }}" class="nav-item">FAQ</a>
						</div>
                    </nav>
                </div>
                <div class="footer-inner__bottom">
                    <div class="row">
                        <div class="col-xs-12">
							<p class="text-center"><span style="font-size:12px">© 2016 - Flamingpot. All Rights Reserved.</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer> 
	</div> <!-- End .boxed-container header.php -->
	<script type="text/javascript"> 
		var FLBase = {url: "{{ url('') }}", csrf: "{{ csrf_token() }}"};
	</script>
	<script type="text/javascript" src="{{ asset('assets/js/flaminpot-bundled.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/flaminpot.js') }}"></script>
	@stack('footer-script')
	
	<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=11118783; 
var sc_invisible=1; 
var sc_security="840726d9"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify site
analytics" href="http://statcounter.com/shopify/"
target="_blank"><img class="statcounter"
src="//c.statcounter.com/11118783/0/840726d9/1/"
alt="shopify site analytics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

<script>
    var position = $(window).scrollTop(); 
    $(window).scroll( function(){
        var bottom_of_object = $(".headerku").outerHeight();
        var bottom_of_window = $(window).scrollTop();

        if( bottom_of_window > bottom_of_object ){
            
            // var scroll = $(window).scrollTop();
            // if(scroll > position) {
            //     $(".stieku").fadeOut();
            // } else {
                $(".stieku").fadeIn("fast");
            // }
            // position = scroll;

        }
        else{
            $(".stieku").fadeOut("fast");
        }

    });

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51794299-51', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>