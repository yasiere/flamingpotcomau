<div class="header header--mobile">
		 <div class="mobile-header-bar">
            <div class="header-inner__left">
                <a href="index.html" class="mobile-header-brand">
					<img src="http://static1.squarespace.com/static/5758f0b12fe131dea624acd3/t/5758f11dab48deeba35ef8f3/1472131107164/?format=1500w" alt="Flamingpot" class="img-responsive"/>
                </a>
            </div>
            <div class="header-inner__center"></div>
            <div class="header-inner__right"> 
                <button class="mobile-bar-menu">
                    <svg class="icon icon--hamburger" viewBox="0 0 24 18">
                        <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--even" class="use--even"></use>
                        <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--odd" class="use--odd"></use>
                    </svg>
                </button>
			</div>
        </div><!--
        <div class="Mobile-bar Mobile-bar--bottom" data-nc-group="bottom" sqs-controller="MobileOffset">
            <div data-nc-container="bottom-left">
                <button class="Mobile-bar-menu" data-nc-element="menu-icon" data-controller-overlay="menu" sqs-controller="MobileOverlayToggle">
                    <svg class="Icon Icon--hamburger" viewBox="0 0 24 18">
                        <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--even" class="use--even"></use>
                        <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#hamburger-icon--odd" class="use--odd"></use>
                    </svg>
                </button>
            </div>
            <div data-nc-container="bottom-center">
                <a href="commerce/show-cart.html" class="Cart sqs-custom-cart" data-nc-element="cart">
                    <span class="Cart-inner">
                        <span class="Cart-label"></span>
                        <svg class="Icon Icon--bag" viewBox="0 0 34 38">
                            <use class="use--odd" xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--odd"></use>
                            <use class="use--even" xlink:href="{{ asset('assets') }}/img/ui-icons.svg#bag-icon--even"></use>
                        </svg>
                        <svg class="Icon Icon--cart" viewBox="0 0 31 26">
                            <use class="use--odd" xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--odd"></use>
                            <use class="use--even" xlink:href="{{ asset('assets') }}/img/ui-icons.svg#cart-icon--even"></use>
                        </svg>
                        <span class="sqs-cart-quantity">0</span>
                    </span>
                </a>
            </div>
            <div data-nc-container="bottom-right">
                <a href="search.html" class="Mobile-bar-search" data-nc-element="search-icon">
                    <svg class="Icon Icon--search" viewBox="0 0 20 20">
                        <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#search-icon"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="Mobile-overlay">
            <div class="Mobile-overlay-menu" sqs-controller="MobileOverlayFolders">
                <div class="Mobile-overlay-menu-main">
                    <nav class="Mobile-overlay-nav Mobile-overlay-nav--primary" data-content-field="navigation">
                        <a href="index.html" class="Mobile-overlay-nav-item">
                        Home
                        </a>
                        <a href="shop/index.html" class="Mobile-overlay-nav-item">
                        Shop
                        </a>
                        <a href="new-page-3/index.html" class="Mobile-overlay-nav-item">
                        About
                        </a>
                        <a href="contact/index.html" class="Mobile-overlay-nav-item">
                        Contact
                        </a>
                    </nav>
                    <nav class="Mobile-overlay-nav Mobile-overlay-nav--secondary" data-content-field="navigation">
                    </nav>
                </div>
                <div class="Mobile-overlay-folders" data-content-field="navigation">
                </div>
            </div>
            <button class="Mobile-overlay-close" sqs-controller="MobileOverlayToggle">
                <svg class="Icon Icon--close" viewBox="0 0 16 16">
                    <use xlink:href="{{ asset('assets') }}/img/ui-icons.svg#close-icon"></use>
                </svg>
            </button>
            <div class="Mobile-overlay-back" sqs-controller="MobileOverlayToggle"></div>
        </div>-->
		
		
		
		
		
		
		
		
		
		
		
		</div>