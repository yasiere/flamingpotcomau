@foreach($siteMenu->roots() as $item) 
	<a href="{!! $item->url() !!}" @lm-attrs($item) class="nav-item" @lm-endattrs> {!! $item->title !!} </a>
@endforeach