<?php 
switch(Session::get('alert.class'))
{
	case 'danger':
		$alert = '<i class="icon fa fa-ban"></i> Error!';
		break;
	case 'success':
		$alert = '<i class="icon fa fa-check"></i> Success!';
		break;
	case 'warning':
		$alert = '<i class="icon fa fa-warning"></i> Warning!';
		break;
	default:
		$alert = '<i class="icon fa fa-info"></i> Info!';
}
?>
<div class="alert alert-{{ Session::get('alert.class') }} alert-dismissible text-left">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
	<h4>{!! $alert !!}</h4>
	{{ trans(Session::get('alert.message')) }}
</div> 