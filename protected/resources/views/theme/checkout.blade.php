<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1">
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.png"/>
    <title>Checkout - Flamingpot</title>
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">
    <script type="text/javascript" src="{{ asset('assets/vendor') }}/typekit/ik/yiTcTx9SAJKwZfzr53wUic2zxGLSm8EW_1Bj6h_H6fbfeGM2fFHN4UJLFRbh52jhWD9aFQycweJUwcZ8wQwUZemyFRJXjAFqwU7sMPG0SeBudeIlSc8DjAoXdkoDSWmyScmDSeBRZPoRdhXCZcUydA80ZagzifoRdhX0jW9Cde"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Rubik:400,300,300italic,700normal,700italic,500"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"/>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendor/jquery/jquery.ui.1.11.4.min.js') }}"></script>
    <!--[if IE 8]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-checkout">
<section class="container checkout-container">
<div class="row">
	<div class="col-sm-12">
        <div class="store-brand">
            <a href="{{ url('') }}"><img src="{{ asset('assets/img/logo.png') }}"></a>
        </div>
    </div>
	@if(Session::has('alert'))
	<div class="col-sm-12">
		@include('theme.part.content.alert')
	</div>
	@endif
</div>
<div class="row">
	<div class="col-sm-12 col-md-6 col-md-push-6">
		<?php $cart_class = (Cart::content()->isEmpty())? ' empty': '';?> 
		<div class="box box-white shoppingcart-_wrapper{{ $cart_class }}">
			<div class="loading-spinner">
				<div class="fl-spin dark">
					<div class="fl-spin__content"></div>
				</div>
			</div>
			<div class="box-header">
				<h2 class="box-title">Shopping Cart</h2>
			</div>
			<div class="box-body">
				@if( ! Cart::content()->isEmpty() ) 
				<table class="table cart-table">
					<thead>
					<tr>
						<th class="item">Item</th>
						<th></th>
						<th class="quantity">Quantity</th>
						<th class="price">Price</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@foreach(Cart::content() as $item) 
						<tr class="cart-row" data-id="{{ $item->rowId }}" >
							<td class="item">
								<div class="image-box image-box-50">
									<div class="image-box--wrapper">
										<div class="image-box--inner">
											<img src="{{ product_img_src($item->options->image) }}" alt="image">
										</div>
									</div>
								</div>
							</td>
							<td class="item-desc">
								<span>{{ $item->name }}<span>
								<div class="variant-info">
									@foreach( $item->options->variant as $variant )
										{{ $variant }}</br>
									@endforeach
								</div>
							</td>
							<td class="quantity">
								<input name="quantity" value="{{ $item->qty }}" size="3">
							</td>
							<td class="price">
								<span class="currency-code-aud">
									<span class="money-native">{{ $item->subtotal( 2, '.' ,',') }}</span>
								</span>
							</td>
							<td class="remove">
								<div class="remove-item"> x </div>
							</td>
						</tr>
					@endforeach 
					</tbody>
				</table>
				<div class="checkout-subtotal cart_subtotal">
					<span class="label">Subtotal</span>
					<span class="price currency-code-aud">
						<span class="money-native">{{ Cart::subtotal(2, '.' ,',') }}</span>
					</span>
				</div>
				<div class="checkout-shipping">
					<span class="label">Shipping</span>
					<span class="price">--</span>
				</div>
				<div class="checkout-tax">
					<span class="label">Tax</span>
					<span class="price currency-code-aud">
						<span class="money-native">{{ Cart::tax(2, '.' ,',') }}</span>
					</span>
				</div>
				<div class="checkout-total cart_total">
					<div class="label">Grand Total</div>
					<div class="price currency-code-aud">
						<span class="money-native">{{ Cart::total(2, '.' ,',') }}</span>
					</div>
				</div> 
				@endif
				<div class="empty-message">
					You have nothing in your shopping cart.&nbsp;
					<a href="{{ route('shop.page') }}">Continue Shopping</a>
				</div>
			</div>
		</div> <!--
		<div class="box box-white">
			<div class="box-header">
				<h2 class="box-title">Coupons</h2>
			</div>
			<div class="box-body">
				<div class="input-group">
					<input class="form-control" placeholder="Promo Code">
					<span class="input-group-btn">
						<button class="btn btn-black" type="button">Redeem</button>
					</span>
				</div>
			</div>
		</div> -->
	</div>
	<div class="col-sm-12 col-md-6 col-md-pull-6">
		<div class="row">
			<form method="POST" action="{{ route('checkout.proses') }}" class="col-sm-12 use-validation hide-label" id="checkout-form">
				{!! csrf_field() !!}
				<input type="hidden" id="shippingCost" name="shippingCost"> 
				<div class="box box-white form-widget editing" data-form="shipping">
					<div class="loading-spinner">
						<div class="fl-spin dark">
							<div class="fl-spin__content"></div>
						</div>
					</div>
					<div class="box-header">
						<h2 class="box-title">Contact &amp; Shipping<a href="javascript:;" class="edit-fieldset">Edit</a></h2>
					</div>
					<fieldset class="box-body">
						<label>Your Email Address</label>
						<!-- Email Address -->
						<div class="form-group">
						{!! form_row($shipping_form->shippingEmail) !!}
						</div>
						<label>Shipping Address</label> 
						<div class="form-group">
							{!! form_row($shipping_form->shippingName) !!}
						</div>
						<div class="form-group">
						{!! form_row($shipping_form->shippingAddress) !!}
						</div>
						<div class="form-group">
						{!! form_row($shipping_form->shippingCountry) !!}
						</div>
						<div class="row form-row">
							<div class="col-sm-5">
								<div class="form-group">
								{!! form_row($shipping_form->shippingCity) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								{!! form_row($shipping_form->shippingState) !!}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
								{!! form_row($shipping_form->shippingZip) !!}
								</div>
							</div>
						</div>
						<div class="form-group">
						{!! form_row($shipping_form->shippingPhone) !!}
						</div>
						<div class="text-center form-widget-summary">
							<button class="btn btn-black" type="submit">Continue</button>
						</div>
					</fieldset>
				</div>
				<!--<div class="box box-white form-widget incomplete">
					<div class="loading-spinner">
						<div class="fl-spin dark">
							<div class="fl-spin__content"></div>
						</div>
					</div>
					<div class="box-header">
						<h2 class="box-title">Delivery Options<a href="javascript:;" class="edit-fieldset">Edit</a></h2>
					</div>
					<fieldset class="box-body">
						<label>Choose a Delivery Option</label>
						<div id="shipping-options" style="margin-bottom: 20px">
							<div id="" class="sqs-shipping-options-list-content empty">
								<div class="empty-message">
									You cannot continue checkout because there are no shipping options available.
								</div>
								<div class="loading-spinner">
									<div class="fl-spin dark">
										<div class="fl-spin__content"></div>
									</div>
								</div>
								<div class="options-container"></div>
							</div>
						</div> 
						<div class="text-center">
							<button class="btn btn-black" type="submit">Continue</button>
						</div>
					</fieldset>
				</div>-->
				<div class="box box-white form-widget incomplete" data-form="shipping">
					<div class="loading-spinner">
						<div class="fl-spin dark">
							<div class="fl-spin__content"></div>
						</div>
					</div>
					<div class="box-header">
						<h2 class="box-title">
							Billing
							<a href="javascript:;" class="edit-fieldset">Edit</a>
							<div class="checkout-cards">
								@foreach( ['mastercard', 'visa', 'americanexpress', 'paypal'] as $card )
									<img alt="card-{{ $card }}" src="{{ asset('assets/img/cards') }}/{{ $card }}.png">
								@endforeach
							</div> 
						</h2>
					</div>
					<fieldset class="box-body">
						<label>Pyment Method</label>
						<div class="form-group">
							<div class="well well-md">
								<div class="form-group" style="margin-bottom: 0">
									<div class="radio" style="margin-top: 0">
										<label style="font-weight: 700">
											<input name="paymentMethod" id="paymentMethod" value="paypal" checked="true" step="" style="margin-top: 1px" type="radio">
											Paypal
										</label>
									</div>
									<div class="radio" style="margin-bottom: 0">
										<label style="font-weight: 700">
											<input name="paymentMethod" id="paymentMethod" value="stripe" step="" src="" style="margin-top: 3px" type="radio">
											Credit Card (Stripe)
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="stripe-billing">
						<label>Billing Address</label>
						<div class="checkbox">
							<label> <input type="checkbox" name="copyFromShipping" id="copyFromShipping">&nbsp;Use Shipping Address</label>
						</div> 
						<div class="form-group">
							{!! form_row($billing_form->billingName) !!}
						</div>
						<div class="form-group">
						{!! form_row($billing_form->billingAddress) !!}
						</div>
						<div class="form-group">
						{!! form_row($billing_form->billingCountry) !!}
						</div>
						<div class="row form-row">
							<div class="col-sm-5">
								<div class="form-group">
								{!! form_row($billing_form->billingCity) !!}
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
								{!! form_row($billing_form->billingState) !!}
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
								{!! form_row($billing_form->billingZip) !!}
								</div>
							</div>
						</div>
						<div id="credit-card">
							<label class="label-credit-card">Secure Payment</label>
							<div class="form-group">
								<div class="well well-lg">
										<div class="form-group">
											<div class="field required" data-label="Card Number">
												<label for="cardNumber" class="control-label required">Card Number</label>
												<input class="form-control" size="20" data-stripe="number" placeholder="Card Number" id="cardNumber" type="text"> 
											</div>
										</div>
										<div class="row form-row">
											<div class="col-sm-3">
												<div class="form-group">
												{!! form_row($card_form->cardExpiryMonth) !!}
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
												{!! form_row($card_form->cardExpiryYear) !!}
												</div>
											</div>
											<div class="col-sm-2">
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<div class="field required" data-label="CVC">
														<label for="cardCVC" class="control-label required">CardCVC</label>
														<input class="form-control" size="4" placeholder="CVC" data-stripe="cvc" id="cardCVC" type="text">
													</div>
												</div>
											</div>
										</div>
								</div>
								<div id="comfort">
									All transactions are secure and encrypted, and we never store your credit card information. Payments are
									processed through Stripe. Payment information is also governed by
									<a target="_blank" href="https://stripe.com/privacy">Stripe's privacy policy</a>. Or use paypal and u don't need to insert credit card.
								</div>
							</div>
						</div>
						</div>
						<div class="text-center form-widget-summary">
							<button class="btn btn-black" type="submit">Proceed</button> 
						</div>
					</fieldset>
				</div>
			</form>
        </div>
    </div>
</div>
</section>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
	var FLBase = {url: "{{ url('') }}", csrf: "{{ csrf_token() }}"};
	Stripe.setPublishableKey('{{ config("services.stripe.key") }}');
</script>
<script type="text/javascript" src="{{ asset('assets/js/cart.dmz.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/checkout.dmz.js') }}"></script>
</body>
</html>