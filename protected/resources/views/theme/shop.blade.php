@extends('theme.layout.full')

@section('title', 'Shop')
@section('keywords', 'Shop')
@section('description', 'Shop')

@push('style')
	<link rel="stylesheet" href="{{ asset('assets/vendor/owl-carousel/owl.carousel.css') }}" type="text/css" media="all"/>
	<link rel="stylesheet" href="{{ asset('assets/vendor/owl-carousel/owl.theme.css') }}" type="text/css" media="all"/>
	<link rel="stylesheet" href="{{ asset('assets/vendor/fancybox/jquery.fancybox.css') }}" type="text/css" media="all"/>
	<link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.css') }}" type="text/css" media="all"/>
@endpush

@push('script')
	<script type="text/javascript" src="{{ asset('assets/vendor/fancybox/jquery.fancybox.js') }}"></script>
@endpush

@section('body', '')

@section('content')
<section class="content-box">
        <nav class="gallery-nav">
            <label class="gallery-nav__dropdown">
				<span class="dropdown-label">Category</span>
				<span class="dropdown-icon">
					<i class="fa fa-angle-up"></i>
					<i class="fa fa-angle-down"></i>
				</span>
            </label> 
            <ul class="gallery-nav__list">
                <li class="{{ empty($filter)? 'active': ''}}">
                    <a href="#" data-filter="*">All</a>
                </li>
                @foreach($categories as $category)
					@if( ! $category->products->isEmpty() )
						<li class="{{ ($filter == $category->category_slug)? 'active': ''}}"> 
							<a href="#" data-filter=".{{ $category->category_slug }}">{{ $category->category_title }}</a>
						</li>
					@endif
                @endforeach 
            </ul>
        </nav>
        <div class="grid-row-40" id="gallery-products">
		@foreach($products as $product)
			<a class="col-xs-12 col-sm-6 col-md-4 grid-col {{ $product->category->category_slug }}" href="{{ route('shop.detail',['slug'=> $product->product_slug ]) }}">
				<div class="grid-inner">
                    <figure class="grid-img">
                        <div class="grid-img-inner">
                            <?php $break = false; ?>
                            @foreach( $product->product_img as $images) 
								<?php $image = get_media_image_src($images, 'smallimage', true);?>
								<img class="{{ $break ? 'grid-image--2nd': '' }}" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}">
								@if($break)
									@break
								@endif
								<?php $break = true;?>
                            @endforeach 
                        </div>
                        <div class="grid-hover-inner">
                            <span href="{{ route('shop.detail',['slug'=> $product->product_slug ]) }}" class="quick-view-button">Quick View</span>
                        </div>
                    </figure>
                    <section class="text-center">
                        <h1 class="grid--title">{{ $product->product_title }}</h1>
                        <div class="product-price currency-code-aud">
                            <span class="money-native">{{ $product->product_price }}</span>
                        </div>
                    </section>
				</div>
			</a>
		@endforeach 
        </div> 
</section>
@stop
@push('footer-script')
<script type="text/javascript" src="{{ asset('assets/js/cart.dmz.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/vendor/select2/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/isotope/isotope.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendor/zoom/zoom.min.js') }}"></script>
<script>
(function($){ 
	$(document).ready(function(){
		// init Isotope
		var $grid = $('#gallery-products').isotope({
			layoutMode: 'fitRows',
			itemSelector: 'a.grid-col',
			filter: '{{ empty($filter)? "*": (".". $filter) }}',
		}); 

		// bind filter button click
		$('.gallery-nav').on( 'click', 'li > a', function(e) {
			var $this = $(this);

			e.preventDefault();

			if( $this.hasClass('active') )
				return;

			var filterValue = $( this ).attr('data-filter');

			$this.closest('ul').children('li').removeClass('active'); 
			$this.closest('li').addClass('active');
			$grid.isotope({ filter: filterValue });
		}); 

		// toggle nav
		$('.gallery-nav__dropdown').on('click', function(){
			$(this).closest('.gallery-nav').toggleClass('open');
		});
		
		$(document).on('click', '.owl-item > .item', function(e) {
			var src = $(this).attr('data-src');
			
			$('.zoompict').trigger('zoom.destroy');
			$('.zoompict').html('<img class="img-responsive" alt="" src="'+ src +'" width="100%" height="100%">');
			$('.zoompict').zoom({magnify: 1.7});
		});
		if (!!$.prototype.fancybox){
			$('.quick-view-button').fancybox({
				'padding': 0,
				'type': 'ajax',
				afterShow: function(){
					$('.zoompict').zoom({magnify: 1.7});
					$('.field-select2').select2();
					$("#owl-demo").owlCarousel({
						items : 8,
						itemsMobile : [479,3],
						lazyLoad : true,
						navigation : true
					});
				}
			});
		}
	});
})(jQuery);  
</script>
@endpush 