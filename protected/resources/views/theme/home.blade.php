@extends('theme.layout.full')

@section('title', 'Home') 
@section('keywords', 'Home') 
@section('description', 'Home')

@section('body', 'class="page-home"')

@section('content') 
	<section class="home-panels bg-parallax">
		<div class="parallax">
			<img src="{{ asset( $banner[1]['image'][0] ) }}">
		</div>
		<div class="home-content">
			<div class="row">   
				<div class="col-xs-12 col-md-7">
					<div class="hentry--content">
						<span class="paralax-title">{!! $banner[1]['title'] !!}</span>						
					</div> 
				</div>
				<div class="col-xs-12 col-md-5">
					<div class="hentry--content">{!! $banner[1]['text'] !!}
					</div>
				</div>
			</div>
		</div>
	</section> 
	<section class="home-panels">
		<div class="home-content">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						<h1>{!! $banner[2]['title'] !!}</h1> 
						<h3>{!! $banner[1]['link'] !!}</h3>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						{!! $banner[2]['text'] !!}
					</div>
				</div>
			</div>
		</div>
	</section> 
	<section class="home-gallery">
		<div class="grid-row-40">
		 @foreach($donnuts as $donnut)
			<a class="col-xs-6 col-md-6 grid-col" href="{{ route('shop.page') }}/{{ $donnut->product_slug}}"> 
				<div class="grid-inner">
					<figure class="grid-img">
						<div class="grid-img-inner">
							<?php $image = get_media_image_src(head($donnut->product_img), 'mediumimage', true);?>
							<img alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}" data-pin-nopin="true">
						</div>
					</figure>
				</div>
			</a>
		@endforeach 
		</div>
	</section> 
	<section class="home-panels bg-parallax">
		<div class="parallax">
			<img src="{{ asset( $banner[4]['image'][0] ) }}">
		</div>
		<div class="home-content">
			<div class="row">   
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						{!! $banner[4]['text'] !!}
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content text-right">
						<span class="paralax-title">{!! $banner[4]['title'] !!}</span>
						<span class="paralax-subtitle ">{!! $banner[4]['link'] !!}</span>
					</div> 
				</div> 
			</div>
		</div>
	</section> 
	<section class="home-panels">
		<div class="home-content">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						<h1>{!! $banner[5]['title'] !!}</h1> 
					</div> 
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content"> 
						{!! $banner[5]['text'] !!}
					</div>
				</div> 
			</div> 
		</div>
	</section>
	<section class="home-gallery">
		 <div class="grid-row-40">
		@foreach($cupcakes as $cupcake)
			<a class="col-xs-6 col-md-6 grid-col" href="{{ route('shop.page') }}/{{ $cupcake->product_slug}}"> 
				<div class="grid-inner">
					<figure class="grid-img">
						<div class="grid-img-inner">
							<?php $image = get_media_image_src(head($cupcake->product_img), 'mediumimage', true); ?>
							<img alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}" data-pin-nopin="true">
						</div>
					</figure>
				</div>
			</a>
		@endforeach
		</div>
	</section> 
	<section class="home-panels bg-parallax">
		<div class="parallax">
			<img src="{{ asset( $banner[7]['image'][0] ) }}">
		</div>
		<div class="home-content">
			<div class="row">   
				<div class="col-xs-12 col-md-5">
					<div class="hentry--content">
						<span class="paralax-title">{!! $banner[7]['title'] !!}</span>
						<span class="paralax-subtitle">{!! $banner[7]['link'] !!}</span>
					</div> 
				</div> 
			</div>
		</div>
	</section>
	<section class="home-panels">
		<div class="home-content">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						<h1>Make The<br />Connection</h1>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="hentry--content">
						<p>Find and follow us for updates, promotions or just some colour to your day!</p>
						<p>
							<a target="_blank" href="https://www.instagram.com/flamingpot/?hl=en">Instagram</a><br/>
							<a target="_blank" href="https://www.facebook.com/flamingpot/">Facebook</a><br/>
							<a href="https://www.etsy.com/au/shop/Flamingpot?ref=l2-shopheader-name#about">Etsy</a>
						</p>
					</div>
					<div class="fl-block">
						<div class="hentry--content">
							<hr/>
						</div>
					</div>
					
					
					
					
					<div class="row">
					<div class="col-xs-12 col-md-11 col-lg-11">
					<div class="newsletter-form-wrapper">
					<h2 style="text-transform: uppercase; font-weight: inherit; font-size: 31px;">Subscribe</h2>
					<p class="f15"> Sign up with your email address to receive the latest updates from Flamingpot!</p>
					<form class="newsletter-form hide-label" method="POST">
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="row">
									<div class="col-xs-12 col-md-12">
										<div class="form-group">
											<div class="field required" data-label="First Name">
												<label for="fname" class="control-label required">First Name</label>
												<input class="field-control inp-sub" required="required" placeholder="First Name" name="fname" id="fname" type="text">
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-md-12">
										<div class="form-group">
											<div class="field required" data-label="Last Name">
												<label for="lname" class="control-label required">Last Name</label>
												<input class="field-control inp-sub" required="required" placeholder="Last Name" name="lname" id="lname" type="text">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<div class="field required" data-label="Email">
										<label for="email" class="control-label required">Email Address</label>
										<input class="field-control inp-sub" required="required" name="email" placeholder="Email Address" id="email" type="email">
									</div>
								</div>
							</div>
						</div>
						<div  class="text-left">
						<input class="btn btn-fl btn-pink" value="Subscribe" type="submit">
						<p class="help-block">We respect your privacy.</p>
						</div>
						
						
						
						
						
						
					</form>
				</div>
					<div class="hidden form-submission-text">Thank you!</div>
				</div>
				</div>
					
					
					
					
					
					
					
					
				</div>
			</div>
		</div>
	</section>
	<section class="home-panels">
		<div class="home-content">
			<div class="grid-row-20"> 
			@foreach($galleries as $gallery)
				<a class="col-xs-6 col-md-4 grid-col" href="{{ route('shop.page') }}/{{ $gallery->product_slug}}"> 
					<div class="grid-inner">
						<figure class="grid-img">
							<div class="grid-img-inner">
								<?php $image = get_media_image_src(head($gallery->product_img), 'smallimage', true);?>								
								<img alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}"  data-pin-url="{{ route('shop.page') }}/{{ $gallery->product_slug}}"  data-pin-media="{{ asset($image[0]) }}"  data-pin-description="{{ $image['meta']['description'] }}">
							</div>
						</figure>
					</div>
				</a>
			@endforeach 
			</div>
		</div>
	</section>
@stop 


@push('footer-script')
<script src="//assets.pinterest.com/js/pinit.js" type="text/javascript" async defer data-pin-hover="true"></script>


<script type="text/javascript">
	var subscribe = {
		status: true,
		post: {},
		run: function( $el ){
			var _t = this;
			
			_t.status = true;
			_t.summary = [];
			
			$el.find('.field.required').each(function(){
				_t.validate( $(this) );
			});
		},
		validate: function($form){
			var _t = this,
				valid = true;
				
			// another form type
			if( $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').exists() )
			{
				var name = $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').attr('name');
				subscribe.post[name] = $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').val(); 
				
				if( ! $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').val() )
				{
					valid = false;
				}
			} 
			
			if( ! valid )
			{
				// update status
				_t.status = false;
				var label = $form.data('label');
				
				// show error
				$form.addClass('has-error');
				$form.append('<div class="label label-danger" style="font-family: arial;font-weight:500;">'+ label +'is required.</div>');
			}
		}
	};

	$(document).on('submit', 'form.newsletter-form', function(){
			var $this = $(this);

			subscribe.run($this);
			if( ! subscribe.status )
			{
				return false;
			}
			
			var data = subscribe.post;
			
			data['_token'] = FLBase.csrf;
			$.post(FLBase.url +'/subscribe-news', data, function( result ) {
				$(".form-submission-text").removeClass('hidden').show();
				$(".newsletter-form-wrapper").addClass('hidden');
				
			});
			
			return false; 
	});









</script>








@endpush 