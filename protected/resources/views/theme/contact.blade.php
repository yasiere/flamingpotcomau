@extends('theme.layout.full')

@section('title', 'Contact') 
@section('keywords', 'Contact') 
@section('description', 'Contact')

@section('body', '')

@section('content') 
<section class="content-box">
    <div class="row">
        <div class="col-xs-12">
            <div class="fl-block fl-block-html">
                <div class="hentry--content">
                    <h1 class="text-center">{{ $contact->page_title }}</h1>
                    {!! $contact->page_content !!}
                </div>
            </div>
				<?php $mail_send = '';?>
				@if( session('mail') )
					<?php $mail_send = 'email-send';?>
				@endif
            <div class="fl-block fl-block-html">
                <div class="hentry--content {{ $mail_send }}">
                    <form action="{{ route('contact.mail') }}" method="POST">
							{!! csrf_field() !!}
                        <div class="field-list">
                            <fieldset class="form-group">
                                <label class="control-label required">Name</label>
                                <div class="row">
                                    <div class="col-xs-6"> 
                                        {!! form_widget($form->fname) !!}
                                        <span class="help-block">First Name</span>
										{!! form_errors($form->fname) !!}
                                    </div>
                                    <div class="col-xs-6">
                                        {!! form_widget($form->lname) !!}
                                        <span class="help-block">Last Name</span>
										{!! form_errors($form->lname) !!}
                                    </div>
                                </div>
                            </fieldset>
							{!! form_rest($form) !!}
							<div class="form-button-wrapper form-button-wrapper--align-left">
								<input class="btn btn-fl btn-pink" type="submit" value="Send"/>
							</div>
                        </div>
                        <div class="form-submission-text">
                            <p>Thank you! We'll get back to you as soon as possible!</p>
                        </div>
                        <div class="form-submission-html"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

