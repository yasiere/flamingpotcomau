@extends('theme.layout.full')

@section('title', $product->product_title) 
@section('keywords', 'Home') 
@section('description', str_limit(strip_tags($product->product_desc), 200))

@push('style')
	<link rel="stylesheet" href="{{ asset('assets/vendor/select2/select2.css') }}" type="text/css" media="all"/>
@endpush

@section('body', '')

@section('content')
	<section class="content-box">
		<article class="hentry">
			<div class="single-top-bar">
				<div class="breadcrumbs pull-left">
					<a class="nav-item" href="{{ route('shop.page') }}">Shop</a>
					<span class="nav-item">{{ $product->product_title }}</span>
				</div>
				<nav class="pull-right">
					<ul class="pager-nav">
						<li class="{{ $pager['prev.class'] }}"><a href="{{ $pager['prev.link'] }}">&lt; &nbsp;Previous</a></li>
						<li><span>/</span></li>
						<li class="{{ $pager['next.class'] }}"><a href="{{ $pager['next.link'] }}">Next&nbsp; &gt;</a></li>
					</ul>
				</nav>
			</div>
			<div class="container__inner">
				<div class="row">
					<div class="col-sm-12 col-md-7"> 
						@foreach( $product->product_img as $images)  
							<div class="clearfix">
								<div class="detail-images">
								<span class="zoompict"> 
									<?php $image = get_media_image_src($images, 'original', true);?>
									<img class="img-responsive" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}">							
								</span>
								</div>
							</div>
						@endforeach
					</div>
					<div class="col-sm-12 col-md-5" style="padding-left: 30px;">
						<h1 class="product-title">{{ $product->product_title }}</h1>

						<span class="product-price currency-code-aud" style="color: #faa74b;">
							<span class="money-native">{{ $product->product_price }}</span>
						</span>
						@if(!empty($product->product_diskon) || $product->product_diskon <> "0")
						<span class="product-price currency-code-aud" style="color: rgba(0,0,0,.5);">
							<span class="money-native" style="text-decoration: line-through;">{{ $product->product_diskon }} </span>
						</span>
						@endif
						<span class="product-price" style="color: #faa74b;"> 
							Incl GST
						</span>
						


						<br><br>
						<div class="product-detail">
							{!! $product->product_desc !!}
						</div>

						@foreach((array)$product->product_variant as $variant)
						<div class="option-variant option-cart">
							<label>{{ $variant['variant_name'] }}:</label> 
							@if( isset($variant['variant_option']) )
								<select class="field-select2" style="min-width: 133px" data-name="{{ $variant['variant_name'] }}">
								@foreach((array)$variant['variant_option'] as $option) 
									<option value="{{ $option }}">{{ $option }}</option> 
								@endforeach
								</select>
							@endif
						</div>
						@endforeach
						<div class="option-cart-addbtn">
							<div class="option-cart option-cart-qty">
								<label>Quantity :</label>
								<input size="4" max="9999" min="1" value="1" step="1" type="number">
							</div>
							<button class="btn btn-fl btn-fl-lg btn-pink" cart="add-to" data-id="{{ $product->productId }}">Add to Cart</button>
						</div>
						
						<a href="https://www.facebook.com/sharer/sharer.php?kid_directed_site=0&sdk=joey&u={{ Request::url() }}&display=popup&ref=plugin&src=share_button" class="fa fa-facebook iconshare" target="_blank"></a>
						<a href="https://twitter.com/intent/tweet?url={{ Request::url() }}&text={{str_limit(strip_tags($product->product_desc), 200)}} ..." class="fa fa-twitter iconshare" target="_blank"></a>
						<a href="https://plus.google.com/share?url=https%3A%2F%2Fwww.flamingpot.com.au%2Fshop%2Ftoddlers-cowl" class="fa fa-google-plus iconshare" target="_blank"></a>
					
					</div>
				</div>
			</div>
		</article>
	</section>
@stop


@push('footer-script')
	<script type="text/javascript" src="{{ asset('assets/vendor/select2/select2.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/cart.dmz.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/vendor/zoom/zoom.min.js') }}"></script>
	<script type="text/javascript">
		(function($) {
			'use strict';
			
			$(document).ready(function() { 
				$('.zoompict').zoom({magnify: 1.7});
			});
			$( document ).ready(function() {
				$('.field-select2').select2();
			}); 
		})(jQuery); 
	</script>
@endpush