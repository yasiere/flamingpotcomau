@extends('theme.layout.full')

@section('title', $news->news_title) 
@section('keywords', 'Home') 
@section('description', 'Home')

@push('style')
<style>
	.blogitem-title{ 
		color: #1d1d1d;
		font-family: "Rubik";
		font-size: 35px;
		font-style: normal;
		font-weight: 300;
		letter-spacing: 0;
		line-height: 1.5em;
		margin: 0 0 32px;
	}
	
	@media (min-width: 768px) {
		.blogitem-title{
			font-size: 60px;
		}
	}
	
	.image-block-news{
		max-width:700px; 
		text-align: center;
		display: inline-block;
		margin: 0 auto 20px; 
	}
</style>


@endpush

@section('body', '')

@section('content')
	<section class="content-box">
		<article class="hentry">
			<div class="single-top-bar">
				<h1 class="blogitem-title">{{ $news->news_title }}</h1>
				<div class="bloglist-item-meta">
					<time>{{ Carbon\Carbon::parse($news->created_at)->format('F j, Y') }}</time>
				</div>
			</div>
			<div class="container__inner text-center">
				<div class="row">
					<div class="col-sm-12"> 
					@if($news->news_img) 
						<div class="image-block-news">
							<?php $image = get_media_image_src($news->news_img, 'original', true);?>
							<img class="img-responsive" alt="{{ $image['meta']['alt_text'] }}" width="{{ $image[1] }}" height="{{ $image[2] }}" src="{{ asset($image[0]) }}"> 
						</div> 
					@endif
					</div>
				<div class="col-sm-12">
					{!! $news->news_content !!}
				</div>
				</div>
			</div>
		</article>
	</section>
@stop


@push('footer-script') 
<script src="//assets.pinterest.com/js/pinit.js" type="text/javascript" async defer data-pin-hover="true"></script>
@endpush