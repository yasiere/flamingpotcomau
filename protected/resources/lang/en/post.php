<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the post class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
	
	'success' => [
		'add' => 'Data was created successfully.',
		'edit' => 'Success, data updated .',
		'delete' => 'Success, data deleted.',
	],
	'errors' => [
		'field' => 'Validation Failed. One or more fields below are required.',
	],
];