(function($){
	// check element exist
	$.fn.exists = function()
	{
		return (0 < $(this).length);
	};
})(jQuery);

/* media image handling
 * base from jquery file upload by bluimp
*/
(function($){

	var Media = {
        colection: {files:{}},
        selection: {files:{}},
        init: function() {
            var $content = $('.media-modal .media-frame-content');

            $content.fileupload({
                url: mediaUrl
            });

            $content.fileupload('option', {
                autoUpload: true,
                maxFileSize: 9999000,
                //maxFileSize: 9,
					 prependFiles: true,
                previewMaxWidth: 150,
                previewMaxHeight: 150,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
            });
			
            $content.bind('fileuploaddone', function(e, data) {
					Media.addcolection(data.result.files);
            });
			
			$content.bind('fileuploadadd', function (e, data) {
				$('#tab-attachments-upload').removeClass('active');
				$('#tab-attachments-view').addClass('active');
				$('[href="#tab-attachments-upload"]').parent().removeClass('active');
				$('[href="#tab-attachments-view"]').parent().addClass('active');
			});
			
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $content.fileupload('option', 'url'),
                dataType: 'json',
                context: $content[0]
            }).always(function() {
                $(this).removeClass('fileupload-processing');
				$('#loading-on-load').remove();
            }).done(function(result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {
                        result: result
                    });
				onLoadMedia(result);
            });
			
			var onLoadMedia = function(data)
			{
				if( ! data.files || ! data.files.length )
				{
					$('#tab-attachments-view').removeClass('active');
					$('#tab-attachments-upload').addClass('active');
					return;
				}
				
				Media.addcolection(data.files);
			};
			
			
			$('#Media-search-input').hideseek({
				list:           '.attachments.files',
				nodata:         '',
				attribute:      'title',
				highlight:      false,
				ignore:         '',
				navigation:     false,
				ignore_accents: false
			}); 
        },
        addcolection: function(files) {
			if( ! Media.colection.files )
			{
				Media.colection.files = {};
			}
			
			$.each(files, function(i, file){
				Media.colection.files[file.id] = file;
			});
			return;
        },
		initMultiBtn: function(){
			var prp = $('.media-modal ul.attachments .attachment.selected').exists()? false: true;
			$('.media-toolbar .multiple-used').prop("disabled", prp);
			$('.media-toolbar .multiple-remove').prop("disabled", prp);
		}
    }; 

    $(document).on('click', '.media-toolbar .multiple-remove', function() {
		console.log('click')
		if( $(this).prop("disabled") ) return;
		console.log('click no disabled')
		$('.media-modal .attachments .save-ready.selected').each(function(){
			$(this).find('button.delete').trigger('click');
		});
    });

    $(document).on('click', '.attachment.save-ready .thumbnails', function() {
		$(this).closest('li.attachment').find('.button-link.check').trigger('click');
    });
	
	$(document).on('click', '.attachment.save-ready .button-link.check', function() {
		if($(this).closest('li.attachment').hasClass('selected')){
			$(this).closest('li.attachment').removeClass('details selected');
			Media.initMultiBtn();
			return;
		}
		
		var $detail = $('.media-sidebar .attachment-info > .details');
		var id = $(this).closest('li.attachment').data('id');
		var item = Media.colection.files[id];
		var blue = $('.media-frame .media-frame-content').data('blueimp-fileupload');
		$('.media-sidebar .attachment-info > .thumbnails img').attr('src', item.thumbnailUrl);
		
		$detail.find('.filename').html(item.name);
		$detail.find('.file-size').html(  blue._formatFileSize(item.size)  );
		var defaults = {
			url: item.url,
			alt: null,
			title: null,
			caption: null,
			description: null,
		};
		
		var info = $.extend({}, defaults, item.info );
		$('.media-sidebar .setting').each(function() {
			var name = $( this ).data('setting');
			if( $(this).find('input').exists() )
			{
				$(this).find('input').val(info[name]);
				return;
			}
			
			$(this).find('textarea').val(info[name]);
		});
		$('.media-sidebar .attachment-details').removeClass('hidden');
		if(! $(this).closest('ul.attachments').hasClass('multiple-ready') )
		{
			$(this).closest('ul.attachments').find(' > .details').each(function(){
				$(this).removeClass('details selected');
			});
		}
		$(this).closest('.attachment.save-ready').addClass('details selected');
		Media.initMultiBtn();
    });
	/*
    $(".fancyMediaLibrary").fancybox({
        type: 'ajax',
        fitToView: false,
        width: '90%',
        height: '90%',
		showCloseButton: false,
        ajax: {
            complete: Media.init
        },
        ajax: {
            complete: function() {
				Media.init();
			}
        },
    });*/
	
    $(".fancyMediaLibrary").fancybox({
        type: 'ajax',
        margin: 0,
        padding: 0,
        fitToView: false,
        width: '90%',
        height: '90%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        ajax: {
            complete: Media.init
        },
        iframe: {
            scrolling: 'auto',
            preload: true
        }
    });
})(jQuery);
