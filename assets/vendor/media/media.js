(function($){
	// check element exist
	$.fn.exists = function()
	{
		return (0 < $(this).length);
	};
})(jQuery);

/* media image handling
 * base from jquery file upload by bluimp
 * this is beta version first so must update. 
*/
(function($){
	'use strict'; 
	
	var Media = {
		traget: null,
		$content: null,
		colection: {},
		selection: {},
		hasSelected: false,
		init: function(){
			// reset colection
			Media.$content = $('.media-modal .media-frame-content');
			
			// ini jquery file 
			var $content = Media.$content;
			
			$content
				.fileupload({url: mediaUrl + '/method'})
				.fileupload('option', {
					autoUpload: true,
					//maxFileSize: 9999000,
					maxFileSize: 2001000,//2MB
					prependFiles: true,
					previewMaxWidth: 150,
					previewMaxHeight: 150,
					acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
				})
				.bind('fileuploaddone', function(e, data) {
					Media.addColection(data.result.files);
				})
				.bind('fileuploadadd', function (e, data) {
					Media.selectViewTab(true);
					$('.media-sidebar .attachment-details').hide();
				});
				
			$.ajax({
				// Uncomment the following to send cross-domain cookies:
				//xhrFields: {withCredentials: true},
				url: $content.fileupload('option', 'url'),
				dataType: 'json',
				context: $content[0]
			})
			.always(function() {
				$('.first-loading').remove();
			})
			.done(function(result) {
				$(this).fileupload('option', 'done')
					.call(this, $.Event('done'), {result: result});
				
				if( result.files && result.files.length ){
					Media.addColection(result.files);
					return;
				}
				
				Media.selectViewTab(false);
			});
			
			// init jquery hideseek	
			$('#media-search-input').hideseek({
				list: 'ul.files',
				attribute: 'title',
			});
			Media.setColumns();
		},
		
		destroy: function(){
			Media.traget = null;
			Media.colection = {};
			Media.hasSelected = false;
			Media.$content.fileupload('destroy');
		},
		
		addColection: function(files) {
			$.each(files, function(i, file){
				Media.colection[file.id] = file;
			});
		},
		
		selectViewTab: function(yes){
			var view = '#tab-attachments-view',
				upload = '#tab-attachments-upload';
				
			if( ! yes ){
				$(view).removeClass('active');
				$(upload).addClass('active');
				return;
			}
			
			$(view).addClass('active');
			$(upload).removeClass('active');
			$('[href="' + view + '"]').parent().addClass('active');
			$('[href="' + upload + '"]').parent().removeClass('active');
		},
		
		getAttachmentSelected: function(){
			return $('ul.attachments > .selected');
		},
		
		propMainBtn: function(){
			var disabled = Media.getAttachmentSelected().exists()? false: true;

			Media.hasSelected = disabled;
			$('.media-btn-select').prop("disabled", disabled);
			$('.media-btn-delete').prop("disabled", disabled);
		},
		
		multipleDelete: function(){
			if( ! Media.hasSelected )
				return;
				
			Media.getAttachmentSelected().each(function(){
				$(this).find('button.delete').trigger('click');
			});
		},
		
		thumbClick: function(){
			var $parent = $(this).closest('li.attachment');
			
			if( $parent.hasClass('selected') ){
				$parent.removeClass('details selected');
				Media.propMainBtn();
				return;
			}
			
			var $sidebarInfo = $('.media-sidebar .attachment-info'),
				CollectionId = $parent.data('id'),
				item = Media.colection[CollectionId],
				blueimp = $('.media-frame .media-frame-content').data('blueimp-fileupload'),
				meta = item.meta;
				
			meta.id = item.id;
			meta.url = item.url;
			
			$sidebarInfo.find('> .thumbnails img').attr('src', item.url);
			$sidebarInfo.find('> .details .filename').html(item.name);
			$sidebarInfo.find('> .details .file-size').html(blueimp._formatFileSize(item.size));
			
			$('.media-sidebar .setting').each(function() {
				var $this = $(this),
					key = $this.data('setting');
				
				$this.find('.input-setting').val(meta[key]);
			});
			
			$sidebarInfo.closest('.attachment-details').show();
			$('.multiple-not-ready > li').removeClass('details selected');
			$parent.addClass('details selected');
			Media.propMainBtn();
		},
		
		useSelected: function(){
			/* must update to dynamicly sekection */
			var $parent = $(this).closest('li.attachment'),
				CollectionId = $parent.data('id'),
				item = Media.colection[CollectionId],
				$parent = Media.traget.closest('.image-uploader');
				
			$parent.find('img').attr('src', item.thumbnailUrl);			
			$parent.children('input[type="hidden"]').val(item.id);
			$parent.addClass('active');
			$.fancybox.close();
		},
		
		setColumns: function() {
			var prev = $('.media-frame-content').attr( 'data-columns', 0 ),
				width = $('.attachments.files').width(),
				idealColumnWidth = $( window ).width() < 640 ? 135 : 150;
	
			if ( width ) {
				this.columns = Math.min( Math.round( width / idealColumnWidth ), 12 ) || 1;
	
				if ( ! prev || prev !== this.columns ) {
					$('.attachments.files').closest( '.media-frame-content' ).attr( 'data-columns', this.columns );
				}
			}
		},
		
		updateInfo: function(){
			var data = {},
				$that = $(this),
				$spinner = $('.media-sidebar .attachment-details'),
				btnUpdate, url;
				
			url = mediaUrl + '/update';
			data._token = $('.media-sidebar .setting-token input').val();
			btnUpdate = function( prop ){
				var txt = prop ?'updating...': 'update';
				
				$that.prop('disabled', prop).text(txt);
			};
			
			btnUpdate(true);
			$spinner.addClass('save-waiting');
			$('.media-sidebar .setting').each(function() {
				var $this = $(this),
					key = $this.data('setting');
					
				data[key] = $this.find('.input-setting').val();
			});
			
			$.post(url, data, function(meta){
				Media.colection[meta.mediaID].meta = meta.media_meta;
			}, 'json')
			.always(function() {
				btnUpdate(false);
				$spinner.removeClass('save-waiting').addClass('save-complete');
				setTimeout(function(){ $spinner.removeClass('save-complete');}, 400);
			});
		}
	};
	
	$(document)
		.on('click', '.select', Media.useSelected)
		.on('click', '.media-btn-delete', Media.multipleDelete)
		.on('click', '.media-sidebar button.update', Media.updateInfo)
		.on('click', '.attachment.save-ready .thumbnails', Media.thumbClick)
		.on('click', '.attachment.save-ready .button-link.check', Media.thumbClick)
		.on('click', '.open-media-library', function(){ Media.traget = $(this) });
		
	
	$( window ).resize(function() {
		Media.setColumns();
	});
	
	$(".open-media-library").fancybox({
		href: mediaUrl,
		type: 'ajax',
		margin: 0,
		padding: 0,
		width: '90%',
		height: '90%',
		autoSize: false,
		fitToView: false,
		openEffect: 'none',
		closeEffect: 'none',
		afterShow: Media.init,
		beforeClose: Media.destroy,
		//closeClick: false,
		//ajax: {
		//	complete: Media.init
		//},
		//tpl:{
			//closeBtn : '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;">close</a>',
		//},
		//iframe: {
		//	scrolling: 'auto',
		//	preload: true
		//}
	});
})(jQuery);