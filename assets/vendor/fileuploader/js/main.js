/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
$(function () {
    'use strict';

	var imageUploaderCb = { 
		btnAdd: function($el, action){
			$el.find('.no-image')[action]();
		},
		inputUpdate: function($el, value){
			var input = $el.find('.input-image-uploader');
			input.val(value);
		}
	};
	
	var imgUploader = {
		start: function(){
			$('.image-file-uploader')
				.bind('fileuploaddone', function (e, data) {
					imageUploaderCb.inputUpdate( $(this), data.result.files[0].name );
				})
				.bind('fileuploaddestroyed', function (e, data) {
					var $this = $(this);
					imageUploaderCb.inputUpdate( $this, '' );
					imageUploaderCb.btnAdd( $this, 'show' );
				})
				.bind('fileuploadfail', function (e, data) {
					imageUploaderCb.btnAdd( $(this), 'show' );
				})
				.bind('fileuploadadd', function (e, data) {
					imageUploaderCb.btnAdd( $(this), 'hide' );
				});
				
			// Initialize the jQuery File Upload widget:
			$('.image-file-uploader').fileupload({
				url: uploderurl
			});
			
			$('.image-file-uploader').fileupload('option', {
				maxFileSize: 999000,
				previewMaxWidth: 175,
				previewMaxHeight: 175,
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
			});
			
			$('.fileinput-button input').on('change', function(){
				$.ajax({
					// Uncomment the following to send cross-domain cookies:
					//xhrFields: {withCredentials: true},
					url: $('.image-file-uploader').fileupload('option', 'url'),
					dataType: 'json',
					context: $('.image-file-uploader')[0]
				}).always(function () {
					$(this).removeClass('fileupload-processing');
				}); 
			});
		},
		destroyed: function(){
			$('.fileinput-button input')
				.off('change')
				.fileupload('destroy');
		}
	}; 
	
	$(document).on('dmz/setup_fields', function(e, el){
		if( $(el).find('.image-file-uploader').exists())
		{
			imgUploader.start();
		}
	});
	
	$(document).ready(imgUploader.start);
});
