(function($, cartBase){
	var Cart = {
		updateInfo: function( cart ){
			$('.header-cart__quantity').text(cart.qtyTotal);
			$('.cart_total .money-native').text(cart.total);
			$('.checkout-tax .money-native').text(cart.tax);
			$('.cart_subtotal .money-native').text(cart.subTotal);
			$(document).trigger('update/shipping');
		},
		loading: function(){
			$('.shoppingcart-_wrapper').addClass('loading-active');
		},
		
		done: function(qty){
			var $wrapper = $('.shoppingcart-_wrapper');
			if(0 == qty.qtyTotal)
			{
				$wrapper.addClass('empty');
			}
			
			$wrapper.removeClass('loading-active');
		},
	};
	
	$(document).on('click', '[cart="add-to"]', function(e){
		var $this = $(this);
		
		$this.text('adding...').prop('disabled', true);
		
		var variant = {};
		$('.option-variant select').each(function(){
			var name = $(this).data('name');
			var value = $(this).val();
			variant[name] = value;
		}); 
		var data = {
			_token: cartBase.csrf,
			key: $this.attr('data-id'),
			qty: $this.prev('.option-cart-qty').find('input').val() || 1,
			opt: variant
		}
		
		$.post(cartBase.url + '/add-to-cart', data, function(qty) {
			$('.header-cart__quantity').html(qty);
			$this.text('Added');
		})
		.fail(function(){
			$this.text('Error');
		})
		.always(function(){
			setTimeout(function(){
				$this.text('Add to Cart').prop('disabled', false);
			}, 800);
		});
	});
	
	$(document).on('click', 'tr.cart-row .remove-item', function(e){
		Cart.loading() 
		var $this = $(this); 
		var $row = $this.closest('tr.cart-row');  
		var rowId = $row.data('id');
		var url = cartBase.url + '/remove-cart-item/' + rowId;
		
		$.get(url, function(qty) {
			qty = JSON.parse(qty);

			$row.remove();
			Cart.updateInfo(qty);
			Cart.done(qty);
		}); 
	});
	
	
	$(document).on('change', 'tr.cart-row .quantity input', function(e){
		var $this = $(this); 
		var value = parseInt($this.val()) || 1; 
		
		if(value < 1 )
		{
			$this.val(1);
			return;
		} 
		
		Cart.loading() 
		var $row = $this.closest('tr.cart-row');  
		var rowId = $row.data('id'); 
		var $thisTotal	= $row.find('td.price .money-native'); 
		var url = cartBase.url + '/update-cart-item/' + rowId + '/' + value;
		
		$.get(url, function(qty) {
			qty = JSON.parse(qty);
			$this.val(value);
			$thisTotal.text(qty.itemTotal);
			Cart.updateInfo(qty); 
			Cart.done(qty);
		}); 
	}); 
})(jQuery, window.FLBase); 