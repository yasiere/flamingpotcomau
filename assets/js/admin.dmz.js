/***
| base
|*/
var DMZ = {
	uniqId: null
};

(function($){

	// check element exist
	$.fn.exists = function()
	{
		return (0 < $(this).length);
	};
	
	$.fn.isClone = function()
	{
		var name = $(this).attr('name');
		
		if( name && ( name.indexOf('[__NAME__]') == -1 ) )
		{
			return false;
		}

		return true;
	};
	
	// create unique id from date get time
	DMZ.uniqId = function()
	{
		var newDate = new Date();

		return newDate.getTime();
	};
	
})(jQuery);

/***
| Repeater
|*/
(function($, uniqId){
	var Repeater = 
	{
		add: function($btn)
		{
			var $container, $clone;
			
			$container = $btn.closest('.repeater').find('.repeater_fields').first();
			$clone = $container.children('.row_clone');console.log($clone);
			$items = $clone.clone().removeClass('row_clone').addClass('row_item');
			/* $rowItem  = $rowClone.clone().removeClass('repeater-clone').addClass('repeater-item');
			 */
			$items.html(function(i,h){
				return h.replace(/__NAME__/g, uniqId());
			});
			
			$clone.before($items);
			//$container.append($clone);
			
			Repeater.order($container);
			
			// setup fields
			$(document).trigger('dmz/setup_fields', $items);
			
			$(document).trigger('add/imgUploader');
		},
		
		order: function( $container ){
			$container.children('.row_item').each(function(i){
				$(this).find('> .field_order .order_number').html( i+1 );				
			});
		},
		
		remove: function($el){
			var $item, $container;

			$item = $el.closest('.row_item');
			$container = $item.parent();
			$(document).trigger('dmz/remove_fields', $item);
			$item.remove();
			Repeater.order($container);
		}
	};
	
	$( document ).on('click', '.repeater > .new-repeater-item', function(e){
		Repeater.add($(this));
		e.preventDefault();
	});
	
	$( document ).on('click', '.repeater .remove-repeater-item', function(e){console.log('clickremove');
		Repeater.remove($(this));
		e.preventDefault();
	});
	
})(jQuery, DMZ.uniqId);

/***
| image uploader
|*/
(function($){
	var ImageUploader = function( $el ){
		var $parent = $el.closest('.image-uploader');
				
		$parent.find('img').attr('src', '');			
		$parent.children('input[type="hidden"]').val('');
		$parent.removeClass('active');
	};

	$( document ).on('click', '.image-uploader .remove-image', function(e){
		e.preventDefault();
		ImageUploader($(this));
	});

	$( document ).on('dmz/remove_fields', function(e, el)
	{
		$(el).find('.image-uploader .remove-image').trigger('click');
	});

})(jQuery);

(function($){
	var setup = function(e, el){
		$(el).find('.ckeditor-control').each(function(){
			if( ! $(this).isClone($(this)) )
			{
				$(this).ckeditor();
			}
		});
		$(el).find('.select2-control').each(function(){
			if( ! $(this).isClone($(this)) )
			{
				$(this).select2();
			}
		});
	};
	$(document).on('dmz/setup_fields', setup);
	$(document).ready(function($) {
		$(document).trigger('dmz/setup_fields', this);
		$('.content-header button[type="submit"]').on('click', function(){
			$('form#master-form').submit();
		});
	});
})(jQuery);
