(function($, i18n){
	var bulkTable = {
		$table: null,
		init: function(){ 
			bulkTable.$table = $(".dataTable");
			bulkTable.$table.DataTable({
				responsive: true,
				autoWidth: false,
				initComplete: bulkTable.initComplete,
				columnDefs:[
					{
						orderable: false,
						targets: 0,
						width: '1%' 
					}
				],
				language:{
					info: "_MAX_ items",
					lengthMenu: "Show: _MENU_",
					paginate:{
						previous: "<i class='fa fa-caret-left'></i>",
						next: "<i class='fa fa-caret-right'></i>"
					}
				}
			});
			
			$("#bulkTableAll, .bulkTableRow").prop('checked', false);			
			$(".bulkTableRow").click( bulkTable.checked);
			$("#bulkTableAll").click(function()
			{ 
				$(".bulkTableRow").prop('checked', this.checked );
				bulkTable.checked();
			});
			
			$(".tablebulk button").click(function()
			{
				return confirm(i18n.bulk_message);
			});
		},
		initComplete: function(settings, json){
			var span = [  
				'<div class="tablebulk">',
					'<div class="btn-group">',
						'<label class="btn btn-default bulk-label">',
							'<input id="bulkTableAll" type="checkbox">', 
							'<span id="bulk-info"><span>0</span>&nbsp;'+ i18n.bulk_selected +'</span>',
						'</label>',
						'<button class="btn btn-default" type="submit">'+ i18n.bulk_button +'</button>',
					'</div>',
				'</div>',
			].join("");
			bulkTable.$table.before(span);
		},
		checked: function(){
			var count = $(".bulkTableRow:checked").length,
				isChecked = (count > 0);
				
			$('#bulk-info span').text(count);
			$('#post_records').toggleClass('bulk-active', isChecked);
			$("#bulkTableAll").prop('checked', isChecked);
		}
	};
	
	$(document).ready(bulkTable.init);
})(jQuery, window.FLTableLayout.i18n);