(function($){

	// check element exist
	$.fn.exists = function()
	{
		return (0 < $(this).length);
	};
})(jQuery); 

(function($){
	$(document).on('click', '.menubar-toggle, .mobile-nav__closebtn', function(){
		var $body = $("body");
		var $target = $("#mySidenav");
		
		if( $target.hasClass('open') )
		{
			$target.css('width', '0').removeClass('open');
			$body.removeClass('mobile-nav-active');
			return;
		}
		
		$body.addClass('mobile-nav-active');
		$target.css('width', '100%').addClass('open');
	});
	
	$(document).on('click', '.searchbar-toggle', function(){
		var $this = $(this),
			show  = $this.hasClass('open'),
			$target = $('.header-search > .header-search__form');
		
		$this.toggleClass('open');
		$target.toggle( !show );
	});
	
	$(document).ready(function(){
      $('.parallax').parallax();
    });

})(jQuery);
