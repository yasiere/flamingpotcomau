
/***!
 * Validation
 ***/
(function($, cartBase){

	// check element exist
	$.fn.exists = function()
	{
		return (0 < $(this).length);
	};

	var CheckotStep = {
		shipping: function(){
			var zip = $("#shippingZip").val(),
				email = $("#shippingEmail").val(),
				name = $("#shippingName").val(),
				addr1 = $("#shippingAddress").val(),
				cntry = $('#shippingCountry option:selected').text(),
				city  = $("#shippingCity").val(),
				state = $("#shippingState").val(),
				phone = $("#shippingPhone").val();

			var states = state.split(':');
			if( states.length > 1 )
			{
				state = states[1];
			}
			
			$(document).trigger('update/shipping');
			return [
				'<label>Your Email Address</label>',
				'<div>'+ email +'</div><br>',
				'<label>Shipping Address</label>',
				'<div>'+ name +'</div>',
				'<div>'+ addr1 +'</div>', 
				'<div>'+ city +', '+ state +' '+ zip +'</div>',
				'<div>'+ cntry +'</div>',
				'<div>'+ phone +'</div>'
			].join('');
		}
	};

	
	var validation = {
		status: true,
		summary: null,
		stripe: false,
		disabled: false,
		
		run: function( $el ){
			var _t = this;
			
			_t.status = true;
			_t.summary = [];
			
			$el.find('.field.required').each(function(){
				_t.validate( $(this) );
			});
		},

		validate: function($form){
			var _t = this,
				valid = true;
				
			if( $form.is(':hidden') )
			{
				return;
			}
			
			// another form type
			if( $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').exists() )
			{
				if( ! $form.find('input[type="text"], input[type="email"], input[type="number"], input[type="hidden"], textarea').val() )
				{
					valid = false;
				}
			}
			
			// select
			if( $form.find('select').exists() )
			{
				if( ! $form.find('select').val() )
				{
					valid = false;
				}
			}
			
			// radio
			if( $form.find('input[type="radio"]').exists() )
			{ 
				if( ! $form.find('input[type="radio"]:checked').exists() )
				{
					valid = false;
				}
			}
			
			// checkbox
			if( $form.find('input[type="checkbox"]:not(.truefalse-control)').exists() )
			{
				if( ! $form.find('input[type="checkbox"]:not(.truefalse-control):checked').exists() )
				{
					valid = false;
				}
			}
			
			if( ! valid )
			{
				// update status
				_t.status = false;
				
				// show error
				$form.addClass('has-error');
				
				// add summary
				_t.summary.push( $form.data('label') );

			}
		}
		
	};
	
	var showLoading = function( state ){
		$('.form-widget').toggleClass('loading-active', state);
		$('.shoppingcart-_wrapper').toggleClass('loading-active', state);
	};

	$(document).on('change', '#shippingCountry', function(){
		var url = cartBase.url + '/get-state/' + $(this).val();
		$.get(url, function(html){
			$('#shippingState').replaceWith(html);
		});
	});

	$(document).on('update/shipping', function(){
			var country = $('#shippingCountry').val();
			var state	= $('select#shippingState').val();
			
			if( ! country )
				return;
			
			if( state )
				country = state;
			
			showLoading(true);

			var url = cartBase.url + '/shipping-price/' + country;
			/* $('.shoppingcart-_wrapper').addClass('loading-active'); */
			$.get(url, function(data) {
				if(data)
				{
					data = JSON.parse(data);
					$('.checkout-shipping .price')
						.addClass('currency-code-aud')
						.html('<span class="money-native">'+ data.cost +'</span>'); 
					$('#shippingCost').val(data.number_cost);
					$('.cart_total .money-native').text(data.total);
				}
			}).always(function(){
				showLoading(false);
				/* //$('.shoppingcart-_wrapper').removeClass('loading-active'); */
			});
	});

	$(document).on('focus click', 'form.use-validation .field.required.has-error', function(){
		$(this).removeClass('has-error');
	});

	$(document).on('click', '.form-widget .edit-fieldset', function(e){
		var $widget = $(this).closest('.form-widget');

		$widget.find('.saved-fieldset').remove();
		$widget.removeClass('complete incomplete').addClass('editing')
			.nextAll('.form-widget').removeClass('complete editing').addClass('incomplete');

		$('input#copyFromShipping').prop('checked', false).trigger('change');
		validation.stripe = false;
		
		e.preventDefault();
	}); 

	$(document).on('change', 'input[name="paymentMethod"]', function(){
		var $this = $(this);
		var stripe = ('stripe' == $this.val());
		
		validation.stripe = stripe;
		$this.closest('.box-body')
			.toggleClass('use-stripe-payment', stripe);
		if('stripe' == $(this).val())
		{
			validation.stripe = true;
			$box.addClass('use-stripe-payment');
			return;
		}
		
		validation.stripe = false;
		$box.removeClass('use-stripe-payment');
	});/* 
	
	$(document).on('click', 'button.paypal-button', function(){ 
		validation.disabled = true; 
	}); 
	
	$(document).on('click', 'button.stripe-button', function(e){
		//$('#checkout-form').find('#credit-card').addClass('hidden');
		var $this = $(this);
		var $stripe = $this.closest('.box-body');
		if( ! $stripe.hasClass('use-stripe-payment') )
		{
			$stripe.addClass('use-stripe-payment');
			e.preventDefault(); 
			return;
		} 
		
		validation.stripe = true;
	}); */

	$(document).on('submit', 'form.use-validation', function(){
		var $this = $(this),
			$focus = $this.find('.form-widget.editing');
		
		showLoading(true);
			//$focus.addClass('loading-active');
			$focus.find('button').prop('disabled', true);
			
		if( validation.disabled )
		{
			return true;
		}
		
		// run validation
		validation.run($this);

		// show validating summary if form now(on editing) not valid
		$this.find('.error-summary').remove();
		if( ! validation.status )
		{ 
			$focus.find('.form-widget-summary').before('<div class="error-summary">'+ validation.summary.join(', ')  +' required.</div>');
			/* $focus.removeClass('loading-active'); */
			$focus.find('button').prop('disabled', false);
			showLoading(false);

			return false;
		}
		
		
		// handle in form now valid then ig has next form will move to.
		// and set form now to complete
		var $next = $focus.next('.form-widget.incomplete')
		if( $next.exists() )
		{
			var step = $focus.data('form');
			
			if( CheckotStep[ step ] )
			{
				var input = CheckotStep[ step ]();	
				$focus.find('fieldset').after('<div class="saved-fieldset">'+ input +'</div>');
			}
			
			$focus.removeClass('editing').addClass('complete');				
			$next.removeClass('incomplete').addClass('editing');
			/* $focus.removeClass('loading-active'); */
			$focus.find('button').prop('disabled', false);
			showLoading(false);
			
			return false;
		}
		
		if( validation.stripe )
		{
			var stripeResponseHandler = function(status, response) {
				if (response.error) {
					// Show the errors on the form
					$focus.find('.form-widget-summary').before('<div class="error-summary">'+ response.error.message  +' required.</div>');
					$focus.find('button').prop('disabled', false); // Re-enable submission
				} 
				else {
					var token = response.id;
					var $form = $('#checkout-form');
					$form.append($('<input type="hidden" name="stripeToken"/>').val(token));
					validation.disabled = true;
					$form.get(0).submit();
				}
				
				showLoading(false);
			};
			
			Stripe.card.createToken($('#checkout-form'), stripeResponseHandler);

			return false; 
		}
		
		return true;
	});
	
	
	$(document).on('change', 'input#copyFromShipping', function(){
		var isCheked = $(this).is(':checked');
		var $zip = $("#billingZip"), 
			$name = $("#billingName"),
			$addr = $("#billingAddress"),
			$cntry = $("#billingCountry"),
			$city  = $("#billingCity"),
			$state = $("#billingState");
			
		var val_zip = '', 
			val_name = '', 
			val_addr = '', 
			val_cntry = '',
			val_city  = '',
			val_state = '';
			
		if(isCheked)
		{ 
			val_zip = $("#shippingZip").val(); 
			val_name = $("#shippingName").val(); 
			val_addr = $("#shippingAddress").val(); 
			val_cntry = $("#shippingCountry").val();
			val_city  = $("#shippingCity").val();
			val_state = $("#shippingState").val(); 
			
			var states = val_state.split(':');
			if( states.length > 1 )
			{
				val_state = states[1];
			}
		}
		
		
		$zip.prop('disabled', isCheked).val(val_zip);
		$name.prop('disabled', isCheked).val(val_name); 
		$addr.prop('disabled', isCheked).val(val_addr); 
		$cntry.prop('disabled', isCheked).val(val_cntry);
		$city.prop('disabled', isCheked).val(val_city);
		$state.prop('disabled', isCheked).val(val_state);
	});
	
	
	$(document).ready(function(){
		$(".form-widget").addClass('incomplete').first().removeClass('incomplete').addClass('editing');
	});
	
})(jQuery, window.FLBase); 
